FROM node:10.19.0-alpine as BUILD

ENV APP_HOME=/root
ENV TZ="America/Argentina/Buenos_Aires"

WORKDIR $APP_HOME

COPY package.json $APP_HOME

COPY ./src $APP_HOME/src
COPY ./e2e $APP_HOME/e2e

COPY ./browserslist $APP_HOME/browserslist
COPY ./.editorconfig $APP_HOME/.editorconfig
COPY ./tslint.json $APP_HOME/tslint.json
COPY ./tsconfig.spec.json $APP_HOME/tsconfig.spec.json
COPY ./tsconfig.json $APP_HOME/tsconfig.json
COPY ./tsconfig.app.json $APP_HOME/tsconfig.app.json
COPY ./proxy.conf.json $APP_HOME/proxy.conf.json
COPY ./karma.conf.js $APP_HOME/karma.conf.js
COPY ./angular.json $APP_HOME/angular.json

RUN npm install
RUN npm run buildProd

FROM nginx


ENV TZ="America/Argentina/Buenos_Aires"

COPY --from=BUILD /root/dist/la-justa-fe /usr/share/nginx/html
COPY /nginx/reverse-proxy.conf /etc/nginx/conf.d/default.conf
#COPY /nginx/reverse-proxy-local.conf /etc/nginx/conf.d/default.conf
COPY /.well-known /www/media/images/.well-known
COPY /nginx/lajustaunlp.com.ar.crt /root/lajustaunlp.com.ar.crt
COPY /nginx/lajustaunlp.com.ar.key /root/lajustaunlp.com.ar.key