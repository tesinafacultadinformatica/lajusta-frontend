import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent {
  @Input() placeholder = 'Buscar';
  @Output() filter = new EventEmitter<string>();
  textFilter = '';


  public onFilter = () => {
    //this.dataSource.filter = this.textFilter.trim().toLocaleLowerCase();
    this.filter.emit(this.textFilter.trim().toLocaleLowerCase());

  }

}