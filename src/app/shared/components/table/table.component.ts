import { Component, Input, Output, EventEmitter, ViewChild, AfterViewInit, ChangeDetectionStrategy, OnInit, Injector, ChangeDetectorRef, ViewChildren, QueryList, OnChanges, SimpleChanges } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { listMap } from '../../services/basic-services/base-configuration-service.interface';
import { merge, of, Subscription } from 'rxjs';
import { startWith, switchMap, map, catchError, debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { DatePipe, CurrencyPipe } from '@angular/common';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { MatSort } from '@angular/material/sort';
import { Filter } from '../../models/filter';
import { addressComplete } from '../../helper/util/address-complete';


interface ExampleFlatNode {
  expandable: boolean;
  name: string;
  count: number;
  level: number;
}
@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableComponent implements OnInit, OnChanges, AfterViewInit {
  @Input() displayedColumns = [];
  @Input() textColumns = [];
  @Input() pageSizeOptions = [6, 12, 27, 60];
  @Input() hasFilter = true;
  @Input() aditionalFilter: Filter = null;
  @Input() hasPagination = true;
  @Input() pageSize = 12;
  @Input() type: string;
  @Input() changeClick:boolean = false;
  @Input() addNew:boolean = true;
  @Input() canEdit:boolean = true;
  @Input() canDeleteRestore:boolean = true;
  @Input() innerDisplayedColumns = [];
  @Input() textInnerDisplayedColumns = [];
  @Input() childrens: string;
  @Input() closeTooltip: string = 'Cerrar';
  @Input() download1Tooltip: string = 'Descargar';
  @Input() download2Tooltip: string = 'Descargar';

  @Output() new = new EventEmitter();
  @Output() rowChange = new EventEmitter();
  @Output() detail = new EventEmitter<number>();
  @Output() edit = new EventEmitter<number>();
  @Output() delete = new EventEmitter<any>();
  @Output() restore = new EventEmitter<any>();
  @Output() check = new EventEmitter<any>();
  @Output() copy = new EventEmitter<any>();
  @Output() detailChild = new EventEmitter<number>();
  @Output() editChild = new EventEmitter<number>();
  @Output() deleteChild = new EventEmitter<any>();
  @Output() restoreChild = new EventEmitter<any>();
  @Output() close = new EventEmitter<any>();
  @Output() download1 = new EventEmitter<any>();
  @Output() download2 = new EventEmitter<any>();


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('outerSort', { static: true }) sort: MatSort;
  //@ViewChildren('innerSort') innerSort: QueryList<MatSort>;
  //@ViewChildren('innerTables') innerTables: QueryList<MatTable<any>>;

  dataSource: MatTableDataSource<any>;
  expandedElement: any | null;
  anyData: any[] = [];


  public service: any;
  public isLoadingResults: boolean = false;
  public isRateLimitReached: boolean = false;
  public resultsLength: number = 0;
  public filter: string;
  subscriptionUpdate: Subscription;

  constructor(private datePipe: DatePipe, private currencyPipe: CurrencyPipe, private cdr: ChangeDetectorRef,
    private injector: Injector) {
  }
  ngOnChanges(changes: SimpleChanges): void {
    if(changes.aditionalFilter &&  changes.aditionalFilter.currentValue != changes.aditionalFilter.previousValue){
      this.mergeData();
    }
  }

  ngOnInit() {
    // Resolve AbstractFactory
    const injectable = listMap.get(this.type);
    // Inject service
    this.service = this.injector.get(injectable);

    this.loadData();

    this.subscriptionUpdate = this.service.getUpdateData().subscribe((data) => {
      this.loadData();
     });
  }

  loadData(){
    this.dataSource = new MatTableDataSource(this.service.getData()); // this.service.getData()
  }

  getColumns(columns: any[]) {
    let aux = [...columns];

    let index = aux.findIndex(el => el === 'close' )
    if (index > -1) {
      aux.splice(index, 1);
    }
    index = aux.findIndex(el => el === 'view' || el === 'viewc' )
    if (index > -1) {
      aux.splice(index, 1);
    }
    index = aux.findIndex(el => el === 'edit' || el === 'editc' )
    if (index > -1) {
      aux.splice(index, 1);
    }
    index = aux.findIndex(el => el === 'check' )
    if (index > -1) {
      aux.splice(index, 1);
    }
    index = aux.findIndex(el => el === 'copy' )
    if (index > -1) {
      aux.splice(index, 1);
    }
    index = aux.findIndex(el => el === 'delete' || el === 'deletec' )
    if (index > -1) {
      aux.splice(index, 1);
    }
    index = aux.findIndex(el => el === 'download1' )
    if (index > -1) {
      aux.splice(index, 1);
    }
    index = aux.findIndex(el => el === 'download2' )
    if (index > -1) {
      aux.splice(index, 1);
    }
    return aux;
  }

  private getTransformValue(value:any, type:any){
    if(typeof(value) === 'boolean'){
      return value ? 'Si' : 'No';
    }
    if(type === 'currency'){
      return this.currencyPipe.transform(value,'$');
    }
    if(type === 'address'){
      return addressComplete(value);
    }
    return (value instanceof Date || type === 'date') ? this.datePipe.transform(value,'dd-MM-yyyy HH:mm') : value;
  }

  getValue(row: any, item: string) {
    let itemType = item.split(':');
    let itemArray = itemType[0].toString().split('.');
    if(itemArray.length == 2){
      if(row[itemArray[0]] && row[itemArray[0]] !== undefined){
        return this.getTransformValue(row[itemArray[0]][itemArray[1]], itemType[1]);
      }
      return null;
    } else {
      if(itemArray.length == 3){
        if(row[itemArray[0]] && row[itemArray[0]] !== undefined  && row[itemArray[0]][itemArray[1]] && row[itemArray[0]][itemArray[1]] !== undefined){
          return this.getTransformValue(row[itemArray[0]][itemArray[1]][itemArray[2]], itemType[1]);
        }
        return null;
     }
    }
    return  this.getTransformValue(row[itemType[0]], itemType[1]);
  }

  getImage(row: any, item: string) {

  }
  /**
   * Set the paginator and sort after the view init since this component will
   * be able to query its view for the initialized paginator and sort.
   */
  ngAfterViewInit() {
    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    this.mergeData();
  }

  mergeData(){
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.service.getData(this.filter, this.sort.active, this.sort.direction,
            this.paginator.pageIndex, this.paginator.pageSize, this.aditionalFilter ? [this.aditionalFilter] : [],
            (total) => this.resultsLength = total);
        }),
        map(data => {
          this.isLoadingResults = false;
          this.isRateLimitReached = false;
          //alternatively to response headers;
          //this.resultsLength = data.length;
          return data;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          this.isRateLimitReached = true;
          return of([]);
        })
      ).subscribe(data => {
        if(this.childrens){
          this.anyData = [];
         (data as any[]).forEach(element => {
            if (element[this.childrens] && Array.isArray(element[this.childrens]) && element[this.childrens].length) {
              this.anyData = [...this.anyData, {...element, childrens: new MatTableDataSource(element[this.childrens])}];
            } else {
              this.anyData = [...this.anyData, element];
            }
          });
          this.dataSource.data = this.anyData;
        } else {
          this.dataSource.data = data as any[];
        }
        this.cdr.detectChanges();});
  }


  toggleRow(element: any) {
    element.childrens && (element.childrens as MatTableDataSource<any>).data.length ? (this.expandedElement = this.expandedElement === element ? null : element) : null;
    this.cdr.detectChanges();
   // this.innerTables.forEach((table, index) => (table.dataSource as MatTableDataSource<any>).sort = this.innerSort.toArray()[index]);
  }


  applyFilter(filterValue: string) {
    this.filter = filterValue.trim().toLowerCase();
    this.mergeData();
  }

  public clickRowChange = (id: number) => {
    if (this.changeClick){
      this.rowChange.emit(id);
    }
  }

  public redirectToNew = () => {
    this.new.emit();
  }

  public redirectToDetails = (id: number) => {
    this.detail.emit(id);
  }

  public redirectToUpdate = (id: number) => {
    this.edit.emit(id);
  }

  public redirectToDelete = (element: any) => {
    this.delete.emit(element);
  }

  public redirectToRestore = (element: any) => {
    this.restore.emit(element);
  }

  public redirectToCheck = (element: any) => {
    this.check.emit(element);
  }

  public redirectToCopy = (element: any) => {
    this.copy.emit(element);
  }

  public onClose = (element: any) => {
    this.close.emit(element);
  }

  public onDownload1 = (element: any) => {
    this.download1.emit(element);
  }

  public onDownload2 = (element: any) => {
    this.download2.emit(element);
  }

  public redirectToDetailsChild = (id: number) => {
    this.detailChild.emit(id);
  }

  public redirectToUpdateChild = (id: number) => {
    this.editChild.emit(id);
  }

  public redirectToDeleteChild = (element: any) => {
    this.deleteChild.emit(element);
  }

  public redirectToRestoreChild = (element: any) => {
    this.restoreChild.emit(element);
  }


  ngOnDestroy() {
    this.subscriptionUpdate.unsubscribe();
  }
}



