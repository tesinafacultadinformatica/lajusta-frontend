import {
  Component,
  OnDestroy,
  OnInit,
  Output,
  EventEmitter,
  Input,
} from "@angular/core";
import { Subject, Subscription } from "rxjs";
import { AuthService } from "../../services/auth.service";
import { Router, NavigationEnd } from "@angular/router";
import { GeneralService } from "../../services/general.service";
import { MatDialog } from '@angular/material/dialog';
import { DialogModalComponent } from '../../modals/dialog-modal/dialog-modal.component';
import { User } from '../../models/user';

@Component({
  selector: "app-header",
  styleUrls: ["./header.component.scss"],
  templateUrl: "./header.component.html",
})
export class HeaderComponent implements OnInit, OnDestroy {
  @Input() categories = [];
  @Output() openLoginRegister = new EventEmitter();
  @Output() openContact = new EventEmitter();
  @Output() openCart = new EventEmitter();
  @Output() openCartHistory = new EventEmitter();
  @Output() openProfile = new EventEmitter();
  @Output() openChangePassword = new EventEmitter();
  @Output() public sidenavToggle = new EventEmitter();

  private destroy$: Subject<void> = new Subject<void>();
  userPictureOnly: boolean = false;
  user: User;
  childProductActive: boolean = false;
  pageActive: boolean = false;
  subscriptionImage:Subscription;
  subscriptionActive: Subscription;

  userMenu = [
    {
      title: "Mi Perfil",
      action: "profile",
      icon: "fa-user",
    },
    {
      title: "Mis Compras",
      action: "cart",
      icon: "fa-shopping-bag",
    },
    {
      title: "Cambiar Contraseña",
      action: "changePassword",
      icon: "fa-key",
    },
    {
      title: "Cerrar Sesión",
      action: "logout",
      icon: "fa-sign-out-alt",
    },
  ];

  constructor(
    public authService: AuthService,
    private generalService: GeneralService,
    public dialog: MatDialog,
    private router: Router
  ) {
    this.subscriptionActive = this.generalService.pageIsActive().subscribe(
      data => {this.pageActive = data;}
    )
    this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationEnd) {
        this.childProductActive = this.router.url.includes("shop/productos");
      }
    });
  }

  public onToggleSidenav = () => {
    this.sidenavToggle.emit();
  };

  openLoginModal() {
    this.openLoginRegister.emit();
  }

  openContactModal() {
    this.openContact.emit();
  }

  openCartModal() {
    this.openCart.emit();
  }

  ngOnInit() {
    this.user = this.authService.isLoggedIn()
      ? this.authService.currentUserValue.user
      : null;

    this.authService.currentUser.subscribe(
      userL => {this.user = userL ? userL.user : null}
    )
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    this.subscriptionImage.unsubscribe();
    this.subscriptionActive.unsubscribe();
  }

  userAction(action) {
    if (action === "profile") {
      this.openProfile.emit();
    }

    if (action === "changePassword") {
      this.openChangePassword.emit();
    }

    if (action === "cart") {
      this.openCartHistory.emit();
    }

    if (action === "logout") {
        const dialogRef = this.dialog.open(DialogModalComponent, {
          disableClose: true,
          width: '300px',
          data: { title: '¿Cerrar sesión?', description: 'Esta seguro/a?' }
        });
        dialogRef.afterClosed().subscribe(result => {
          if (result){
            this.authService.logout();
            this.router.navigate(['/']);
          }
        });
      }
  }

  getRouterLink(id: number) {
    let link = "/shop/productos";
    if (id) {
      link += "/" + id;
    }
    return link;
  }
}
