import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { CurrencyPipe } from '@angular/common';
import { CartService } from 'src/app/shared/services/cart.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-cart-status',
  templateUrl: './cart-status.component.html',
  styleUrls: ['./cart-status.component.scss'],
})
export class CartStatusComponent implements OnInit, OnDestroy {
  totalPrice: number = 0;
  totalQuantity: number = 0;
  
  subscriptionPrice: Subscription;
  
  subscriptionQuantity: Subscription;

  @Output() openCart = new EventEmitter();

  constructor(private currencyPipe: CurrencyPipe, private cartService: CartService ) {  }

  ngOnInit() {
    
    //subscribe to the events
    this.subscriptionPrice = this.cartService.getTotalPrice().subscribe(
      data => {this.totalPrice = data;}
    )

    this.subscriptionQuantity = this.cartService.getTotalQuantity().subscribe(
      data => this.totalQuantity = data
    );
  }


  openCartModal() {
    this.openCart.emit();
  }
  
ngOnDestroy() {
  
this.subscriptionPrice.unsubscribe();

this.subscriptionQuantity.unsubscribe();
}
  /*
  transformTotal() {
    const value = this.claimForm.controls.total.value;
    this.claimForm.controls.total.setValue(
      this.formatMoney(value.replace(/\,/g, "")), 
      {emitEvent: false}
    );
} */
}