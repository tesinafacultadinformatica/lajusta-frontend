import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-button-with-children',
  templateUrl: './button-with-children.component.html',
  styleUrls: ['./button-with-children.component.scss']
})
export class ButtonWithChildrenComponent {
  @Input() menuCategory;
  @Output() onSelect= new EventEmitter(); 

  constructor() {  }

  selectCategory(){
    this.onSelect.emit();
  }
}