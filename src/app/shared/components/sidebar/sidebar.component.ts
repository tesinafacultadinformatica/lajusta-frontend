import { Component, Output, EventEmitter, Input} from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  styleUrls: ['./sidebar.component.scss'],
  templateUrl: './sidebar.component.html',
})
export class SidebarComponent  {
  @Input() categories = [];
  @Output() openContact = new EventEmitter();
  @Output() sidenavClose = new EventEmitter();
 
  childProductActive: boolean = false;
  constructor(public authService: AuthService,  
    private router: Router) {
      this.router.events.subscribe(
        (event: any) => {
          if (event instanceof NavigationEnd) {
            this.childProductActive = this.router.url.includes('shop/productos');
          }
        }
      ); }
 
  ngOnInit() {
  }
 
  public onSidenavClose = () => {
    this.sidenavClose.emit();
  }

  public openContactModal = () => {
    this.openContact.emit();
    this.onSidenavClose();
  }
  
  getRouterLink(id:number){
    let link = '/shop/productos'
    if(id){
      link +=  '/' + id;
    }
    return link ;
  }
}
