import { Component, Input, OnInit } from '@angular/core';
import { Banner } from '../../models/banner';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {
  @Input() banners: Banner[] = [];

    constructor(){
    }

  ngOnInit(): void {
  }

    goTo(url:string){
      if(url){
        window.open(url, "_blank");
      }
    }
}
