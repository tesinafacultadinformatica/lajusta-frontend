import { NgModule } from "@angular/core";
import { CommonModule, CurrencyPipe, UpperCasePipe, DatePipe, KeyValuePipe } from "@angular/common";
import { NoProductsFoundComponent } from "./components/no-products-found/no-products-found.component";
import { FormsModule, FormBuilder, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { NoAccessComponent } from "./components/no-access/no-access.component";
import { PageNotFoundComponent } from "./components/page-not-found/page-not-found.component";
import { FilterByBrandPipe } from "./pipes/filterByBrand.pipe";
import { AdminGaurd } from "./guards/admin-gaurd";
import { AuthGuard } from "./guards/auth_gaurd";
import { AuthService } from "./services/auth.service";
import { MomentTimeAgoPipe } from "./pipes/moment-time-ago.pipe";
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { LoaderService } from './services/loader.service';
import { LoaderInterceptor } from './interceptors/loader.interceptor';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { JwtInterceptor } from './interceptors/jwt.interceptor';
import { ErrorInterceptor } from './interceptors/error.interceptor';
import { EllipsisPipe } from './pipes/ellipsis.pipe';
import { CartStatusComponent } from './components/header/cart-status/cart-status.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CarouselComponent } from './components/carousel/carousel.component';
import { SearchComponent } from './components/search/search.component';
import { MaterialModule } from '../material.module';
import { TableComponent } from './components/table/table.component';
import { FilterComponent } from './components/table/filter/filter.component';
import { ButtonWithChildrenComponent } from './components/button-with-children/button-with-children.component';
import { ModalsModule } from './modals/modals.module';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		HttpClientModule,
		RouterModule,
		MDBBootstrapModule.forRoot(),
		ModalsModule,
		MaterialModule,
		FlexLayoutModule,
	],
	declarations: [
		NoProductsFoundComponent,
		FilterByBrandPipe,
		NoAccessComponent,
		PageNotFoundComponent,
		MomentTimeAgoPipe,
		EllipsisPipe,
		FooterComponent,
		HeaderComponent,
		CartStatusComponent,
		ButtonWithChildrenComponent,
		SidebarComponent,
		SpinnerComponent,
		CarouselComponent,
		SearchComponent,
		FilterComponent,
    TableComponent,
	],
	exports: [
		NoProductsFoundComponent,
		FormsModule,
		RouterModule,
		FilterByBrandPipe,
		NoAccessComponent,
		PageNotFoundComponent,
		MomentTimeAgoPipe,
		EllipsisPipe,
		FooterComponent,
		HeaderComponent,
		CartStatusComponent,
		ButtonWithChildrenComponent,
		SidebarComponent,
		MDBBootstrapModule,
		SpinnerComponent,
		CarouselComponent,
		SearchComponent,
    TableComponent,
		ModalsModule
	],
	providers: [
		AuthService,
		AuthGuard,
		AdminGaurd,
		FormBuilder,
		CurrencyPipe,
    DatePipe,
    KeyValuePipe,
		UpperCasePipe,
		LoaderService,
		{ provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
		{ provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
		{ provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
	]
})
export class SharedModule { }
