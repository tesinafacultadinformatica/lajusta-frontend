import { Node } from "../../models/node";
import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  Input,
  ElementRef,
  EventEmitter,
  OnChanges,
  Output,
} from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { CartService } from "../../services/cart.service";
import { ModalService } from "../../services/modal.service";
import { CartItem, Cart } from "src/app/shared/models/cart";
import { Product } from "src/app/shared/models/product";
import { Subscription } from "rxjs";
import { AuthService } from "../../services/auth.service";
import { GeneralService } from "../../services/general.service";
import { DateTimeNode } from "../../models/general";
import { Router } from '@angular/router';
import { Role } from "../../enums/role.enum";

@Component({
  selector: "app-cart-modal",
  templateUrl: "./cart-modal.component.html",
  styleUrls: ["./cart-modal.component.scss"],
})
export class CartModalComponent implements OnInit, OnChanges, OnDestroy {
  @Input() id: string;
  @Input() idCart: number = null;
  @ViewChild("cartModal") cartModal: any;
  private element: any;

  @Output() openProductDetail = new EventEmitter<number>();
  @Output() openNode = new EventEmitter<Node>();
  @Output() openLogin = new EventEmitter<number>();

  available = false;

  cartActive: Cart;
  cartItems: CartItem[] = [];
  nodes: DateTimeNode[] = [];

  observation: string;
  editField: string;
  idGeneral: number = null;
  selectedNode: number = 1;
  subscriptionCartItems: Subscription;

  constructor(
    private cartService: CartService,
    private generalService: GeneralService,
    private toastrService: ToastrService,
    private modalService: ModalService,
    private authService: AuthService,
    public router: Router,
    el: ElementRef
  ) {
    this.element = el.nativeElement;
    this.generalService.getActive().subscribe((result) => {
      this.idGeneral = result ? result.id : null;
      this.getNodes();
    });
  }
  ngOnChanges(): void {}

  ngOnInit(): void {
    let modal = this;

    // ensure id attribute exists
    if (!this.id) {
      console.error("modal must have an id");
      return;
    }
    // add self (this modal instance) to the modal service so it's accessible from controllers
    this.modalService.add(this);
    if (!this.idCart) {
      this.subscriptionCartItems = this.cartService
        .getCartItems()
        .subscribe((data) => {
          this.cartItems = data;
          this.getNodes();
        });
    }
    this.getCarts();
  }

  getNodes() {
    if (this.idGeneral) {
      // this.spinnerService.show();
      let withFridge = this.cartItems
        ? this.cartItems.some((item) => item.product.needCold)
        : null;
      this.generalService
        .getGeneralNodes(this.idGeneral, withFridge)
        .subscribe((result) => {
          this.nodes = result;
          if (!this.selectedNode) {
            this.selectedNode =
              this.nodes?.length > 0 ? this.nodes[0].id : null;
          }
        });
    }
  }
  // open modal
  open(): void {
    this.available = true;
    this.cartModal.show();
  }

  // close modal
  close(): void {
    this.available = false;
    this.cartModal.hide();
  }

  getCarts() {
    // this.spinnerService.show();
    if (!this.idCart) {
      this.cartItems = this.cartService.getLocalCartProducts();
      this.getNodes();
    } else {
      this.cartService.getById(this.idCart).subscribe((res) => {
        this.cartActive = res;
        this.cartItems = res.cartProducts;
        this.selectedNode = res.nodeDate.node.id;
        if (this.idGeneral === res.idGeneral) {
          this.getNodes();
        }
      });
    }
  }

  updateList(id: number, property: string, event: any) {
    if (event.target.value === "" || event.target.value < "1") {
      this.cartItems[id][property] = "1";
    } else {
      if (event.target.value > this.cartItems[id].product.stock) {
        this.cartItems[id][property] = this.cartItems[id].product.stock;
      } else {
        this.cartItems[id][property] = event.target.value;
      }
    }
    this.cartService.updateLocalCartProduct(
      this.cartItems[id].product.id,
      Number(this.cartItems[id].quantity)
    );
  }

  getTotal() {
    return this.cartItems.reduce(
      (acc, cur) => acc + cur.quantity * cur.product.price,
      0
    );
  }

  getTotalWithDiscount(){
    return (this.getTotal() - this.getPersonalDiscount());
  }

  getPersonalDiscount(){
    var temp = 0
    if (this.isPersonal()){
      temp =  this.cartItems.reduce(
        (acc, cur) => acc + cur.quantity * (cur.product.price - cur.product.buyPrice),0
      )
    }
    return temp;
  }

  isPersonal():boolean{
    return this.authService.currentUserValue != null ? 
    this.authService.currentUserValue.user.role == +Role.Equipo ||
    this.authService.currentUserValue.user.role == +Role.Administrador  :
    false;
  }

  openProductDetailModal(productId: number) {
    this.openProductDetail.emit(productId);
  }

  openLocationModal(node: Node) {
    this.openNode.emit(node);
  }

  getDescriptionNode(node: Node) {
    return `${node.name} - ${node.address.street ? node.address.street : ''} N° ${node.address.number ? node.address.number : ''} ${node.address.description ? node.address.description : ''}  ${node.address.floor ? node.address.floor : ''}  ${node.address.apartment ? node.address.apartment : ''} ${node.address.betweenStreets ? node.address.betweenStreets : ''} `;
  }

  getDescriptionNodeActive() {
    const node = this.nodes
      ? this.nodes.find((n) => n.id === this.selectedNode).node
      : this.cartActive.nodeDate.node;
    return this.getDescriptionNode(node);
  }

  confirm() {
    if (this.authService.isLoggedIn()) {
      let cartToSave = new Cart();
      cartToSave.cartProducts = this.cartItems.map((item) => {
        let product = new Product();
        product.id = item.product.id;
        return new CartItem(product, item.quantity);
      });
      cartToSave.nodeDate = this.nodes.find(
        (node) => node.id === this.selectedNode
      );
      cartToSave.user = { id: this.authService.currentUserValue.user.id };
      if (this.isPersonal()) cartToSave.staffDiscount = true;
      cartToSave.observation = this.observation;
      this.cartService[this.idCart ? "update" : "create"](cartToSave).subscribe(
        (result) => {
          this.toastrService.success(
            "Felicitaciones!",
            "La compra fue " +
              (this.idCart ? "actualizada" : "confirmada") +
              " con exito."
          );

          localStorage.setItem("recent_buy", JSON.stringify(cartToSave.nodeDate));
          this.cartItems = [];
          this.close();
          setTimeout(() =>
          {
            window.location.href = '/shop/home';
          },
          5000);
        },
        (err) => {
          this.toastrService.error("Error en la compra", err);
        }
      );
    } else {
      this.toastrService.error(
        "Debe loguearse.",
        "Por favor ingrese para confirmar nuevamente la compra."
      );
      this.openLogin.emit();
    }

    //this.openNodeInfo.emit(node);
  }

  removeItem(productId: number, idx: number) {
    this.cartService.removeLocalCartProduct(productId);
    this.cartItems = this.cartItems.filter(cartItem => cartItem.product.id !== productId);
    this.getNodes();
  }

  ngOnDestroy() {
    this.modalService.remove(this.id);
    this.subscriptionCartItems.unsubscribe();
    this.element.remove();
  }
}
