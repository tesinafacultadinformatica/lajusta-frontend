import { Component, OnInit, OnDestroy, ViewChild, Input, ElementRef, OnChanges } from '@angular/core';
import { ModalService } from '../../services/modal.service';
import { Image } from 'src/app/shared/models/image';
@Component({
	selector: 'app-gallery-modal',
	templateUrl: './gallery-modal.component.html',
	styleUrls: ['./gallery-modal.component.scss']
})
export class GalleryModalComponent implements OnInit, OnChanges, OnDestroy {
	
	@Input() id: string;
	@Input() images: Image[];
	private element: any;
	@ViewChild('galleryModal') galleryModal: any;
	
	available = false;

	constructor(
		private modalService: ModalService,
		el: ElementRef
	) {
		this.element = el.nativeElement;
	}

	ngOnInit(): void {
		let modal = this;

		// ensure id attribute exists
		if (!this.id) {
			console.error('modal must have an id');
			return;
		}
		// add self (this modal instance) to the modal service so it's accessible from controllers
		this.modalService.add(this);

	}

	ngOnChanges(): void {
		
	}
	
	// open modal
	open(): void {
		this.available= true;
		this.galleryModal.show();
	}

	// close modal
	close(): void {
		this.available= false;
		this.galleryModal.hide()
	}
	

	ngOnDestroy() {
		this.modalService.remove(this.id);
		this.element.remove();
	}
}
