import {
  Component,
  ViewChild,
  ElementRef,
  OnInit
} from "@angular/core";
import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";
import { ModalService } from "src/app/shared/services/modal.service";
import { GeneralService } from "src/app/shared/services/general.service";
import { ToastrService } from "ngx-toastr";
import { NodeService } from "../../services/node.service";
import { Node } from "../../models/node";
import { ConfigBaseModalComponent } from '../config-base-modal/config-base-modal.component';

@Component({
  selector: "app-config-general-modal",
  templateUrl: "./config-general-modal.component.html",
  styleUrls: ["./config-general-modal.component.scss"],
})
export class ConfigGeneralModalComponent extends ConfigBaseModalComponent implements OnInit{
  @ViewChild("configGeneralModal") configGeneralModal: any;

  nodes: Node[];
  minDate: Date = new Date(Date.now());
  maxDate: Date = new Date(
    new Date().setFullYear(new Date().getFullYear() + 1)
  );

  constructor(
    private formBuilder: FormBuilder,
    private generalService: GeneralService,
    private nodeService: NodeService,
    private toastrService: ToastrService,
    modalService: ModalService,
    el: ElementRef
  ) {
    super(modalService,el);
  }

  getActiveNodesControls() {
    return (<FormArray>this.form.get("activeNodes")).controls;
  }

  ctrActiveNode(str: string, i: number) {
    return (<FormArray>this.form.get("activeNodes")).controls[i].get(
      str
    );
  }

  get dateActivePage() {
    return this.form.get("dateActivePage");
  }
  get dateDownPage() {
    return this.form.get("dateDownPage");
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.loadNodes();
  }

  loadNodes(): void {
    this.nodeService.getAll().subscribe((result: Node[]) => {
      this.nodes = result;
    });
  }

  loadData(): void {
    super.loadData();
    if (this.idElement) {
      this.generalService.getById(this.idElement).subscribe((result) => {
        this.current = result;
        if (this.action === 'copy'){
          this.current.id = null;
          this.current.dateActivePage = null;
          this.current.dateDownPage = null;
         // this.current.activeNodes.forEach(node => {node.dateTimeFrom = null; node.dateTimeTo = null;})
        }
        this.createForm();
      });
    } else {
      this.current = null;
      this.createForm();
    }
  }

  addNode(): void {
    (<FormArray>this.form.get("activeNodes")).push(this.addActiveNode());
  }

  addActiveNode(): FormGroup {
    return this.formBuilder.group({
      node: ["", Validators.required],
      day: ["", Validators.required],
      dateTimeFrom: ["", Validators.required],
      dateTimeTo: ["", Validators.required],
    });
  }

  removeNode(idx) {
    (<FormArray>this.form.get("activeNodes")).removeAt(idx);
  }

  createFormActiveNodes() {
    if (this.current && this.current.activeNodes) {
      return this.current.activeNodes.reduce(
        (valorAnterior, valorActual, indice, vector) => {
          valorAnterior.push(
            this.formBuilder.group({
              id: [valorActual.id],
              node: [valorActual.node.id, Validators.required],
              day: [valorActual.day? valorActual.day : null, Validators.required],
              dateTimeFrom: [ valorActual.dateTimeFrom?
                valorActual.dateTimeFrom : null,
                Validators.required,
              ],
              dateTimeTo: [valorActual.dateTimeTo?
                valorActual.dateTimeTo : null,
                Validators.required,
              ],
            })
          );
          return valorAnterior;
        },
        []
      );
    } else {
      return [];
    }
  }

  createForm() {
    this.form = this.formBuilder.group({
      id: [this.current ? this.current.id : null],
      activeNodes: this.formBuilder.array(this.createFormActiveNodes()),
      dateActivePage: [
        this.current ? this.current.dateActivePage : null,
        Validators.required,
      ],
      dateDownPage: [
        this.current ? this.current.dateDownPage : null,
        Validators.required,
      ],
    });
  }


  save() {
    super.save();
    if (this.form.valid) {
      const formObj = this.form.getRawValue();
      this.generalService[this.current && this.current.id ? "update" : "create"](formObj ).subscribe(
        (data) => {
          this.toastrService.success(
            "Configuración General " + (this.current && this.current.id
              ? "actualizada"
              :  "creada" ) + " correctamente",
            "Guardando"
          );
          window.location.reload();
        },
        (error) => {
          this.errorMessage= error;
          this.toastrService.error(
            "No guardado",
            "La configuración no pudo ser " + (this.current && this.current.id
              ? "actualizada."
              : "creada.")
          );
        }
      );
    }
  }

  // open modal
  open(): void {
    super.open();
    this.configGeneralModal.show();
  }

  // close modal
  close(): void {
    super.close();
    this.configGeneralModal.hide();
  }
}
