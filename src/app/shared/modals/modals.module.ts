import { NgModule } from "@angular/core";
import { NgxMaterialTimepickerModule } from "ngx-material-timepicker";
import { CommonModule, DatePipe, KeyValuePipe } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { MDBBootstrapModule } from "angular-bootstrap-md";
import { MaterialModule } from "src/app/material.module";

import { CartHistoryModalComponent } from "./cart-history-modal/cart-history-modal.component";
import { CartModalComponent } from "./cart-modal/cart-modal.component";
import { ContactModalComponent } from "./contact-modal/contact-modal.component";
import { DialogModalComponent } from "./dialog-modal/dialog-modal.component";
import { GalleryModalComponent } from "./gallery-modal/gallery-modal.component";
import { LocationModalComponent } from "./location-modal/location-modal.component";
import { LoginModalComponent } from "./login-modal/login-modal.component";
import { PasswordModalComponent } from "./password-modal/password-modal.component";
import { ChangePasswordModalComponent } from "./change-password-modal/change-password-modal.component";
import { ProductDetailModalComponent } from "./product-detail-modal/product-detail-modal.component";
import { ProfileModalComponent } from "./profile-modal/profile-modal.component";
import { MapComponent } from "./location-modal/map/map.component";

import { AuthService } from "../services/auth.service";
import { ModalService } from "../services/modal.service";
import { ProductService } from "../services/product.service";
import { UserService } from "../services/user.service";
import { ConfigUserModalComponent } from "./config-user-modal/config-user-modal.component";
import { ConfigCartModalComponent } from "./config-cart-modal/config-cart-modal.component";
import { ConfigBannerModalComponent } from "./config-banner-modal/config-banner-modal.component";
import { ConfigNewsModalComponent } from "./config-news-modal/config-news-modal.component";
import { ConfigCategoryModalComponent } from "./config-category-modal/config-category-modal.component";
import { ConfigGeneralModalComponent } from "./config-general-modal/config-general-modal.component";
import { ConfigNodeModalComponent } from './config-node-modal/config-node-modal.component';
import { ConfigProducerModalComponent } from './config-producer-modal/config-producer-modal.component';
import { ConfigUnitModalComponent } from './config-unit-modal/config-unit-modal.component';
import { ConfigPersonalModalComponent } from './config-personal-modal/config-personal-modal.component';

import { BannerService } from "../services/banner.service";
import { YouTubePlayerModule } from '@angular/youtube-player';
import { ConfigProductModalComponent } from './config-product-modal/config-product-modal.component';
import { ChipsSelectComponent } from './chips-select/chips-select.component';
import { ConfigStaffModalComponent } from './config-staff-modal/config-staff-modal.component';
import { ConfigTagModalComponent } from './config-tag-modal/config-tag-modal.component';
import { ConfigExpenseModalComponent } from './config-expense-modal/config-expense-modal.component';
import { ConfigPurchaseModalComponent } from './config-purchase-modal/config-purchase-modal.component';
import { LoadingComponent } from './loading/loading.component';
import { NumericDirective } from '../directives/numeric.directive';
import { AddressDirective } from '../directives/address.directive';
import { NgxNumberSpinnerModule } from "ngx-number-spinner";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MDBBootstrapModule.forRoot(),
    MaterialModule,
    NgxMaterialTimepickerModule.setLocale("es-AR"),
    YouTubePlayerModule,
		NgxNumberSpinnerModule
  ],
  declarations: [
    CartHistoryModalComponent,
    CartModalComponent,
    ChipsSelectComponent,
    ContactModalComponent,
    DialogModalComponent,
    GalleryModalComponent,
    LocationModalComponent,
    LoginModalComponent,
    MapComponent, //Export?
    PasswordModalComponent,
    ChangePasswordModalComponent,
    ProductDetailModalComponent,
    ProfileModalComponent,
    ConfigBannerModalComponent,
    ConfigCartModalComponent,
    ConfigCategoryModalComponent,
    ConfigExpenseModalComponent,
    ConfigGeneralModalComponent,
    ConfigNewsModalComponent,
    ConfigNodeModalComponent,
    ConfigProducerModalComponent,
    ConfigProductModalComponent,
    ConfigPurchaseModalComponent,
    ConfigStaffModalComponent,
    ConfigTagModalComponent,
    ConfigUserModalComponent,
    ConfigUnitModalComponent,
    ConfigPersonalModalComponent,

    LoadingComponent,
    NumericDirective,
    AddressDirective
  ],
  providers: [
    DatePipe,
    KeyValuePipe,
    AuthService,
    ModalService,
    ProductService,
    BannerService,
    UserService,
  ],
  exports: [
    CartHistoryModalComponent,
    CartModalComponent,
    ContactModalComponent,
    DialogModalComponent,
    GalleryModalComponent,
    LocationModalComponent,
    LoginModalComponent,
    PasswordModalComponent,
    ChangePasswordModalComponent,
    ProductDetailModalComponent,
    ProfileModalComponent,
    ConfigBannerModalComponent,
    ConfigCartModalComponent,
    ConfigCategoryModalComponent,
    ConfigExpenseModalComponent,
    ConfigGeneralModalComponent,
    ConfigNewsModalComponent,
    ConfigNodeModalComponent,
    ConfigProducerModalComponent,
    ConfigProductModalComponent,
    ConfigPurchaseModalComponent,
    ConfigStaffModalComponent,
    ConfigTagModalComponent,
    ConfigUserModalComponent,
    ConfigUnitModalComponent,
    ConfigPersonalModalComponent,
    AddressDirective,
    LoadingComponent,
  ],
  entryComponents: [],
})
export class ModalsModule {}
