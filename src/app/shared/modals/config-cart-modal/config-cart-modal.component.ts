import { Component, ViewChild, ElementRef, OnInit } from "@angular/core";
import { ModalService } from "src/app/shared/services/modal.service";
import { CartService } from "src/app/shared/services/cart.service";
import { ToastrService } from "ngx-toastr";
import { GeneralService } from "../../services/general.service";
import { DateTimeNode } from "../../models/general";
import { ConfigBaseModalComponent } from "../config-base-modal/config-base-modal.component";

@Component({
  selector: "app-config-cart-modal",
  templateUrl: "./config-cart-modal.component.html",
  styleUrls: ["./config-cart-modal.component.scss"],
})
export class ConfigCartModalComponent extends ConfigBaseModalComponent
  implements OnInit {
  @ViewChild("configCartModal") configCartModal: any;
  nodes: DateTimeNode[] = [];
  idGeneral: number = null;

  constructor(
    private generalService: GeneralService,
    private cartService: CartService,
    private toastrService: ToastrService,
    modalService: ModalService,
    el: ElementRef
  ) {
    super(modalService, el);
    this.generalService.getActive().subscribe((result) => {
      this.idGeneral = result ? result.id : null;
    });
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.getNodes();
  }

  getNodes() {
    // this.spinnerService.show();
    let withFridge = this.current
      ? this.current.cartProducts.some((item) => item.product.needCold && !item.isCanceled)
      : null;

    if (this.idGeneral) {
      this.generalService
        .getGeneralNodes(this.idGeneral, withFridge)
        .subscribe((result) => {
          this.nodes = result;
        });
    }
  }

  loadData(): void {
    super.loadData();
    if(this.idElement){
      this.cartService.getById(this.idElement).subscribe((result) => {
        this.current = result;
      });
    }
  }
  updateList(id: number, property: string, event: any) {
    if (event.target.value === "" || event.target.value < "1") {
      this.current.cartProducts[id][property] = "1";
    } else {
      if (event.target.value > this.current.cartProducts[id].product.stock) {
        this.current.cartProducts[id][property] = this.current.cartProducts[
          id
        ].product.stock;
      } else {
        this.current.cartProducts[id][property] = event.target.value;
      }
    }
  }

  removeRestoreItem(idx: number) {
    if (this.current.cartProducts[idx].isCanceled) {
      this.current.cartProducts[idx].isCanceled = false;
      this.current.cartProducts[idx].quantity = 1;
    } else {
      this.current.cartProducts[idx].isCanceled = true;
      this.current.cartProducts[idx].quantity = 0;
    }
  }

  getTotal() {
    return this.current.cartProducts.reduce(
      (acc, cur) => acc + cur.quantity * cur.price,
      0
    );
  }

  getTotalWithDiscount(){
    return (this.getTotal() - this.getPersonalDiscount())
  }

  getPersonalDiscount(){
    var temp = 0;
    if (this.isPersonal()) {
      temp = this.current.cartProducts.reduce(
        (acc, cur) => acc + cur.quantity * (cur.product.price - cur.product.buyPrice),0,
        0
      )
    }
    return temp
  }

  isPersonal():boolean{
    return this.current.staffDiscount;
  }

  save() {
    super.save();
    this.cartService.update(this.current).subscribe(
      (data) => {
        this.toastrService.success(
          "Compra actualizada correctamente",
          "Actualizando"
        );
        window.location.reload();
      },
      (error) => {
        this.toastrService.error(
          "No actualizada",
          "La compra no pudo ser actualizada."
        );
      }
    );
  }

  // open modal
  open(): void {
    super.open();
    this.configCartModal.show();
  }

  // close modal
  close(): void {
    super.close();
    this.configCartModal.hide();
  }
}
