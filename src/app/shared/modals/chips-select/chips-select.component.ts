import {
  Component,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  OnChanges,
  OnInit,
} from "@angular/core";

/**
 * @title Chips Autocomplete
 */
@Component({
  selector: "app-chips-select",
  templateUrl: "chips-select.component.html",
  styleUrls: ["chips-select.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChipsSelectComponent implements OnInit, OnChanges {
  @Input() elementsSelected: any[] = [];
  @Input() allElements: any[];
  @Input() field: string = "name";
  @Input() readonly = false;
  @Input() invalid = false;
  @Input() placeholder: string = "Seleccione...";

  @Output() onSelected = new EventEmitter();

  visible = true;
  removable = true;

  constructor() {}

  ngOnInit() {}

  ngOnChanges() {
    let selectedsId = this.elementsSelected.map((es) => es.id);
    this.elementsSelected = this.allElements.filter((ae) =>
      selectedsId.includes(ae.id)
    );
  }

  remove(index) {
    this.onSelected.emit(this.elementsSelected.filter((_, i) => i !== index));
  }

  selected(): void {
    this.onSelected.emit(this.elementsSelected);
  }
}
