
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Component, OnInit, Input, ViewChild, ElementRef } from "@angular/core";
import { ModalService } from '../../services/modal.service';
declare var $: any;
@Component({
	selector: "app-contact-modal",
	templateUrl: "./contact-modal.component.html",
	styleUrls: ["./contact-modal.component.scss"]
})
export class ContactModalComponent implements OnInit {
	@Input() id: string;
	@ViewChild('contactModal') contactModal: any;
	private element: any;

	contactForm: FormGroup;
	submittedContact = false;
	available = false;

	constructor(
		private modalService: ModalService,
		private formBuilder: FormBuilder,
		el: ElementRef) {
		this.element = el.nativeElement;
	}

    get name() { return this.contactForm.get('name'); };
    get email() { return this.contactForm.get('email'); };
    get subject() { return this.contactForm.get('subject'); };
    get comment() { return this.contactForm.get('comment'); };

	ngOnInit(): void {
		let modal = this;

		// ensure id attribute exists
		if (!this.id) {
			console.error('modal must have an id');
			return;
		}
		// add self (this modal instance) to the modal service so it's accessible from controllers
		this.modalService.add(this);

		this.contactForm = this.formBuilder.group({
			name: [null, Validators.required],
			email: [null, [Validators.required, Validators.email,
        Validators.pattern(
          /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        )]],
			subject: [null, Validators.required],
			comment: [null, Validators.required]
		});
	}

	sendContact() {
		/*
		this.submittedLogin = true;
		if (this.loginForm.valid) {
			this.authService.login(this.loginForm.value["email"], this.loginForm.value["loginPassword"])
				.pipe(first())
				.subscribe(
					data => {
						this.toastrService.success("Ingreso correctamente", "Ingresando");

						const returnUrl = this.route.snapshot.queryParamMap.get("returnUrl");
						setTimeout((router: Router) => {
							this.close();
							this.router.navigate([returnUrl || "/"]);
						}, 1500);

						this.router.navigate(["/"]);
					},
					error => {
						this.errorLoginRegister = true;
						this.errorMessage = "Credenciales Invalidas, Por favor, checkear los datos ingresados.";
						this.cd.detectChanges();
						this.toastrService.error("No autenticado", "Credenciales Invalidas, Por favor, checkear los datos ingresados.");
					});
		}
		*/
		console.log('send');
	}


	// remove self from modal service when directive is destroyed
	ngOnDestroy(): void {
		this.modalService.remove(this.id);
		this.element.remove();
	}

	// open modal
	open(): void {
		this.available= true;
		this.contactForm.markAsPristine();
		this.contactForm.markAsUntouched();
		this.contactForm.reset();
		this.submittedContact = false;
		this.contactModal.show();
	}

	// close modal
	close(): void {
		this.available= false;
		this.contactModal.hide()
	}

}
