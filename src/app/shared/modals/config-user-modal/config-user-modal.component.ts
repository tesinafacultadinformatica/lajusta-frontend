import { Component, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ModalService } from 'src/app/shared/services/modal.service';
import { UserService } from 'src/app/shared/services/user.service';
import { ToastrService } from 'ngx-toastr';
import { ConfigBaseModalComponent } from '../config-base-modal/config-base-modal.component';
import { Role } from '../../enums/role.enum';

@Component({
  selector: 'app-config-user-modal',
  templateUrl: './config-user-modal.component.html',
  styleUrls: ['./config-user-modal.component.scss']
})
export class ConfigUserModalComponent extends ConfigBaseModalComponent {
  @ViewChild('configUserModal') configUserModal: any;

  roles = Role;

  constructor(
    private formBuilder: FormBuilder,
		private userService: UserService,
		private toastrService: ToastrService,
    modalService: ModalService,
    el: ElementRef) {
    super(modalService,el);
  }

  get email() { return this.form.get('email'); };

  loadData(): void {
    super.loadData();
    if (this.idElement) {
      this.userService.getById(this.idElement).subscribe(
        (result) => {
          this.current = result;
          this.createForm();
        });
    }else{
      this.current = null;
      this.createForm();
    }
  }

  createForm() {
    this.form = this.formBuilder.group({
      id: [this.current ? this.current.id : null, ],
      firstName: [this.current ? this.current.firstName : null, Validators.required],
      lastName: [this.current ? this.current.lastName : null, Validators.required],
      role: [this.current ? this.current.role.toString() : null, Validators.required],
      email: [this.current ? this.current.email : null, [Validators.required, Validators.email,
        Validators.pattern(
          /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        )]],
      description: [this.current ? this.current.description : null],
      phone: [this.current ? this.current.phone : null, Validators.required],
      address:this.formBuilder.group({
        street: [this.current ? this.current.address.street : null, Validators.required],
        number: [this.current ? this.current.address.number : null, Validators.required],
        apartment: [this.current ? this.current.address.apartment : null],
        floor: [this.current ? this.current.address.floor : null],
        description: [this.current ? this.current.address.description : null],
        latitude: [this.current ? this.current.address.latitude : null],
        longitude: [this.current ? this.current.address.longitude : null],
      })
    });

  }

  save() {
    super.save();
		if (this.form.valid) {
      const formObj = this.form.getRawValue();
			this.userService.update(formObj)
				.subscribe(
					data => {
						this.toastrService.success("Usuario actualizado correctamente", "Actualizando");
            window.location.reload();
					},
					error => {
						this.toastrService.error("No actualizado", "El usuario no pudo ser actualizado.");
					});
		}
  }

  // open modal
  open(): void {
    super.open();
    this.configUserModal.show();
  }

  // close modal
  close(): void {
		super.close();
    this.configUserModal.hide()
  }

}


