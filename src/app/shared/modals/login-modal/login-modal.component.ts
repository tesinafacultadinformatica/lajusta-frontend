import {
  EmailValidator,
  FormGroup,
  FormBuilder,
  Validators,
} from "@angular/forms";
import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Input,
  ViewChild,
  ElementRef,
  Output,
  EventEmitter,
} from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { first } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";
import { MustMatch } from "src/app/shared/helper/validator/must-match.validator";
import { AuthService } from "../../services/auth.service";
import { UserService } from "../../services/user.service";
import { ModalService } from "../../services/modal.service";
import { Image } from '../../models/image';
declare var $: any;
@Component({
  selector: "app-login-modal",
  templateUrl: "./login-modal.component.html",
  styleUrls: ["./login-modal.component.scss"],
  providers: [EmailValidator],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginModalComponent implements OnInit {
  @Input() id: string;
  private element: any;
  @ViewChild("loginModal") loginModal: any;
  @Output() openPassword = new EventEmitter();

  available = false;
  tabRegisterActive = false;

  errorLoginRegister = false;
  errorMessage: any;
  loginForm: FormGroup;
  submittedLogin = false;
  submittedRegister = false;
  userForm: FormGroup;
  image: Image = null;
  loading = false;
  matcher = false;

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private toastrService: ToastrService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private modalService: ModalService,
    private cd: ChangeDetectorRef,
    el: ElementRef
  ) {
    this.element = el.nativeElement;
  }

  get firstName() {
    return this.userForm.get("firstName");
  }
  get lastName() {
    return this.userForm.get("lastName");
  }
  get email() {
    return this.userForm.get("email");
  }
  get encryptedPassword() {
    return this.userForm.get("encryptedPassword");
  }
  get passwordConfirmation() {
    return this.userForm.get("passwordConfirmation");
  }
  get phone() {
    return this.userForm.get("phone");
  }
  get addressStreet() {
    return this.userForm.get("address.street");
  }
  get addressNumber() {
    return this.userForm.get("address.number");
  }

  ngOnInit(): void {
    let modal = this;

    // ensure id attribute exists
    if (!this.id) {
      console.error("modal must have an id");
      return;
    }
    // add self (this modal instance) to the modal service so it's accessible from controllers
    this.modalService.add(this);

    this.loginForm = this.formBuilder.group({
      email: [
        null,
        [
          Validators.required,
          Validators.email,
          Validators.pattern(
            /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
          ),
        ],
      ],
      password: [null, Validators.required],
    });

    this.userForm = this.formBuilder.group(
      {
        firstName: [null, Validators.required],
        lastName: [null, Validators.required],
        email: [
          null,
          [
            Validators.required,
            Validators.email,
            Validators.pattern(
              /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            ),
          ],
        ],
        description: [null],
        image: [null],
        encryptedPassword: [
          null,
          [Validators.required, Validators.minLength(6)],
        ],
        passwordConfirmation: [null, Validators.required],
        phone: [null, Validators.required],
        address: this.formBuilder.group({
          street: [null, Validators.required],
          number: [null, Validators.required],
          apartment: [null],
          floor: [null],
          description: [null],
          latitude: [null],
          longitude: [null],
        }),
      },
      {
        validator: MustMatch("encryptedPassword", "passwordConfirmation"),
      }
    );
  }

  // remove self from modal service when directive is destroyed
  ngOnDestroy(): void {
    this.modalService.remove(this.id);
    this.element.remove();
  }

  // open modal
  open(): void {
    this.available = true;
    this.userForm.markAsPristine();
    this.userForm.markAsUntouched();
    this.userForm.reset();
    this.loginForm.markAsPristine();
    this.loginForm.markAsUntouched();
    this.loginForm.reset();
    this.image = null;
    this.tabRegisterActive = false;
    this.errorLoginRegister = false;
    this.submittedLogin = false;
    this.submittedRegister = false;
    this.cd.detectChanges();
    this.loginModal.show();
  }

  // close modal
  close(): void {
    this.available = false;
    this.loginModal.hide();
  }

  openPasswordModal() {
    this.openPassword.emit();
  }

  changeTab(tab: boolean){
    this.tabRegisterActive = tab;
    this.errorLoginRegister = false;
  }

  onFileChange(event) {
    const file = event.target.files[0];
    if(file){
      if (/\.(jpe?g|png|gif|bmp)$/i.test(file.name)) {
        if (file.size > 2097152) {
          alert("El archivo excede los 2MB!");
          this.image = null;
        } else {
          this.loading=true;
          this.image = new Image();

          const title = this.userForm.controls["firstName"].value;
          this.image.name = (title ? title : 'User') + '_' + new Date().getTime();

          const documentType = /[^.]*$/.exec(file.name)[0].toUpperCase();
          this.image.type=documentType;

          const reader = new FileReader();
          reader.readAsDataURL(file);
          reader.onload = (e) => {
            this.image.value = reader.result;
            this.loading = false;
          };

          this.userForm.controls["image"].patchValue(this.image);
        }
      } else {
        alert(
          "La extension de la imagen debe ser de tipo jpg, jpeg, png, gif o bmp"
        );
        this.image = null;
      }
    } else {
      this.image = null;
    }
  }

  addUser() {
    this.submittedRegister = true;
    if (this.userForm.valid) {
      let formObj = this.userForm.getRawValue();
      this.userService.register(formObj).subscribe(
        (res) => {
          this.toastrService.success("Registrando", "Registrando Usuario");

          setTimeout((router: Router) => {
            this.close();
            this.router.navigate(["/"]);
          }, 1500);
        },
        (error) => {
          this.errorLoginRegister = true;
          this.errorMessage = error;
          this.cd.detectChanges();
          this.toastrService.error("Error Creando el usuario", error);
        }
      );
    }
  }

  signInWithEmail() {
    this.submittedLogin = true;
    if (this.loginForm.valid) {
      this.authService
        .login(this.loginForm.value["email"], this.loginForm.value["password"])
        .pipe(first())
        .subscribe(
          (data) => {
            this.toastrService.success("Ingreso correctamente", "Ingresando");

            const returnUrl = this.route.snapshot.queryParamMap.get(
              "returnUrl"
            );
            setTimeout((router: Router) => {
              this.close();
              this.router.navigate([returnUrl || "/"]);
            }, 1500);

            this.router.navigate(["/"]);
          },
          (error) => {
            this.errorLoginRegister = true;
            this.errorMessage =
              "Credenciales Invalidas, Por favor, checkear los datos ingresados.";
            this.cd.detectChanges();
            this.toastrService.error(
              "No autenticado",
              "Credenciales Invalidas, Por favor, checkear los datos ingresados."
            );
          }
        );
    }
  }
}
