import { Component, OnInit, ViewChild, ElementRef, ChangeDetectionStrategy, Input } from '@angular/core';
import Mapboxgl from 'mapbox-gl';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MapComponent implements OnInit {
    @Input() long: number;
	@Input() lat: number;
    map: Mapboxgl.Map;
    @ViewChild('mapElement') mapElement: ElementRef;

    constructor() { }

    ngOnInit() {
        Mapboxgl.accessToken = environment.mapboxKey;
    }

    ngAfterViewInit(){
       this.changeMap();
    }   

    changeMap(){        
        this.map = new Mapboxgl.Map({
            container: this.mapElement.nativeElement,
            style: 'mapbox://styles/mapbox/streets-v9',
            center: [this.long, this.lat], // long, lat -57.94142851184983, -34.92297879916583
            zoom: 15
        });
        const marker = new Mapboxgl.Marker()
			.setLngLat([this.long, this.lat])
            .addTo(this.map);
            this.map.on('load', () => {
                this.map.resize();
        });  
    }

    ngOnChanges(): void {
        if(this.mapElement){
            this.map.remove();
            this.changeMap();
        }
    }
}