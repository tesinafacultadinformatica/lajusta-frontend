import { Node } from '../../models/node';
import { Component, OnInit, OnDestroy, ViewChild, Input, ElementRef, ChangeDetectionStrategy } from '@angular/core';
import { ModalService } from '../../services/modal.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
	selector: 'app-location-modal',
	templateUrl: './location-modal.component.html',
	styleUrls: ['./location-modal.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class LocationModalComponent implements OnInit, OnDestroy {
	@Input() id: string;
	@Input() node: Node;
	private element: any;
	@ViewChild('locationModal') locationModal: any;

	available = false;

	constructor(
		private modalService: ModalService,
		private domSanitizer : DomSanitizer,
		el: ElementRef
	) {
		this.element = el.nativeElement;
	}

	
	ngOnInit(): void {
		let modal = this;

		// ensure id attribute exists
		if (!this.id) {
			console.error('modal must have an id');
			return;
		}
		// add self (this modal instance) to the modal service so it's accessible from controllers
		this.modalService.add(this);
		
	}


	// open modal
	open(): void {
		this.available= true;
		this.locationModal.show();
	}

	// close modal
	close(): void {
		this.available= false;
		this.locationModal.hide()
	}

	ngOnDestroy() {
		this.modalService.remove(this.id);
		this.element.remove();
	}
}
