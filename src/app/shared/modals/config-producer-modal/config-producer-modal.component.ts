import { Component, ViewChild, ElementRef } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { ModalService } from "src/app/shared/services/modal.service";
import { ProducerService } from "src/app/shared/services/producer.service";
import { ToastrService } from "ngx-toastr";
import { ConfigBaseModalComponent } from "../config-base-modal/config-base-modal.component";
import { Image } from "../../models/image";
import { Tag } from '../../models/tag';
import { TagService } from '../../services/tag.service';

@Component({
  selector: "app-config-producer-modal",
  templateUrl: "./config-producer-modal.component.html",
  styleUrls: ["./config-producer-modal.component.scss"],
})
export class ConfigProducerModalComponent extends ConfigBaseModalComponent {
  @ViewChild("configProducerModal") configProducerModal: any;

  currentImages: Image[];
  tagsList: Tag[];
  loading: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private producerService: ProducerService,
    private tagService: TagService,
    private toastrService: ToastrService,
    modalService: ModalService,
    el: ElementRef
  ) {
    super(modalService, el);
  }

  get tags() {
    return this.form.controls["tags"];
  }

  get email() {
    return this.form.get("email");
  }
  get youtubeVideoIdValue() {
    return this.form.get("youtubeVideoId").value;
  }

  loadData(): void {
    super.loadData();
    this.tagService.getAll().subscribe((result: Tag[]) => {
      this.tagsList = result;
    });
    if (this.idElement) {
      this.producerService.getById(this.idElement).subscribe((result) => {
        this.current = result;
        this.createForm();
        this.currentImages = this.current.images;
        this.changeSelectedTag(this.current.tags);
      });
    } else {
      this.current = null;
      this.currentImages = [];
      this.createForm();
    }
  }

  createForm() {
    this.form = this.formBuilder.group({
      id: [this.current ? this.current.id : null],
      name: [this.current ? this.current.name : null, Validators.required],
      origin: [this.current ? this.current.origin : null],
      address: this.formBuilder.group({
        street: [this.current ? this.current.address.street : null],
        number: [this.current ? this.current.address.number : null],
        apartment: [this.current ? this.current.address.apartment : null],
        floor: [this.current ? this.current.address.floor : null],
        description: [this.current ? this.current.address.description : null],
        latitude: [this.current ? this.current.address.latitude : null],
        longitude: [this.current ? this.current.address.longitude : null],
      }),
      phone: [this.current ? this.current.phone : null, Validators.required],
      tags: [
        this.current ? this.current.tags : [],
        Validators.required,
      ],
      email: [
        this.current ? this.current.email : null,
        [
          Validators.email,
          Validators.pattern(
            /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
          ),
        ],
      ],
      description: [this.current ? this.current.description : null],
      images: [this.current ? this.current.images : null],
      youtubeVideoId: [this.current ? this.current.youtubeVideoId : null],
      isCompany: [this.current ? this.current.isCompany : false],
    });
  }

  readURL(event: any): void {
    const total = event.target.files.length;
    let loaded = 0;
    for (var i = 0; i < total; i++) {
      // this.files.push(event.target.files[i]);
      const file = event.target.files[i];
      if (/\.(jpe?g|png|gif|bmp)$/i.test(file.name)) {
        if (file.size > 2097152) {
          loaded++;
          alert("¡El archivo " + file.name + " excede los 2MB!");
        } else {
          this.loading = true;
          const img = new Image();

          const title = this.form.controls["name"].value;
          img.name = (title ? title : "Produces") + "_" + new Date().getTime();

          const documentType = /[^.]*$/.exec(file.name)[0].toUpperCase();
          img.type = documentType;

          const reader = new FileReader();
          reader.readAsDataURL(file);

          reader.onload = (e) => {
            img.value = reader.result;
            if(this.currentImages.length === 0){
              img.isMain = true;
            }
            this.currentImages.push(img);

            loaded++;
            if (loaded === total){
              this.loading = false;
            }
          };

          this.form.controls["images"].patchValue(this.currentImages);
        }
      } else {
        loaded++;
        alert(
          "La extension de la imagen " +
            file.name +
            " debe ser de tipo jpg, jpeg, png, gif o bmp"
        );
      }
    }
  }

  selectImageMain(i:number){
    const index = this.currentImages.findIndex(img => img.isMain);
    if(index !== i){
      this.currentImages[index].isMain = false;
      this.currentImages[i].isMain = true;
      this.form.controls["images"].patchValue(this.currentImages);
    }
  }

  removeImage(img) {
    const index = this.currentImages.indexOf(img);

    if (index >= 0) {
      this.currentImages.splice(index, 1);
      this.form.controls["images"].patchValue(this.currentImages);
    }
  }

  save() {
    super.save();
    if (this.form.valid) {
      const formObj = this.form.getRawValue();
      this.producerService[this.idElement ? "update" : "create"](
        formObj
      ).subscribe(
        (data) => {
          this.toastrService.success(
            "Productor " + (!this.idElement
              ? "creado"
              : "actualizado") + " correctamente",
            "Guardando"
          );
          window.location.reload();
        },
        (error) => {
          this.toastrService.error(
            "No guardado",
            "El productor no pudo ser " +(!this.idElement
              ? "creado"
              : "actualizado")
          );
        }
      );
    }
  }

  changeSelectedTag(tags: Tag[]) {
    this.form.controls["tags"].patchValue(tags);
  }

  // open modal
  open(): void {
    super.open();

    this.configProducerModal.show();
  }

  // close modal
  close(): void {
    super.close();
    this.configProducerModal.hide();
  }
}
