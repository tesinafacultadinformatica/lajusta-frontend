import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import {
  Component,
  OnInit,
  Input,
  ViewChild,
  ElementRef,
  OnChanges,
  SimpleChanges,
} from "@angular/core";
import { ModalService } from "../../services/modal.service";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { MustMatch } from "../../helper/validator/must-match.validator";
import { UserService } from "../../services/user.service";
declare var $: any;
@Component({
  selector: "app-change-password-modal",
  templateUrl: "./change-password-modal.component.html",
  styleUrls: ["./change-password-modal.component.scss"],
})
export class ChangePasswordModalComponent implements OnInit, OnChanges {
  @Input() id: string;
  @Input() code: string = null;
  private element: any;
  @ViewChild("changePasswordModal") changePasswordModal: any;

  available = false;

  changePasswordForm: FormGroup;
  errorChange = false;
  errorMessage: any;
  submitted = false;

  constructor(
    private modalService: ModalService,
    private formBuilder: FormBuilder,
    private userService: UserService,

    private router: Router,
    private toastrService: ToastrService,
    el: ElementRef
  ) {
    this.element = el.nativeElement;
  }

  get email() {
    return this.changePasswordForm.get("email");
  }
  get oldPassword() {
    return this.changePasswordForm.get("oldPassword");
  }
  get newPassword() {
    return this.changePasswordForm.get("newPassword");
  }
  get passwordRepeat() {
    return this.changePasswordForm.get("passwordRepeat");
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.code.previousValue != changes.code.currentValue){
      if (this.changePasswordForm) {
        this.changePasswordForm.reset();
        this.changePasswordForm.removeControl("oldPassword");
        this.changePasswordForm.removeControl("code");
        this.changePasswordForm.addControl(
          this.code ? "code" : "oldPassword",
          this.code
            ? this.formBuilder.control(this.code)
            : this.formBuilder.control(null, [
                Validators.required,
                Validators.minLength(6),
              ])
        ); // Add new form control
      }
    }
  }

  ngOnInit(): void { 
    let modal = this;

    // ensure id attribute exists
    if (!this.id) {
      console.error("modal must have an id");
      return;
    }
    // add self (this modal instance) to the modal service so it's accessible from controllers
    this.modalService.add(this);

    this.changePasswordForm = this.formBuilder.group(
      {
        email: [
          null,
          [
            Validators.required,
            Validators.email,
            Validators.pattern(
              /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            ),
          ],
        ],
        oldPassword: [null, [Validators.required, Validators.minLength(6)]],
        newPassword: [null, [Validators.required, Validators.minLength(6)]],
        passwordRepeat: [null, Validators.required],
      },
      {
        validator: MustMatch("newPassword", "passwordRepeat"),
      }
    );
  }

  // remove self from modal service when directive is destroyed
  ngOnDestroy(): void {
    this.modalService.remove(this.id);
    this.element.remove();
  }

  changePass() {
    this.submitted = true;
    if (this.changePasswordForm.valid) {
      let formObj = this.changePasswordForm.getRawValue();
      this.userService.changePassword(formObj).subscribe(
        (res) => {
          this.toastrService.success(
            "Cambió de Contraseña",
            "Se cambio con exito"
          );

          setTimeout((router: Router) => {
            this.close();
          }, 1500);
        },
        (error) => {
          this.errorChange = true;
          this.errorMessage = error;
          this.toastrService.error(
            "Error al querer cambiar la contraseña",
            error
          );
        }
      );
    }
  }

  // open modal
  open(): void {
    this.available = true;
    this.changePasswordModal.show();
    this.submitted = false;
   
  }

  // close modal
  close(): void {
    this.available = false;
    this.changePasswordModal.hide();
    if(this.code){
      this.code = null;
      this.router.navigate(["/"]);
    }
  }
}
