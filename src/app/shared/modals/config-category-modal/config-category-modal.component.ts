import { Component, ViewChild, ElementRef } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { ModalService } from "src/app/shared/services/modal.service";
import { CategoryService } from "src/app/shared/services/category.service";
import { Category } from "src/app/shared/models/category";
import { ToastrService } from "ngx-toastr";
import { ConfigBaseModalComponent } from "../config-base-modal/config-base-modal.component";
import { Image } from "../../models/image";

@Component({
  selector: "app-config-category-modal",
  templateUrl: "./config-category-modal.component.html",
  styleUrls: ["./config-category-modal.component.scss"],
})
export class ConfigCategoryModalComponent extends ConfigBaseModalComponent {
  @ViewChild("configCategoryModal") configCategoryModal: any;

  categories: Category[];
  currentImage: Image = null;
  loading: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private categoryService: CategoryService,
    private toastrService: ToastrService,
    modalService: ModalService,
    el: ElementRef
  ) {
    super(modalService, el);
  }

  loadData(): void {
    super.loadData();
    this.categoryService.getFullAll().subscribe((result) => {
      this.categories = result.filter((cat) => cat.id != null);
    });
    if (this.idElement) {
      this.categoryService.getById(this.idElement).subscribe((result) => {
        this.current = result;
        this.createForm();
        this.currentImage = this.current.image;
      });
    } else {
      this.current = null;
      this.currentImage = null;
      this.createForm();
    }
  }

  createForm() {
    this.form = this.formBuilder.group({
      id: [this.current ? this.current.id : null],
      name: [this.current ? this.current.name : null, Validators.required],
      parent: this.formBuilder.group({
        id: [this.current && this.current.parent ? this.current.parent.id : null]
      }),
      image: [this.current ? this.current.image : null],
    });
  }

  getSrcImage() {
    return this.currentImage ? this.currentImage.value : null;
  }

  readURL(event: any): void {
    const file = event.target.files[0];
    if (file) {
      if (/\.(jpe?g|png|gif|bmp)$/i.test(file.name)) {
        if (file.size > 2097152) {
          alert("El archivo excede los 2MB!");
          this.currentImage = null;
        } else {
          this.loading = true;
          this.currentImage = new Image();

          const title = this.form.controls["name"].value;
          this.currentImage.name =
            (title ? title : "Category") + "_" + new Date().getTime();

          const documentType = /[^.]*$/.exec(file.name)[0].toUpperCase();
          this.currentImage.type = documentType;

          const reader = new FileReader();
          reader.readAsDataURL(file);
          reader.onload = (e) => {
            this.currentImage.value = reader.result;
            this.loading = false;
          };

          this.form.controls["image"].patchValue(this.currentImage);
        }
      } else {
        alert(
          "La extension de la imagen debe ser de tipo jpg, jpeg, png, gif o bmp"
        );
        this.currentImage = null;
      }
    } else {
      this.currentImage = null;
    }
  }

  save() {
    super.save();
    if (this.form.valid) {
      const formObj = this.form.getRawValue();
      this.categoryService[this.idElement ? "update" : "create"](
        formObj
      ).subscribe(
        (data) => {
          this.toastrService.success(
            "Categoria " + (!this.idElement
              ? "creada"
              : "actualizada") + " correctamente",
            "Guardando"
          );
          window.location.reload();
        },
        (error) => {
          this.toastrService.error(
            "No guardado",
            "La categoria no pudo ser " + (!this.idElement
              ? "creada."
              : "actualizada.")
          );
        }
      );
    }
  }

  removeImage() {
      this.currentImage = null;
      this.form.controls["images"].patchValue(null);
  }

  // open modal
  open(): void {
    super.open();
    this.configCategoryModal.show();
  }

  // close modal
  close(): void {
    super.close();
    this.configCategoryModal.hide();
  }
}
