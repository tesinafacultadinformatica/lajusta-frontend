import { Node } from "../../models/node";
import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  Input,
  ElementRef,
  EventEmitter,
  OnChanges,
  Output,
  AfterViewInit,
} from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { CartService } from "../../services/cart.service";
import { ModalService } from "../../services/modal.service";
import { CartItem, Cart } from "src/app/shared/models/cart";
import { Subscription, merge, of } from "rxjs";
import { AuthService } from "../../services/auth.service";
import { GeneralService } from "../../services/general.service";
import { DateTimeNode } from "../../models/general";
import { MatTableDataSource } from "@angular/material/table";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort, MatSortable } from '@angular/material/sort';
import { startWith, switchMap, map, catchError } from "rxjs/operators";
import { DatePipe, CurrencyPipe } from "@angular/common";
import { DialogModalComponent } from "../dialog-modal/dialog-modal.component";
import { MatDialog } from "@angular/material/dialog";
import { SubscriptionLike as ISubscription } from "rxjs";
import { addressComplete } from "../../helper/util/address-complete"
import { Pageable } from '../../models/pageable';
import { Role } from "src/app/shared/enums/role.enum"

@Component({
  selector: "app-cart-history-modal",
  templateUrl: "./cart-history-modal.component.html",
  styleUrls: ["./cart-history-modal.component.scss"],
})
export class CartHistoryModalComponent implements OnInit, OnChanges, AfterViewInit, OnDestroy {
  @Input() id: string;
  @ViewChild("cartHistoryModal") cartHistoryModal: any;
  private element: any;

  @Output() openProductDetail = new EventEmitter<number>();
  @Output() openNode = new EventEmitter<Node>();
  @Output() openLogin = new EventEmitter<number>();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild("outSort", { static: true }) sort: MatSort;
  subs: ISubscription[] = [];

  displayedColumns = [
    "id",
    "nodeDate.node.name",
    "saleDate",
    "canceled",
    "total:currency",
    "delete",
  ];
  textColumns = ['id', 'Nodo', 'Fecha Compra', 'Cancelado', 'Total']
  dataSource: MatTableDataSource<Cart>;
  pageSizeOptions = [5, 10, 25, 50];
  pageSize = 10;
  public resultsLength: number = 0;

  available = false;
  carts: Cart[] = [];
  selectedCart: any = null;
  selectedCartIsCurrentGeneral = false;

  nodes: DateTimeNode[] = [];

  editField: string;
  idGeneral: number = null;
  selectedNode: number = 1;
  public isLoadingResults: boolean = false;
  public isRateLimitReached: boolean = false;

  showAddress = false;

  constructor(
    private cartService: CartService,
    private generalService: GeneralService,
    private toastrService: ToastrService,
    private modalService: ModalService,
    private authService: AuthService,
    private datePipe: DatePipe,
    private currencyPipe: CurrencyPipe,
    public dialog: MatDialog,
    el: ElementRef
  ) {
    this.element = el.nativeElement;
    this.generalService.getActive().subscribe((result) => {
      this.idGeneral = result ? result.id : null;
      this.validateSelectedCart();
     });
  }

  ngOnChanges(): void {}

  ngOnInit(): void {
    let modal = this;

    // ensure id attribute exists
    if (!this.id) {
      console.error("modal must have an id");
      return;
    }
    // add self (this modal instance) to the modal service so it's accessible from controllers
    this.modalService.add(this);

    this.dataSource = new MatTableDataSource();
    this.loadData();
    this.subs.push(
      this.cartService.getUpdateData().subscribe((data) => {
        this.loadData();
      })
    );
  }

  private loadData() {
    this.sort.sort(({ id: 'id', start: 'desc'}) as MatSortable);
    this.cartService
      .getData(
        null,
        this.sort.active,
        this.sort.direction,
        this.paginator.pageIndex?this.paginator.pageIndex : 0,
        this.paginator.pageSize,
        [{ key: "history", value: true }],
        (total) => (this.resultsLength = total)
      )
      .subscribe((carts) => {
        this.dataSource.data = carts;
        //this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.findHighestId(carts);
      });
  }

  getNodes() {
    // this.spinnerService.show();
    if (this.selectedCartIsCurrentGeneral) {
      let withFridge = this.selectedCart.cartProducts
        ? this.selectedCart.cartProducts.some((item) => item.product.needCold)
        : null;
      if (this.idGeneral) {
        this.generalService
          .getGeneralNodes(this.idGeneral, withFridge)
          .subscribe((result) => {
            this.nodes = result;

            this.selectedNode =
              this.nodes?.length > 0 ? this.nodes[0].id : null;
          });
      }
    }
  }

  // open modal
  open(): void {
    this.available = true;
    this.cartHistoryModal.show();
  }

  // close modal
  close(): void {
    this.available = false;
    this.cartHistoryModal.hide();
  }

  /**
   * Set the paginator and sort after the view init since this component will
   * be able to query its view for the initialized paginator and sort.
   */
  ngAfterViewInit() {
    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));
    this.mergeData();
  }

  mergeData() {
    merge(this.sort.sortChange, this.paginator.page).pipe(
      startWith({}),
      switchMap(() => {
        this.isLoadingResults = true;
        return this.cartService.getData(
          null,
          this.sort.active,
          this.sort.direction,
          this.paginator.pageIndex,
          this.paginator.pageSize,
          [{ key: "history", value: true }],
          (total) => (this.resultsLength = total)
        );
      }),
      map((data) => {
        this.isLoadingResults = false;
        this.isRateLimitReached = false;
        //alternatively to response headers;
        //this.resultsLength = data.length;
        return data;
      }),
      catchError(() => {
        this.isLoadingResults = false;
        this.isRateLimitReached = true;
        return of([]);
      })
    ).subscribe(data => {this.dataSource.data = data; });
  }

  findHighestId(cartsToFind) {
    if (cartsToFind.length > 0) {
      const highestId = cartsToFind.sort(function (a, b) {
        return b.id - a.id;
      });
      this.changeSelected(highestId[0]);
    }
  }

  changeSelected(cart: Cart) {
    this.selectedCart = cart;
    this.validateSelectedCart();
  }

  validateSelectedCart(){
    this.selectedCartIsCurrentGeneral = this.selectedCart &&
      this.idGeneral === this.selectedCart.general.id;
    if (this.selectedCartIsCurrentGeneral) {
      this.selectedCart.cartProducts.forEach(
        (p) => (p.product.stock = p.product.stock + p.quantity)
      );
    }
    if(this.selectedCart){
      this.getNodes();
    }
  }

  getTotal() {
    return this.selectedCart.cartProducts.reduce(
      (acc, cur) => acc + cur.quantity * cur.product.price,
      0
    );
  }

  getTotalWithDiscount(){
    return this.getTotal() - this.getPersonalDiscount()
  }

  getPersonalDiscount(){
    var temp = 0;
    if (this.isPersonal()) {
      temp = this.selectedCart.cartProducts.reduce(
        (acc, cur) => acc + cur.quantity * (cur.product.price - cur.product.buyPrice),0,
        0
      )
    }
    return temp
  }

  isPersonal():boolean{
    return this.selectedCart.staffDiscount;
  }

  openProductDetailModal(productId: number) {
    this.openProductDetail.emit(productId);
  }

  openLocationModal(node: Node) {
    this.openNode.emit(node);
  }

  getColumns(columns: any[]) {
    let aux = [...columns];
    let index = aux.findIndex((el) => el === "delete");
    if (index > -1) {
      aux.splice(index, 1);
    }
    return aux;
  }

  private getTransformValue(value: any, type: any) {
    if (typeof value === "boolean") {
      return value ? "Si" : "No";
    }
    if (type == "currency") {
      return this.currencyPipe.transform(value, "$");
    }
    return value instanceof Date
      ? this.datePipe.transform(value, "dd-MM-yyyy HH:mm")
      : value;
  }

  getValue(row: any, item: string) {
    let itemType = item.split(":");
    let itemArray = itemType[0].toString().split(".");
    if (itemArray.length == 2) {
      if (row[itemArray[0]] && row[itemArray[0]] !== undefined) {
        return this.getTransformValue(
          row[itemArray[0]][itemArray[1]],
          itemType[1]
        );
      }
      return null;
    } else {
      if (itemArray.length == 3) {
        if (
          row[itemArray[0]] &&
          row[itemArray[0]] !== undefined &&
          row[itemArray[0]][itemArray[1]] &&
          row[itemArray[0]][itemArray[1]] !== undefined
        ) {
          return this.getTransformValue(
            row[itemArray[0]][itemArray[1]][itemArray[2]],
            itemType[1]
          );
        }
        return null;
      }
    }
    return this.getTransformValue(row[itemType[0]], itemType[1]);
  }

  confirm() {
    if (this.authService.isLoggedIn() && this.selectedCartIsCurrentGeneral) {
      this.cartService.update(this.selectedCart).subscribe(
        (result) => {
          this.showAddress = false;
          this.toastrService.success(
            "Cambios realizados!",
            "La compra fue modificada con exito."
          );
        },
        (err) => {
          this.toastrService.error(
            "Error en la modificación de la compra",
            err
          );
        }
      );
    } else {
      this.toastrService.error(
        "Debe loguearse.",
        "Por favor ingrese para modificar la compra."
      );
      this.openLogin.emit();
    }

    //this.openNodeInfo.emit(node);
  }


  removeItem(productId: number, idx: number) {
    const dialogRef = this.dialog.open(DialogModalComponent, {
      disableClose: true,
      width: "300px",
      data: { title: "¿Esta seguro que desea eliminar el producto?", description: "Esta acción no puede deshacerse." },
      });
    dialogRef.afterClosed().subscribe((result) => {
      if(result){
        let product = this.selectedCart.cartProducts.find(
          (productCart) => productCart.product.id === productId
        );
        product.isCanceled = true;
        product.quantity = 0;
        this.confirmRemove();
      }
    })
  }

  confirmRemove(){
    if (this.authService.isLoggedIn() && this.selectedCartIsCurrentGeneral) {
      this.cartService.update(this.selectedCart).subscribe(
        (result) => {
          this.toastrService.success(
            "¡Cambios realizados!",
            "El producto fue eliminado correctamente."
          );
        },
        (err) => {
          this.toastrService.error(
            "Error al eliminar el producto.",
            err
          );
        }
      );
    } else {
      this.toastrService.error(
        "Por favor ingrese para eliminar el producto de la compra."
      );
      this.openLogin.emit();
    }
  }

  restoreItem(productId: number, idx: number) {
    let product = this.selectedCart.cartProducts.find(
      (productCart) => productCart.product.id === productId
    );
    product.isCanceled = false;
    product.quantity = 1;
  }

  getNodeData(node: Node){
    return (node.name ? node.name + ' - ' : '') +  addressComplete(node.address);
  }

  updateList(id: number, property: string, event: any) {
    if (event.target.value === "" || event.target.value < "1") {
      this.selectedCart.cartProducts[id][property] = "1";
    } else {
      if (event.target.value > this.selectedCart.cartProducts[id].product.stock) {
        this.selectedCart.cartProducts[id][property] = this.selectedCart.cartProducts[id].product.stock;
      } else {
        this.selectedCart.cartProducts[id][property] = event.target.value;
      }
    }
  }

  deleteCart(cart: Cart) {
    const dialogRef = this.dialog.open(DialogModalComponent, {
      disableClose: true,
      width: "300px",
      data: { title: "¿Esta seguro?", description: "Va a cancelar la compra" },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.subs.push(
          this.cartService.delete(cart.id).subscribe(
            (data) => {
              this.toastrService.success(
                `Compra Eliminada correctamente`,
                `Se eliminó la compra`
              );
              window.location.reload();
            },
            (error) => {
              this.toastrService.error(
                "Algo salio mal",
                `No se pudo eliminarse la compra`
              );
            }
          )
        );
      }
    });
  }

  ngOnDestroy() {
    this.modalService.remove(this.id);
    this.subs.forEach((sub) => sub.unsubscribe());
    this.element.remove();
  }
}