import {
  Component,
  ViewChild,
  ElementRef
} from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { ModalService } from "src/app/shared/services/modal.service";
import { TagService } from "src/app/shared/services/tag.service";
import { ToastrService } from "ngx-toastr";
import { ConfigBaseModalComponent } from '../config-base-modal/config-base-modal.component';

@Component({
  selector: "app-config-tag-modal",
  templateUrl: "./config-tag-modal.component.html",
  styleUrls: ["./config-tag-modal.component.scss"],
})
export class ConfigTagModalComponent  extends ConfigBaseModalComponent {
  @ViewChild("configTagModal") configTagModal: any;

  constructor(
    private formBuilder: FormBuilder,
    private tagService: TagService,
    private toastrService: ToastrService,
    modalService: ModalService,
    el: ElementRef) {
    super(modalService,el);
  }

  loadData(): void {
    super.loadData();
    if (this.idElement) {
      this.tagService.getById(this.idElement).subscribe((result) => {
        this.current = result;
        this.createForm();
      });
    } else {
      this.current = null;
      this.createForm();
    }
  }

  createForm() {
    this.form = this.formBuilder.group({
      id: [this.current ? this.current.id : null],
      description: [
        this.current ? this.current.description : null,
        Validators.required,
      ],
    });
  }

  save() {
    super.save();
    if (this.form.valid) {
      const formObj = this.form.getRawValue();
      this.tagService[this.idElement ? "update" : "create"](formObj).subscribe(
        (data) => {
          this.toastrService.success(
            "Tag " + (!this.idElement
              ? "creado"
              : "actualizado") + " correctamente",
            "Guardando"
          );
          window.location.reload();
        },
        (error) => {
          this.toastrService.error(
            "No guardado",
            "El Tag no pudo ser " + (!this.idElement
              ? "creado"
              : "actualizado")
          );
        }
      );
    }
  }

  // open modal
  open(): void {
   super.open();
    this.configTagModal.show();
  }

  // close modal
  close(): void {
    super.close();
    this.configTagModal.hide();
  }
}
