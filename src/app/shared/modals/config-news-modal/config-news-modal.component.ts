import { Component, ViewChild, ElementRef } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { ModalService } from "src/app/shared/services/modal.service";
import { NewsService } from "src/app/shared/services/news.service";
import { ToastrService } from "ngx-toastr";
import { ConfigBaseModalComponent } from "../config-base-modal/config-base-modal.component";
import { Image } from "../../models/image";
import { News } from "../../models/news";

@Component({
  selector: "app-config-news-modal",
  templateUrl: "./config-news-modal.component.html",
  styleUrls: ["./config-news-modal.component.scss"],
})
export class ConfigNewsModalComponent extends ConfigBaseModalComponent {
  @ViewChild("configNewsModal") configNewsModal: any;

  currentImage: Image = null;
  loading: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private newsService: NewsService,
    private toastrService: ToastrService,
    modalService: ModalService,
    el: ElementRef
  ) {
    super(modalService, el);
  }

  loadData(): void {
    super.loadData();
    if (this.idElement) {
      this.newsService.getById(this.idElement).subscribe((result) => {
        this.current = result;
        this.createForm();
        this.currentImage = this.current.image;
      });
    } else {
      this.current = null;
      this.currentImage = null;
      this.createForm();
    }
  }

  createForm() {
    this.form = this.formBuilder.group({
      id: [this.current ? this.current.id : null],
      title: [this.current ? this.current.title : null],
      subtitle: [this.current ? this.current.subtitle : null],
      text: [this.current ? this.current.text : null],
      image: [this.current ? this.current.image : null, Validators.required],
      url: [this.current ? this.current.url : null],
    });
  }

  getSrcImage() {
    return this.currentImage ? this.currentImage.value : null;
  }

  readURL(event: any): void {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];
      if (/\.(jpe?g|png|gif|bmp)$/i.test(file.name)) {
        if (file.size > 2097152) {
          alert("El archivo excede los 2MB!");
          this.currentImage = null;
        } else {
          this.loading = true;
          this.currentImage = new Image();

          const title = this.form.controls["title"].value;
          this.currentImage.name =
            (title ? title : "News") + "_" + new Date().getTime();

          const documentType = /[^.]*$/.exec(file.name)[0].toUpperCase();
          this.currentImage.type = documentType;

          const reader = new FileReader();
          reader.readAsDataURL(file);
          reader.onload = (e) => {
            this.currentImage.value = reader.result;
            this.loading = false;
            this.form.controls["image"].patchValue(this.currentImage);
          };

        }
      } else {
        alert(
          "La extension de la imagen debe ser de tipo jpg, jpeg, png, gif o bmp"
        );
        this.currentImage = null;
      }
    }
  }

  save() {
    super.save();
    if (this.form.valid) {
      const formObj = this.form.getRawValue();
      this.newsService[this.idElement ? "update" : "create"](
        formObj
      ).subscribe(
        (data) => {
          this.toastrService.success(
            "Prensa " + (!this.idElement
              ? "creado"
              : "actualizado") +  " correctamente",
            "Guardando"
          );
          window.location.reload();
        },
        (error) => {
          this.toastrService.error(
            "No guardado",
            "La Prensa no pudo ser " + (!this.idElement
              ? "creado"
              : "actualizado")
          );
        }
      );
    }
  }

  // open modal
  open(): void {
    super.open();
    this.configNewsModal.show();
  }

  // close modal
  close(): void {
    super.close();
    this.configNewsModal.hide();
  }
}
