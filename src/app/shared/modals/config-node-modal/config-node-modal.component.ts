import { Component, ViewChild, ElementRef } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { ModalService } from "src/app/shared/services/modal.service";
import { NodeService } from "src/app/shared/services/node.service";
import { ToastrService } from "ngx-toastr";
import { ConfigBaseModalComponent } from "../config-base-modal/config-base-modal.component";

import { Node } from "src/app/shared/models/node";
import { Image } from "../../models/image";
@Component({
  selector: "app-config-node-modal",
  templateUrl: "./config-node-modal.component.html",
  styleUrls: ["./config-node-modal.component.scss"],
})
export class ConfigNodeModalComponent extends ConfigBaseModalComponent {
  @ViewChild("configNodeModal") configNodeModal: any;

  currentImage: Image = null;
  loading: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private nodeService: NodeService,
    private toastrService: ToastrService,
    modalService: ModalService,
    el: ElementRef
  ) {
    super(modalService, el);
  }

  loadData(): void {
    super.loadData();
    if (this.idElement) {
      this.nodeService.getById(this.idElement).subscribe((result) => {
        this.current = result;
        this.createForm();
        this.currentImage = this.current.image;
      });
    } else {
      this.current = null;
      this.currentImage = null;
      this.createForm();
    }
  }

  createForm() {
    this.form = this.formBuilder.group({
      id: [this.current ? this.current.id : null],
      name: [this.current ? this.current.name : null, Validators.required],
      address: this.formBuilder.group({
        street: [
          this.current ? this.current.address.street : null,
          Validators.required,
        ],
        number: [this.current ? this.current.address.number : null],
        apartment: [this.current ? this.current.address.apartment : null],
        betweenStreets: [this.current ? this.current.address.betweenStreets : null],
        floor: [this.current ? this.current.address.floor : null],
        description: [this.current ? this.current.address.description : null],
        latitude: [
          this.current ? this.current.address.latitude : null,
          Validators.required,
        ],
        longitude: [
          this.current ? this.current.address.longitude : null,
          Validators.required,
        ],
      }),
      phone: [this.current ? this.current.phone : null],
      image: [this.current ? this.current.image : null],
      hasFridge: [this.current ? this.current.hasFridge : false],
      description: [this.current ? this.current.description : null],
    });
  }

  getSrcImage() {
    return this.currentImage ? this.currentImage.value : null;
  }

  readURL(event: any): void {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];
      if (/\.(jpe?g|png|gif|bmp)$/i.test(file.name)) {
        if (file.size > 2097152) {
          alert("¡El archivo excede los 2MB!");
          this.currentImage = null;
        } else {
          this.loading = true;
          this.currentImage = new Image();

          const title = this.form.controls["name"].value;
          this.currentImage.name =
            (title ? title : "Node") + "_" + new Date().getTime();

          const documentType = /[^.]*$/.exec(file.name)[0].toUpperCase();
          this.currentImage.type = documentType;

          const reader = new FileReader();
          reader.readAsDataURL(file);
          reader.onload = (e) => {
            this.currentImage.value = reader.result;
            this.loading = false;
            this.form.controls["image"].patchValue(this.currentImage);
          };

        }
      } else {
        alert(
          "La extension de la imagen debe ser de tipo jpg, jpeg, png, gif o bmp"
        );
        this.currentImage = null;
      }
    }
  }

  save() {
    this.submitted = true;
    if (this.form.valid) {
      const formObj = this.form.getRawValue();
      this.nodeService[this.idElement ? "update" : "create"](formObj).subscribe(
        (data) => {
          this.toastrService.success(
            "Nodo " + this.idElement
              ? "creado"
              : "actualizado" + " correctamente",
            "Guardando"
          );
          window.location.reload();
        },
        (error) => {
          this.toastrService.error(
            "No guardado",
            "El nodo no pudo ser " + this.idElement ? "creado." : "actualizado."
          );
        }
      );
    }
  }

  // open modal
  open(): void {
    super.open();
    this.configNodeModal.show();
  }

  // close modal
  close(): void {
    super.close();
    this.configNodeModal.hide();
  }
}
