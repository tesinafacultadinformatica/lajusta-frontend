import { Component, ViewChild, ElementRef } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { ModalService } from "src/app/shared/services/modal.service";
//import { PurchaseService } from "src/app/shared/services/purchase.service";
import { ToastrService } from "ngx-toastr";
import { ConfigBaseModalComponent } from "../config-base-modal/config-base-modal.component";
import { PurchaseService } from "../../services/purchase.service";
import { Purchase } from "../../models/purchase";
import { Product } from "../../models/product";
import { ProductService } from "../../services/product.service";

@Component({
  selector: "app-config-purchase-modal",
  templateUrl: "./config-purchase-modal.component.html",
  styleUrls: ["./config-purchase-modal.component.scss"],
})
export class ConfigPurchaseModalComponent extends ConfigBaseModalComponent {
  @ViewChild("configPurchaseModal") configPurchaseModal: any;

  productsList: Product[];

  constructor(
    private formBuilder: FormBuilder,
    private purchaseService: PurchaseService,
    private productService: ProductService,
    private toastrService: ToastrService,
    modalService: ModalService,
    el: ElementRef
  ) {
    super(modalService, el);
  }

  loadData(): void {
    super.loadData();
    this.purchaseService.getAll().subscribe((result: Purchase[]) => {
      let listProductsId = result.map((purch) => purch.product.id);
      this.productService
        .getAll()
        .subscribe(
          (result: Product[]) =>
            (this.productsList = result.filter(
              (prod) => !listProductsId.includes(prod.id)
            ))
        );
    });
    if (this.idElement) {
      this.purchaseService.getById(this.idElement).subscribe((result) => {
        this.current = result;
        this.createForm();
      });
    } else {
      this.current = null;
      this.createForm();
    }
  }

  createForm() {
    this.form = this.formBuilder.group({
      id: [this.current ? this.current.id : null],
      product: [
        this.current ? this.current.product : null,
        Validators.required,
      ],
      quantity: [this.current ? this.current.quantity : 0],
      aditional: [this.current ? this.current.aditional : 0],
      price: [this.current ? this.current.price : 0],
      diferencial: [this.current ? this.current.diferencial : 0],
      total: [this.current ? this.current.total : null],
      generalId: [this.current ? this.current.generalId : null],
    });
  }

  save() {
    super.save();
    if (this.form.valid) {
      const formObj = this.form.getRawValue();
      this.purchaseService[this.idElement ? "update" : "create"](formObj).subscribe(
        (data) => {
          this.toastrService.success(
            "Compra " + (!this.idElement
              ? "creada"
              : "actualizada") + " correctamente",
            "Guardando"
          );
          window.location.reload();
        },
        (error) => {
          this.toastrService.error(
            "No guardado",
            "La compra no pudo ser " + (!this.idElement
              ? "creada"
              : "actualizada")
          );
        }
      );
    }
  }

  selectProduct(p: any) {
    this.form.controls["price"].patchValue(
      p ? this.productsList.find((prod) => prod.id === p).buyPrice : 0
    );
  }

  open(): void {
    super.open();

    this.configPurchaseModal.show();
  }

  // close modal
  close(): void {
    super.close();
    this.configPurchaseModal.hide();
  }
}
