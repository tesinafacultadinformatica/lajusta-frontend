import { Component, ViewChild, ElementRef } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { ModalService } from "src/app/shared/services/modal.service";
import { ProductService } from "src/app/shared/services/product.service";
import { ToastrService } from "ngx-toastr";
import { Producer } from "../../models/producer";
import { Category } from "../../models/category";
import { Unit } from "../../models/unit";
import { ProducerService } from "../../services/producer.service";
import { CategoryService } from "../../services/category.service";
import { AuthService } from "../../services/auth.service";
import { UnitService } from "../../services/unit.service";
import { ConfigBaseModalComponent } from "../config-base-modal/config-base-modal.component";
import { Image } from "../../models/image";

@Component({
  selector: "app-config-product-modal",
  templateUrl: "./config-product-modal.component.html",
  styleUrls: ["./config-product-modal.component.scss"],
})
export class ConfigProductModalComponent extends ConfigBaseModalComponent {
  @ViewChild("configProductModal") configProductModal: any;

  currentImages: Image[];
  loading: boolean = false;
  producersList: Producer[];
  categoriesList: Category[];
  units: Unit[];

  constructor(
    private formBuilder: FormBuilder,
    private productService: ProductService,
    private producerService: ProducerService,
    private categoryService: CategoryService,
    private unitService: UnitService,
    private authService: AuthService,
    private toastrService: ToastrService,
    modalService: ModalService,
    el: ElementRef
  ) {
    super(modalService, el);
  }

  get categories() {
    return this.form.controls["categories"];
  }

  loadData(): void {
    super.loadData();
    this.producerService.getAll().subscribe((result: Producer[]) => {
      this.producersList = result;
    });
    this.unitService.getAll().subscribe((result: Unit[]) => {
      this.units = result;
    });
    this.categoryService.getFullAll().subscribe((result) => {
      this.categoriesList = result.filter((cat) => cat.id != null);
    });
    if (this.idElement) {
      this.productService.getById(this.idElement).subscribe((result) => {
        this.current = result;
        this.createForm();
        this.currentImages = this.current.images;
      });
    } else {
      this.current = null;
      this.currentImages = [];
      this.createForm();
    }
  }

  createForm() {
    this.form = this.formBuilder.group({
      id: [this.current ? this.current.id : null],
      user: this.formBuilder.group({
        id: [this.current && this.current.user ? this.current.user.id : null]
      }),
      title: [this.current ? this.current.title : null, Validators.required],
      images: [this.current ? this.current.images : []],
      producer: [this.current ? this.current.producer.id :null],
      description: [this.current ? this.current.description : null],
      brand: [this.current ? this.current.brand : null],
      unitQuantity: [this.current ? this.current.unitQuantity : null],
      unit: [this.current ? this.current.unit.id : null, Validators.required],
      unitDescription: [this.current ? this.current.unitDescription : null],
      categories: [
        this.current ? this.current.categories : [],
        Validators.required,
      ],
      buyPrice: [this.current ? this.current.buyPrice : null, Validators.required],
      price: [this.current ? this.current.price : null, Validators.required],
      stock: [this.current ? this.current.stock : 0],
      isPromotion: [this.current ? this.current.isPromotion : false],
      percent: [this.current ? this.current.percent : null],
      needCold: [this.current ? this.current.needCold : false],
    });
  }

  readURL(event: any): void {
    const total = event.target.files.length;
    let loaded = 0;
    for (var i = 0; i < total; i++) {
      // this.files.push(event.target.files[i]);
      const file = event.target.files[i];
      if (/\.(jpe?g|png|gif|bmp)$/i.test(file.name)) {
        if (file.size > 2097152) {
          loaded++;
          alert("¡El archivo " + file.name + " excede los 2MB!");
        } else {
          this.loading = true;
          const img = new Image();

          const title = this.form.controls["title"].value;
          img.name = (title ? title : "Product") + "_" + new Date().getTime();

          const documentType = /[^.]*$/.exec(file.name)[0].toUpperCase();
          img.type = documentType;

          const reader = new FileReader();
          reader.readAsDataURL(file);

          reader.onload = (e) => {
            img.value = reader.result;
            if(this.currentImages.length === 0){
              img.isMain = true;
            }
            this.currentImages.push(img);

            loaded++;
            if (loaded === total){
              this.loading = false;
            }
          };

          this.form.controls["images"].patchValue(this.currentImages);
        }
      } else {
        loaded++;
        alert(
          "La extension de la imagen debe ser de tipo jpg, jpeg, png, gif o bmp"
        );
      }
    }
  }


  selectImageMain(i:number){
    const index = this.currentImages.findIndex(img => img.isMain);
    if(index !== i){
      this.currentImages[index].isMain = false;
      this.currentImages[i].isMain = true;
      this.form.controls["images"].patchValue(this.currentImages);
    }
  }

  removeImage(img) {
    const index = this.currentImages.indexOf(img);

    if (index >= 0) {
      this.currentImages.splice(index, 1);
      this.form.controls["images"].patchValue(this.currentImages);
    }
  }

  save() {
    super.save();
    if (this.form.valid) {
      this.form.controls["user"].setValue({ id: this.authService.currentUserValue.user.id })
      const formObj = this.form.getRawValue();
      this.productService[this.idElement ? "update" : "create"](
        formObj
      ).subscribe(
        (data) => {
          this.toastrService.success(
            "Producto " + (this.idElement
              ? "actualizado"
              : "creado") + " correctamente",
            "Guardando"
          );
          window.location.reload();
        },
        (error) => {
          this.toastrService.error(
            "No guardado",
            "El producto no pudo ser " + (this.idElement
              ? "actualizado."
              : "creado.")
          );
        }
      );
    }
  }

  changeSelectedCategory(categories: Category[]) {
    this.form.controls["categories"].patchValue(categories);
  }

  // open modal
  open(): void {
    super.open();

    this.configProductModal.show();
  }

  // close modal
  close(): void {
    super.close();
    this.configProductModal.hide();
  }
}
