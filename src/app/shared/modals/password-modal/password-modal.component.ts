
import { NgForm, EmailValidator, FormControl, FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Component, OnInit, Input, ViewChild, ElementRef } from "@angular/core";
import { ModalService } from '../../services/modal.service';
import { UserService } from '../../services/user.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
declare var $: any;
@Component({
	selector: "app-password-modal",
	templateUrl: "./password-modal.component.html",
	styleUrls: ["./password-modal.component.scss"]
})
export class PasswordModalComponent implements OnInit {
	@Input() id: string;
  private element: any;
	@ViewChild('passwordModal') passwordModal: any;

  passwordForm: FormGroup;
  available = false;
  submitted = false;

	constructor(
		private modalService: ModalService,
		el: ElementRef,
    private userService: UserService,
    private formBuilder: FormBuilder,
    private toastrService: ToastrService) {
		this.element = el.nativeElement;
	}

  get emailPass() {
    return this.passwordForm.get("correo");
  }

	ngOnInit(): void {
		let modal = this;

		// ensure id attribute exists
		if (!this.id) {
			console.error('modal must have an id');
			return;
		}
		// add self (this modal instance) to the modal service so it's accessible from controllers
    this.modalService.add(this);

    this.passwordForm = this.formBuilder.group(
      {
        email: [
          null,
          [
            Validators.required,
            Validators.email,
            Validators.pattern(
              /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            ),
          ],
        ]
      }
    );
	}

	// remove self from modal service when directive is destroyed
	ngOnDestroy(): void {
		this.modalService.remove(this.id);
		this.element.remove();
  }

  recoverPass(){
    this.submitted = true;
    if (this.passwordForm.valid) {
      let formObj = this.passwordForm.getRawValue();
    this.userService.recoveryPassword(formObj).subscribe(
      (res) => {
        this.toastrService.success(
          "Se solicito el cambio de contraseña",
          "Le va a estar llegando un correo a su casilla"
        );

        setTimeout((router: Router) => {
          this.close();
        }, 1500);
      },
      (error) => {
        this.toastrService.error(
          "Error al solicitar el cambio de contraseña",
          error
        );
      }
    );
  }
}

	// open modal
	open(): void {
		this.available= true;
		this.passwordModal.show();
	}

	// close modal
	close(): void {
		this.available= false;
		this.passwordModal.hide()
	}

}
