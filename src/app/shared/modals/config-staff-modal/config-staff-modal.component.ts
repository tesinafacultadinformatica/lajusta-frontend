import {
  Component,
  ViewChild,
  ElementRef
} from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { ModalService } from "src/app/shared/services/modal.service";
import { StaffService } from "src/app/shared/services/staff.service";
import { ToastrService } from "ngx-toastr";
import { ConfigBaseModalComponent } from '../config-base-modal/config-base-modal.component';

@Component({
  selector: "app-config-staff-modal",
  templateUrl: "./config-staff-modal.component.html",
  styleUrls: ["./config-staff-modal.component.scss"],
})
export class ConfigStaffModalComponent  extends ConfigBaseModalComponent {
  @ViewChild("configStaffModal") configStaffModal: any;

  constructor(
    private formBuilder: FormBuilder,
    private staffService: StaffService,
    private toastrService: ToastrService,
    modalService: ModalService,
    el: ElementRef) {
    super(modalService,el);
  }

  loadData(): void {
    super.loadData();
    if (this.idElement) {
      this.staffService.getById(this.idElement).subscribe((result) => {
        this.current = result;
        this.createForm();
      });
    } else {
      this.current = null;
      this.createForm();
    }
  }

  createForm() {
    this.form = this.formBuilder.group({
      id: [this.current ? this.current.id : null],
      description: [
        this.current ? this.current.description : null,
        Validators.required,
      ],
    });
  }

  save() {
    super.save();
    if (this.form.valid) {
      const formObj = this.form.getRawValue();
      this.staffService[this.idElement ? "update" : "create"](formObj).subscribe(
        (data) => {
          this.toastrService.success(
            "Staff " + (!this.idElement
              ? "creado"
              : "actualizado") + " correctamente",
            "Guardando"
          );
          window.location.reload();
        },
        (error) => {
          this.toastrService.error(
            "No guardado",
            "El Staff no pudo ser " + (!this.idElement
              ? "creado"
              : "actualizado")
          );
        }
      );
    }
  }

  // open modal
  open(): void {
   super.open();
    this.configStaffModal.show();
  }

  // close modal
  close(): void {
    super.close();
    this.configStaffModal.hide();
  }
}
