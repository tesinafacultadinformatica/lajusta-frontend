import { Product } from '../../models/product';
import { Component, OnInit, OnDestroy, ViewChild, Input, ElementRef, EventEmitter, OnChanges, Output } from '@angular/core';
import { ProductService } from '../../services/product.service';
import { CartService } from '../../services/cart.service';
import { ModalService } from '../../services/modal.service';
import { Image } from 'src/app/shared/models/image';
import { ToastrService } from 'ngx-toastr';
import { GeneralService } from '../../services/general.service';

@Component({
	selector: 'app-product-detail-modal',
	templateUrl: './product-detail-modal.component.html',
	styleUrls: ['./product-detail-modal.component.scss']
})
export class ProductDetailModalComponent implements OnInit, OnChanges, OnDestroy {
	@Input() id: string;
	@Input() idProduct: number;
	private element: any;
	@ViewChild('productDetailModal') productDetailModal: any;

	available = false;
	@Output() openGallery = new EventEmitter<any[]>();

	product: Product;
  cantImage: number = 0;
  generalActive: boolean;

	constructor(
		private productService: ProductService,
    private generalService: GeneralService,
		private cartService: CartService,
		private toastrService: ToastrService,
		private modalService: ModalService,
		el: ElementRef
	) {
		this.product = new Product();
    this.element = el.nativeElement;
    this.generalService.getActive().subscribe((result) => {
      this.generalActive = !!result;
    });
	}

	ngOnInit(): void {
		let modal = this;

		// ensure id attribute exists
		if (!this.id) {
			console.error('modal must have an id');
			return;
		}
		// add self (this modal instance) to the modal service so it's accessible from controllers
		this.modalService.add(this);

		this.getProductDetail();
	}

	ngOnChanges(): void {
		this.getProductDetail();
	}

	// open modal
	open(): void {
		this.available= true;
		this.productDetailModal.show();
	}

	// close modal
	close(): void {
		this.available= false;
		this.productDetailModal.hide()
	}

getFirstImage(){
  return (this.product.images[0] as Image).value
}

	getProductDetail() {
		// this.spinnerService.show();
		if (this.idProduct) {

			const x = this.productService.getById(this.idProduct).subscribe(
				(product) => {
					this.product = product;
					this.cantImage = product.images ? product.images.length : 0;
				},
				(error) => {
					this.toastrService.error('Error mientras se obtenia el producto', error);
				}
			);
		}
	}

	availableAddToCart(){
		return this.generalActive && !this.cartService.exist(this.product.id) && this.product.stock > 0;
	  }


	removeToCart() {
		this.cartService.removeLocalCartProduct(this.product.id);
	}

	changeToCart(value: number) {
		this.cartService.updateLocalCartProduct(this.product.id,value);
	}

	inTheCart(){
		return this.cartService.exist(this.product.id);
	}

	quantityInTheCart(){
		return this.cartService.quantityProduct(this.product.id);
	  }

	openGalleryModal() {
		this.openGallery.emit(this.product.images);
	}

	addToCart() {
		this.cartService.addToCart(this.product);
	}

	ngOnDestroy() {
		this.modalService.remove(this.id);
		this.element.remove();
	}
}
