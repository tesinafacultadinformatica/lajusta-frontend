import {
  Input,
  ElementRef,
  OnChanges
} from "@angular/core";
import { ModalService } from "src/app/shared/services/modal.service";
import { FormGroup } from '@angular/forms';

export class ConfigBaseModalComponent implements OnChanges {
  @Input() id: string;
  @Input() idElement: number;
  @Input() action: string;
  private element: any;

  available = false;
  actionEdit: boolean = false;
  submitted: boolean = false;
  errorMessage: string = null;
  current: any;
  form: FormGroup;

  constructor(
    protected modalService: ModalService,
    el: ElementRef
  ) {
    this.element = el.nativeElement;
  }

  ngOnChanges(): void {
    this.current = null;
    this.loadData();
  }

  ngOnInit(): void {
    let modal = this;

    // ensure id attribute exists
    if (!this.id) {
      console.error("modal must have an id");
      return;
    }
    // add self (this modal instance) to the modal service so it's accessible from controllers
    this.modalService.add(this);
  }

  hasError(control:string, errorType: string = 'required'){
    return this.submitted && this.form.get(control).hasError(errorType);
  }

  loadData(){}

  edit() {
    this.actionEdit = true;
  }

  save() {
    this.submitted = true;
  }

  // remove self from modal service when directive is destroyed
  ngOnDestroy(): void {
    this.modalService.remove(this.id);
    this.element.remove();
  }

  // open modal
  open(): void {
    this.submitted = false;
    this.actionEdit = false;
    this.errorMessage = null;
    //this.loadData();
    if(this.action === 'new'){
      this.form.reset();
    }
    this.available = true;
  }

  // close modal
  close(): void {
    this.available = false;
  }
}
