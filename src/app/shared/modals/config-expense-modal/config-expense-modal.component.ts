import {
  Component,
  ViewChild,
  ElementRef
} from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { ModalService } from "src/app/shared/services/modal.service";
//import { ExpenseService } from "src/app/shared/services/expense.service";
import { Expense } from "src/app/shared/models/expense";
import { ToastrService } from "ngx-toastr";
import { ConfigBaseModalComponent } from '../config-base-modal/config-base-modal.component';
import { ExpenseService } from '../../services/expense.service';
import { Staff } from '../../models/staff';
import { StaffService } from '../../services/staff.service';
import { Pageable } from '../../models/pageable';

@Component({
  selector: "app-config-expense-modal",
  templateUrl: "./config-expense-modal.component.html",
  styleUrls: ["./config-expense-modal.component.scss"],
})
export class ConfigExpenseModalComponent extends ConfigBaseModalComponent {
  @ViewChild("configExpenseModal") configExpenseModal: any;

  maxDate: Date = new Date(Date.now());
  staffsList : Staff[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private expenseService: ExpenseService,
    private staffService: StaffService,
    private toastrService: ToastrService,
    modalService: ModalService,
    el: ElementRef) {
    super(modalService,el);
  }

  loadData(): void {
    super.loadData();
    this.staffService.getAll().subscribe((result: Staff[]) => {
      this.staffsList= result;
    });
    if (this.idElement) {
    this.expenseService.getById(this.idElement).subscribe((result) => {
        this.current = result;
        this.createForm();
      });
    } else {
      this.current = null;
      this.createForm();
    }
    this.form.controls['price'].valueChanges.subscribe(value => {
      this.refreshTotal();
    });
    this.form.controls['quantity'].valueChanges.subscribe(value => {
      this.refreshTotal();
    });
  }

  createForm() {
    this.form = this.formBuilder.group({
      id: [this.current ? this.current.id : null],
      staff: [this.current ? this.current.staff : null, Validators.required],
      description: [this.current ? this.current.description : null, Validators.required],
      quantity: [this.current ? this.current.quantity : 0],
      price: [this.current ? this.current.price : 0],
      total: [this.current ? this.current.total : 0],
      date: [this.current ? this.current.date : null],
      balanceDate: [this.current ? this.current.balanceDate : null],
      generalId: [this.current ? this.current.generalId : null]
    });
  }

  refreshTotal(){
    this.form.controls["total"].patchValue(this.form.controls["quantity"].value * this.form.controls["price"].value)
  }

  save() {
    super.save();
    if (this.form.valid) {
      const formObj = this.form.getRawValue();
      this.expenseService[this.idElement ? "update" : "create"](formObj).subscribe(
        (data) => {
          this.toastrService.success(
            "Gasto " + (!this.idElement
              ? "creado"
              : "actualizado") + " correctamente",
            "Guardando"
          );
          window.location.reload();
        },
        (error) => {
          this.toastrService.error(
            "No guardado",
            "El Gasto no pudo ser " + (!this.idElement
              ? "creado"
              : "actualizado")
          );
        }
      );
    }
  }

  // open modal
  open(): void {
    super.open();
    this.configExpenseModal.show();
  }

  // close modal
  close(): void {
    super.close();
    this.configExpenseModal.hide();
  }
}
