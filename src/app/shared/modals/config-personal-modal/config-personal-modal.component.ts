import {
  Component,
  ViewChild,
  ElementRef,
} from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { ModalService } from "src/app/shared/services/modal.service";
import { PersonalService } from "src/app/shared/services/personal.service";
import { ToastrService } from "ngx-toastr";
import { ConfigBaseModalComponent } from '../config-base-modal/config-base-modal.component';
import { SharedService } from "src/app/shared/services/shared.service";
import { DialogModalComponent } from "../dialog-modal/dialog-modal.component";
import { MatDialog } from "@angular/material/dialog";
import { SubscriptionLike as ISubscription } from "rxjs";
import { User } from "../../models/user";
import { UserService } from "../../services/user.service";
import { Role } from "../../enums/role.enum";

@Component({
  selector: "app-config-personal-modal",
  templateUrl: "./config-personal-modal.component.html",
})
export class ConfigPersonalModalComponent  extends ConfigBaseModalComponent {
  @ViewChild("configStaffModal") configStaffModal: any;

  subs: ISubscription[] = [];
  user: User;


  constructor(
    private formBuilder: FormBuilder,
    private personalService: PersonalService,
    private sharedService: SharedService,
    private toastrService: ToastrService,
    private dialog: MatDialog,
    private userService: UserService,
    modalService: ModalService,
    el: ElementRef) {
    super(modalService,el);
   
  }

  get email() { return this.form.get('email'); };

  loadData(): void {
    super.loadData();
    if (this.idElement) {
      this.personalService.getStaff().subscribe((result) => {
        this.current = result;
        this.createForm();
      });
    } else {
      this.current = null;
      this.createForm();
    }
  }

  createForm() {
    this.form = this.formBuilder.group({
      email: [this.current ? this.current.email : null, [Validators.required, Validators.email,
        Validators.pattern(
          /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        )]],
    });
  }

  save() {
    super.save();
    if (this.form.valid) {
      this.userService.getByEmail(this.form.value["email"]).subscribe(
        (data) => { 
          this.user = data["page"][0]; 
          this.user != null ? 
            this.openDialog(
              "¿Esta seguro?",
              "Va a agregar al usuario  " + this.user.firstName + " " + this.user.lastName + " al equipo",
              this.user
            )
          : this.toastrService.error('No existe usuario con el email', "Error")
        }
        ,(error) => {
          this.toastrService.error('Error mientras se obtenia al usuario', error);
      })
    }
  }

  openDialog(
    title: string,
    description: string,
    user: User,
  ): void {
    const dialogRef = this.dialog.open(DialogModalComponent, {
      disableClose: true,
      width: "300px",
      data: { title: title, description: description },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        if (this.isPersonal(this.user)) {
          this.toastrService.success(
            "El usuario " + this.user.firstName + " ya pertenece al equipo",
            "Guardando")
        } else {
          this.user.role = +Role.Equipo;
          this.subs.push(
            this.userService.update(user).subscribe(
              (data) => {
                this.toastrService.success(
                  "Personal " + (!this.idElement
                    ? "agregado"
                    : "actualizado") + " correctamente",
                  "Guardando"
                );
                window.location.reload();
              },
              (error) => {
                this.toastrService.error(
                  "No guardado",
                  "El personal no pudo ser " + (!this.idElement
                    ? "creado"
                    : "actualizado")
                );
              }
            )   
          );
        }
      };
      this.close();
    });
  }

  isPersonal(user:User){
    return this.user.role == +Role.Administrador || this.user.role == +Role.Equipo
  }

  // open modal
  open(): void {
   super.open();
    this.configStaffModal.show();
  }

  // close modal
  close(): void {
    super.close();
    this.configStaffModal.hide();
  }
}
