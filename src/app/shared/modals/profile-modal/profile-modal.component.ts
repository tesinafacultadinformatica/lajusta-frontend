import {
  Component,
  OnInit,
  OnDestroy,
  Input,
  ElementRef,
  ViewChild,
} from "@angular/core";
import { AuthService } from "src/app/shared/services/auth.service";
import { UserService } from "../../services/user.service";
import * as moment from "moment";
import { ModalService } from "src/app/shared/services/modal.service";
import { User } from "src/app/shared/models/user";
import { Image } from "../../models/image";

@Component({
  selector: "app-profile-modal",
  templateUrl: "./profile-modal.component.html",
  styleUrls: ["./profile-modal.component.scss"],
})
export class ProfileModalComponent implements OnInit, OnDestroy {
  @Input() id: string;
  private element: any;
  @ViewChild("profileModal") profileModal: any;

  available = false;

  public user: User; // Container for holding the current user from /account/{id} end point.
  public updatedUserModel: any = {}; // Container for holding a user data submited from edited page to pass to /account/updatestatus end point.
  public editPage: boolean = false; // Variable for tracking whether the edit page button is clicked or not.

  loading: boolean = false;

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private modalService: ModalService,
    el: ElementRef
  ) {
    this.element = el.nativeElement;
  }

  ngOnInit(): void {
    let modal = this;

    // ensure id attribute exists
    if (!this.id) {
      console.error("modal must have an id");
      return;
    }
    // add self (this modal instance) to the modal service so it's accessible from controllers
    this.modalService.add(this);
    if (this.authService.isLoggedIn()) {
      this.updatedUserModel = { ...this.authService.currentUserValue.user };
      this.user = this.authService.currentUserValue.user;
    }
  }

  openEdit() {
    this.editPage = true;
  }

  updateProfile() {
    this.userService
      .update(this.updatedUserModel)
      .subscribe((user_res: any) => {
        this.authService.refreshLocalCurrentUser(user_res);
      });
  }

  changeTimeFormat(model: any) {
    return moment(model).fromNow();
  }

  getSrcImage() {
    return this.updatedUserModel.image
      ? this.updatedUserModel.image.value
      : "../../../../assets/no_image.jpg";
  }

  readURL(event: any): void {
    const file = event.target.files[0];
    if (/\.(jpe?g|png|gif|bmp)$/i.test(file.name)) {
      if (file.size > 2097152) {
        alert("¡El archivo excede los 2MB!");
        this.updatedUserModel.image = null;
      } else {
        this.loading = true;
        const img = new Image();

        const title = this.updatedUserModel.firstName;
        img.name = (title ? title : "User") + "_" + new Date().getTime();

        const documentType = /[^.]*$/.exec(file.name)[0].toUpperCase();
        img.type = documentType;

        const reader = new FileReader();
        reader.readAsDataURL(file);

        reader.onload = (e) => {
          img.value = reader.result;
          this.updatedUserModel.image = img;
          this.loading = false;
        };
      }
    } else {
      alert(
        "La extension de la imagen debe ser de tipo jpg, jpeg, png, gif o bmp"
      );
      this.updatedUserModel.image = null;
    }
  }

  isValidEmail() {
    return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
      this.updatedUserModel.email
    );
  }

  // open modal
  open(): void {
    this.editPage = false;
    this.available = true;
    if (this.authService.isLoggedIn()) {
      this.updatedUserModel = { ...this.authService.currentUserValue.user };
    }
    this.profileModal.show();
  }

  // close modal
  close(): void {
    this.available = false;
    this.profileModal.hide();
  }

  ngOnDestroy() {
    this.modalService.remove(this.id);
    this.element.remove();
  }
}
