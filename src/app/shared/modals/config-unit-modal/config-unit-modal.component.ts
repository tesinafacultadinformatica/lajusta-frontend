import {
  Component,
  ViewChild,
  ElementRef
} from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { ModalService } from "src/app/shared/services/modal.service";
import { UnitService } from "src/app/shared/services/unit.service";
import { ToastrService } from "ngx-toastr";
import { ConfigBaseModalComponent } from '../config-base-modal/config-base-modal.component';

@Component({
  selector: "app-config-unit-modal",
  templateUrl: "./config-unit-modal.component.html",
  styleUrls: ["./config-unit-modal.component.scss"],
})
export class ConfigUnitModalComponent  extends ConfigBaseModalComponent {
  @ViewChild("configUnitModal") configUnitModal: any;

  constructor(
    private formBuilder: FormBuilder,
    private unitService: UnitService,
    private toastrService: ToastrService,
    modalService: ModalService,
    el: ElementRef) {
    super(modalService,el);
  }

  loadData(): void {
    super.loadData();
    if (this.idElement) {
      this.unitService.getById(this.idElement).subscribe((result) => {
        this.current = result;
        this.createForm();
      });
    } else {
      this.current = null;
      this.createForm();
    }
  }

  createForm() {
    this.form = this.formBuilder.group({
      id: [this.current ? this.current.id : null],
      code: [
        this.current ? this.current.code : null,
        Validators.required,
      ],
      description: [
        this.current ? this.current.description : null,
        Validators.required,
      ]
    });
  }


  save() {
    super.save();
    if (this.form.valid) {
      const formObj = this.form.getRawValue();
      this.unitService[this.idElement ? "update" : "create"](formObj).subscribe(
        (data) => {
          this.toastrService.success(
            "Unidad " + (!this.idElement
              ? "creado"
              : "actualizado") + " correctamente",
            "Guardando"
          );
          window.location.reload();
        },
        (error) => {
          this.toastrService.error(
            "No guardado",
            "La Unidad no pudo ser " + (!this.idElement
              ? "creado"
              : "actualizado")
          );
        }
      );
    }
  }

  // open modal
  open(): void {
    super.open();

    this.configUnitModal.show();
  }

  // close modal
  close(): void {
    super.close();
    this.configUnitModal.hide();
  }
}
