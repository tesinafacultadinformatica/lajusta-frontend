import { Directive, ElementRef,  Input, OnInit } from "@angular/core";
import { addressComplete } from '../helper/util/address-complete';
import { Address } from '../models/address';

@Directive({
  selector: "[addressFull]"
})
export class AddressDirective implements OnInit {
  @Input('address') address: Address;
  @Input('aditional') aditional: string;

  constructor(private el: ElementRef) {
      }

  ngOnInit() {
    this.el.nativeElement.innerHTML = (this.aditional ? this.aditional + ' - ' : '') +  addressComplete(this.address);
  }
}
