import { Pipe, PipeTransform } from '@angular/core';
import { of } from 'rxjs';

@Pipe({
  name: 'ellipsis'
})
export class EllipsisPipe implements PipeTransform {

  transform(text: string, characterLimit?: number, link?: any): string {

    if (text.length > characterLimit) {
      text = text.slice(0,characterLimit);

      if(link){
        text += ' <a class="pipeLink" href="' + link + '">...<a>'
      }else{
        text += '...'
      }

    }
    return text;
  }

}
