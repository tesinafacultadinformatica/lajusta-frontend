import { Address } from '../../models/address';

export function addressComplete(address: Address): string {
  let result = '';
  if (address)
   result = `${address.street ? address.street : ''} N° ${address.number ? address.number : ''} ${address.description ? address.description : ''}  ${address.floor ? address.floor : ''}  ${address.apartment ? address.apartment : ''} ${address.betweenStreets ? address.betweenStreets : ''} `;

  return result;
}
