import { stringify } from 'query-string';

export function createUrlQuery(params: any): any {
    if (!params) {
        return "";
    }

    let page;
    let perPage;
    let field;
    let order;
    let query: any = {};
    if (params.pagination && (params.pagination.page >= 0)) {
         page = params.pagination.page;
         perPage =  params.pagination.perPage;
         query.range = page + ',' + perPage;
    }

    if (params.sort) {
        field = params.sort.field;
        order = params.sort.order;
        if (field && order) {
            query.sort = field + ',' + order;
        }
        else {
            query.sort = 'id,ASC';
        }
    }

    if (!params.filter) {
        params.filter = {};
    }else{
      query.filter = JSON.stringify(params.filter)
    }
    if (Array.isArray(params.ids)) {
        params.filter.id = params.ids;
    }
    if (params.properties) {

      let properties=[];
      params.properties.forEach(element => {
        properties.push({ 'key': element.key , 'value': element.value });
      });
      query.properties = JSON.stringify(properties);
    }

    return stringify(query);
}
