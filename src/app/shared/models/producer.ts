import { Address } from './address';
import { Base } from './base';
import { Image } from './image';
import { Product } from './product';
import { Tag } from './tag';

export class Producer extends Base{
  name: string;
  origin: string;
  email: string;
  description?: string;
  images?: Image[];
  tags: Tag[];
  youtubeVideoId?: string;
  phone: string;
  address: Address;
  isCompany: boolean;
  deletedAt?: Date;
}

export class ProductProducer extends Base {
  product: Product;
  price: number;
  quantity: number;
}
