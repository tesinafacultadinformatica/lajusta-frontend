import { Base } from './base';

export class Image extends Base{
    name: string;
    value: string | ArrayBuffer;
    type: string;
    isMain?: boolean;
  }