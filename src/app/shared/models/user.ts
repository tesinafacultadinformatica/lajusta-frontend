import { Address } from './address';
import { Base } from './base';
import { Image } from './image';


export class User extends Base{
  role: number = 2; //generic
  firstName: string;
  lastName: string;
  email: string;
  age:number;
  description?: string;
  encryptedPassword?:string;
  passwordConfirmation?:string;
  image?: Image;
  phone: string;
  address?: Address;
  deliveryAddress?: Address;
  rememberToken?: boolean;
  deletedAt?: Date;
}

export class LoginUser {
  value: string; //token
  user:User;
}

