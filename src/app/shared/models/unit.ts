import { Base } from './base';

export class Unit extends Base{
    description: string;
    code: string;
    deletedAt?: Date;
  }
