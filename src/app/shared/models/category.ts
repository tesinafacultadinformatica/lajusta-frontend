import { Image } from './image';
import { Base } from './base';
import { Generic } from './generic';

export class Category extends Base{
    name: string;
    parent: Generic; //generic
    image?: Image;
    childList?: Category[];
    deletedAt?: Date;
  }
