import { Base } from './base';

export class Balance extends Base{
  totalExpenses:number;
  totalSales:number;
  balance:number;
  openDate:Date;
  closeDate:Date;
  generalId:number;
}
