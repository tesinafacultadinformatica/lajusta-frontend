import { Base } from './base';
import { Generic } from './generic';
import { Node } from './node';

export class DateTimeNode {
  id: number;
  node: Node;
  day: Date;
  dateTimeFrom: string;
  dateTimeTo: string;
}

export class General extends Base{
  user: Generic;
  canClose: Boolean;
  dateActivePage: Date;
  dateDownPage: Date;
  activeNodes:DateTimeNode[];
  deletedAt?: Date;
}
