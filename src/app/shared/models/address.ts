import { Base } from './base';

export class Address extends Base{
    street: string;
    number?: number;
    apartment?: string;
    betweenStreets?: string;
    floor?: string;
    description?: string;
    latitude?:number;
    longitude?:number;
  }
