import { Base } from './base';

export class Tag extends Base{
    description: string;
    deletedAt?: Date;
  }
