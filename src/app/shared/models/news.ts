import { Base } from './base';
import { Image } from './image';

export class News extends Base{
    title?: string;
    subtitle?: string;
    text?: string;
    image: Image;
    url?: string;
    deletedAt?: Date;
  }
