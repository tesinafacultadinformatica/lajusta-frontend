import { Base } from './base';

export class Staff extends Base{
    description: string;
    deletedAt?: Date;
  }
