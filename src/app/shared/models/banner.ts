import { Base } from './base';
import { Image } from './image';

export class Banner extends Base{
    title?: string;
    subtitle?: string;
    text?: string;
    image: Image;
    url?: string;
    deletedAt?: Date;
  }