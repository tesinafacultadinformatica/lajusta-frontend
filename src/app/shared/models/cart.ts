import { Product } from './product';
import { DateTimeNode } from './general';
import { Base } from './base';
import { Generic } from './generic';

export class CartItem {
    id?: number;
    product: Product;
    quantity: number;
    price?: number;
    isCanceled?: boolean;

    constructor(product: Product, quantity: number = 1){
        this.product = product;
        this.quantity = quantity;
    }
}

export class Cart extends Base{
  nodeDate: DateTimeNode;
  user: Generic; //generic
  saleDate: Date;
  cartProducts: CartItem[];
  possibleDeliveryDate: Date;
  total: number;
  idGeneral:number;
  observation:string;
  canceled?: boolean;
  deletedAt?: Date;
  staffDiscount: boolean;
}
