import { Address } from './address';
import { Base } from './base';
import { Image } from './image';

export class Node extends Base{
  name: string;
  address: Address;
  description: string;
  phone: string;
  image: Image;
  hasFridge: boolean;
  deletedAt?: Date;
}
