import { Producer } from './producer';
import { Product } from './product';
import { Base } from './base';
import { Generic } from './generic';

export class Purchase extends Base{
  product:Generic;
  quantity: number;
  aditional: number;
  price:number;
  diferencial:number;
  total:number;
  generalId: number;
}
