

import { Staff } from './staff';
import { Base } from './base';

export class Expense extends Base{
  staff:Staff;
  description:string;
  quantity: number;
  price:number;
  total:number;
  date:Date;
  balanceDate:Date;
  generalId: number;
}
