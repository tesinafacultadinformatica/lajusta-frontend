import { Image } from './image';
import { Category } from './category';
import { Base } from './base';
import { Unit } from './unit';
import { Generic } from './generic';

export class Product extends Base{
  user: Generic;
  producer: Generic;
  title: string;
  description: string;
  brand: string;
  unitQuantity: number;
  unit: Unit;
  unitDescription: string;
  buyPrice: number;
  price: number;
  stock: number;
  isPromotion: boolean;
  percent: number;
  categories: Category[];
  images: Image[];
  needCold?:boolean;
  deletedAt?: Date;
}
