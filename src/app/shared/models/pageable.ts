import { Base } from './base';

export class Pageable<T extends Base> {
  totalElements: number;
  page: T[];
}
