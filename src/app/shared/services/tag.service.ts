import { Injectable } from "@angular/core";

import { HttpClient } from "@angular/common/http";
import { Tag } from "../models/tag";
import { BasicService } from './basic-services/basic.service';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: "root",
})
export class TagService extends BasicService<Tag> {
  constructor(http: HttpClient) {
    super(http, "/api/tag");
  }

}
