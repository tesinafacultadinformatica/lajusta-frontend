import { Injectable } from "@angular/core";
import { Subject, Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class SharedService {
  openProductDetail: Subject<number> = new Subject<number>();
  openChangePassword: Subject<string> = new Subject<string>();
  openUser: Subject<any> = new Subject<any>();
  openUnit: Subject<any> = new Subject<any>();
  openStaff: Subject<any> = new Subject<any>();
  openTag: Subject<any> = new Subject<any>();
  openPurchase: Subject<any> = new Subject<any>();
  openProduct: Subject<any> = new Subject<any>();
  openProducer: Subject<any> = new Subject<any>();
  openNode: Subject<any> = new Subject<any>();
  openGeneral: Subject<any> = new Subject<any>();
  openExpense: Subject<any> = new Subject<any>();
  openCategory: Subject<any> = new Subject<any>();
  openCart: Subject<any> = new Subject<any>();
  openBanner: Subject<any> = new Subject<any>();
  openNews: Subject<any> = new Subject<any>();
  openPersonal: Subject<any> = new Subject<any>();
  constructor() {}

  modalProductDetail(): Observable<number> {
    return this.openProductDetail.asObservable();
  }

  showProductDetail(productId: number) {
    this.openProductDetail.next(productId);
  }

  modalChangePassword(): Observable<string> {
    return this.openChangePassword.asObservable();
  }

  showChangePassword(code: string) {
    this.openChangePassword.next(code);
  }


  modalUser(): Observable<any> {
    return this.openUser.asObservable();
  }

  modalUnit(): Observable<any> {
    return this.openUnit.asObservable();
  }

  modalStaff(): Observable<any> {
    return this.openStaff.asObservable();
  }

  modalTag(): Observable<any> {
    return this.openTag.asObservable();
  }

  modalPurchase(): Observable<any> {
    return this.openPurchase.asObservable();
  }

  modalProduct(): Observable<any> {
    return this.openProduct.asObservable();
  }

  modalProducer(): Observable<any> {
    return this.openProducer.asObservable();
  }

  modalNode(): Observable<any> {
    return this.openNode.asObservable();
  }

  modalGeneral(): Observable<any> {
    return this.openGeneral.asObservable();
  }

  modalExpense(): Observable<any> {
    return this.openExpense.asObservable();
  }

  modalCategory(): Observable<any> {
    return this.openCategory.asObservable();
  }

  modalCart(): Observable<any> {
    return this.openCart.asObservable();
  }

  modalBanner(): Observable<any> {
    return this.openBanner.asObservable();
  }

  modalNews(): Observable<any> {
    return this.openNews.asObservable();
  }

  modalPersonal(): Observable<any> {
    return this.openPersonal.asObservable();
  }

  showUser(userId: number, action: string) {
    this.openUser.next({ userId: userId, action: action });
  }

  showUnit(unitId: number, action: string) {
    this.openUnit.next({ unitId: unitId, action: action });
  }

  showStaff(staffId: number, action: string) {
    this.openStaff.next({ staffId: staffId, action: action });
  }

  showTag(tagId: number, action: string) {
    this.openTag.next({ tagId: tagId, action: action });
  }

  showPurchase(purchaseId: number, action: string) {
    this.openPurchase.next({ purchaseId: purchaseId, action: action });
  }

  showProduct(productId: number, action: string) {
    this.openProduct.next({ productId: productId, action: action });
  }

  showProducer(producerId: number, action: string) {
    this.openProducer.next({ producerId: producerId, action: action });
  }

  showNode(nodeId: number, action: string) {
    this.openNode.next({ nodeId: nodeId, action: action });
  }

  showGeneral(generalId: number, action: string) {
    this.openGeneral.next({ generalId: generalId, action: action });
  }

  showExpense(expenseId: number, action: string) {
    this.openExpense.next({ expenseId: expenseId, action: action });
  }

  showCategory(categoryId: number, action: string) {
    this.openCategory.next({ categoryId: categoryId, action: action });
  }

  showCart(cartId: number, action: string) {
    this.openCart.next({ cartId: cartId, action: action });
  }

  showBanner(bannerId: number, action: string) {
    this.openBanner.next({ bannerId: bannerId, action: action });
  }

  showNews(newsId: number, action: string) {
    this.openNews.next({ newsId: newsId, action: action });
  }

  showPersonal(personalId:number, action: string){
    this.openPersonal.next({ personalId: personalId, action: action});
  }

}
