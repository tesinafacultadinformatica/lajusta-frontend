import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IBaseConfigurationService } from './base-configuration-service.interface';
import { Observable, Subject } from 'rxjs';
import { createUrlQuery } from '../../helper/util/create-url-query';
import { map, tap } from 'rxjs/operators';
import { Base } from '../../models/base';
import { Pageable } from '../../models/pageable';
import { Filter } from '../../models/filter';
import { environment } from 'src/environments/environment';

@Injectable()
export class BasicService<T extends Base> implements IBaseConfigurationService{
  updateData: Subject<boolean> = new Subject<boolean>();

  constructor(protected http:HttpClient,
    @Inject(String) public url: string){}

  getUpdateData(): Observable<boolean> {
    return this.updateData.asObservable();
  }

  getData(
    filter: string = "",
    sort: string = "",
    order: string = "",
    page: number = 1,
    perPage: number = 5,
    properties: Filter[] = [],
    initTotal: Function = () => {}
  ): Observable<T[]> {
    let qString = createUrlQuery({
      filter: filter, sort: {field: sort, order: order}, pagination: { page, perPage }, properties: properties});
    return this.getAll(qString).pipe(map((res: Pageable<T>) => {
      initTotal(res.totalElements);
      return res.page
  }));
  }

  getAll(queryString:string = ''): Observable<Pageable<T> | T[]> {
		return this.http.get<Pageable<T> | T[]>(`${this.url}${queryString !== '' ? '?' + queryString : ''}`);
  }

  getById(id: number): Observable<T> {
    return this.http.get<T>(`${this.url}/${id}`);
  }

  create(data: any): Observable<T> {
    return this.http.post(`${this.url}`, data).pipe(
      map((p) => {
        this.updateData.next(true);
        return <T>p;
      })
    );
  }

  restore(id: number): Observable<T> {
    return this.http.post(`${this.url}/activate/${id}`,id).pipe(map((res) => { this.updateData.next(true); return <T>res; }));
  }

  update(data: T): Observable<T> {
    return this.http.put(`${this.url}`, data).pipe(
      map((res) => {
        this.updateData.next(true);
        return <T>res;
      })
    );
  }

  delete(id: number): Observable<T> {
    return this.http.delete(`${this.url}/${id}`).pipe(
      map((res) => {
        this.updateData.next(true);
        return <T>res;
      })
    );
  }

}
