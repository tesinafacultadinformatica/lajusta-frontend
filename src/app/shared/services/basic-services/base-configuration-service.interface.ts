import { Observable } from "rxjs";
import { ProductService } from "../product.service";
import { UserService } from "../user.service";
import { CategoryService } from "../category.service";
import { CartService } from "../cart.service";
import { NodeService } from "../node.service";
import { ProducerService } from "../producer.service";
import { GeneralService } from "../general.service";
import { BannerService } from "../banner.service";
import { NewsService } from "../news.service";
import { UnitService } from '../unit.service';
import { BalanceService } from '../balance.service';
import { PurchaseService } from '../purchase.service';
import { ExpenseService } from '../expense.service';
import { StaffService } from '../staff.service';
import { TagService } from '../tag.service';
import { Base } from '../../models/base';
import { Filter } from '../../models/filter';
import { PersonalService } from "../personal.service";

// AbstractFactoryInterface
export interface IBaseConfigurationService {
  getUpdateData(): Observable<boolean>;

  getData(
    filter: string,
    sort: string,
    order: string,
    page: number,
    perPage: number,
    properties: Filter[],
    initTotal: Function
  ): Observable<any>;
}

// AbstractFactoryProvider as a HashMap
export const listMap = new Map<string, any>([
  ["BALANCE", BalanceService],
  ["BANNER", BannerService],
  ["CATEGORY", CategoryService],
  ["CART", CartService],
  ["EXPENSE", ExpenseService],
  ["GENERAL", GeneralService],
  ["NEWS", NewsService],
  ["NODE", NodeService],
  ["PRODUCER", ProducerService],
  ["PRODUCT", ProductService],
  ["PURCHASE", PurchaseService],
  ["STAFF", StaffService],
  ["TAG", TagService],
  ["UNIT", UnitService],
  ["USER", UserService],
  ["PERSONAL", PersonalService]
]);
