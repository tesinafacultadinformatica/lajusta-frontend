import { Injectable } from "@angular/core";
import { Observable, of, Subject } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { Category } from "../models/category";
import { BasicService } from './basic-services/basic.service';
import { createUrlQuery } from '../helper/util/create-url-query';
import { map } from 'rxjs/operators';
import { Pageable } from '../models/pageable';
import { stringify } from 'query-string';
import { Filter } from '../models/filter';

@Injectable({
  providedIn: "root",
})
export class CategoryService extends BasicService<Category> {

  constructor(http: HttpClient) {
    super(http, '/api/category');
    this.getUpdateData().subscribe( () => {this.loadListCategories()})
  }

  getData(
    filter: string = "",
    sort: string = "",
    order: string = "",
    page: number = 1,
    perPage: number = 5,
    properties: Filter[] = [],
    initTotal: Function = () => {}
  ): Observable<Category[]> {
    let qString = createUrlQuery({
      filter: filter, sort: {field: sort, order: order}, pagination: { page, perPage }, properties: properties});
    return super.getAll(qString).pipe(map((res: Pageable<Category>) => {
      initTotal(res.totalElements);
      return res.page
  }));
  }

  private listCategories: Category[] = [];

  private searchCategory(valorAnterior: Category[], id: number): Category {
    let cateResult = null;
    for (let cate of valorAnterior) {
      if (cate.id === id) {
        cateResult = cate;
        break;
      } else {
        if (cate.childList) {
          cateResult = this.searchCategory(cate.childList, id);
          if (cateResult) {
            break;
          }
        }
      }
    }
    return cateResult;
  }

  private setListCategories(categories: Category[]) {
    let aux = [...categories];

    return aux.reduce((valorAnterior, valorActual, indice, vector) => {
      if (valorActual.parent && valorActual.parent.id) {
        let cate = this.searchCategory(valorAnterior, valorActual.parent.id);

        if (cate) {
          if (!cate.childList) {
            cate.childList = [];
          }
          cate.childList.push(valorActual);
        }
      } else {
        valorAnterior.push(valorActual);
      }
      return valorAnterior;
    }, [{
      id: null,
      image: null,
      name: "Todos",
      parent: null
    } as Category]);
  }

  loadListCategories(){

    super.getAll(createUrlQuery({sort:'name,ASC', properties:[{ 'key': "deletedAt" , 'value': "" }]})).subscribe((res: Pageable<Category>) => {
      this.listCategories = this.setListCategories(res.page);
    });
  }

  getFullAll(): Observable<Category[]>  {
    return super.getAll() as Observable<Category[]>;
  }

  getAllActives(): Observable<Category[]> {
    if (this.listCategories.length > 0) {
      return of(this.listCategories);
    }

    const subject = new Subject<Category[]>();

    super.getAll(createUrlQuery({sort:'name,ASC', properties:[{ 'key': "deletedAt" , 'value': "" }]})).subscribe((res: Pageable<Category>) => {
      this.listCategories = this.setListCategories(res.page);
      subject.next(this.listCategories);
    });

    return subject.asObservable();
  }

  /*
   ----------  Favourite Category Function  ----------
  */

  // Get Favourite Category based on userId
  /*	getUsersFavouriteCategory() {
			const user = this.authService.getLoggedInUser();
			this.favouriteCategorys = this.db.list('favouriteCategorys', (ref) =>
				ref.orderByChild('userId').equalTo(user.$key)
			);
			return this.favouriteCategorys;
		}
		// Adding New product to favourite if logged else to localStorage
		addFavouriteCategory(data: Category): void {
			let a: Category[];
			a = JSON.parse(localStorage.getItem('avf_item')) || [];
			a.push(data);
			this.toastrService.info('Adding Category', 'Adding Category as Favourite');
			setTimeout(() => {
				localStorage.setItem('avf_item', JSON.stringify(a));
				this.calculateLocalFavProdCounts();
			}, 1500);
		}

		// Fetching unsigned users favourite proucts
		getLocalFavouriteCategorys(): Category[] {
			const categories: Category[] = JSON.parse(localStorage.getItem('avf_item')) || [];

			return categories;
		}

		// Removing Favourite Category from Database
		removeFavourite(key: string) {
			this.favouriteCategorys.remove(key);
		}

		// Removing Favourite Category from localStorage
		removeLocalFavourite(product: Category) {
			const categories: Category[] = JSON.parse(localStorage.getItem('avf_item'));

			for (let i = 0; i < categories.length; i++) {
				if (categories[i].productId === product.productId) {
					categories.splice(i, 1);
					break;
				}
			}
			// ReAdding the categories after remove
			localStorage.setItem('avf_item', JSON.stringify(categories));

			this.calculateLocalFavProdCounts();
		}

		// Returning Local Categorys Count
		calculateLocalFavProdCounts() {
			this.navbarFavProdCount = this.getLocalFavouriteCategorys().length;
		}

	*/
  /*
   ----------  Cart Category Function  ----------
  */

  // Adding new Category to cart db if logged in else localStorage
}
