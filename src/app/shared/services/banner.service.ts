import { Injectable } from '@angular/core';
import { Banner } from '../models/banner';
import { HttpClient } from '@angular/common/http';
import { BasicService } from './basic-services/basic.service';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Filter } from '../models/filter';

@Injectable({
  providedIn: "root",
})
export class BannerService extends BasicService<Banner> {
	ovs: Promise<Banner[]> = null
  constructor(http: HttpClient) {
    super(http, '/api/banner');
    this.ovs = this.http.get<Banner[]>(`/api/banner`).toPromise()
  }

  getAllBanners() {
  	return this.ovs;
  }

}
