

import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: "root",
})
export class ReportService{
  constructor(private http: HttpClient) { }

  getReprtGeneralByNode(id: number): Observable<any>{
    return this.http.post(`/api/reportes/general/balance`,id, { responseType: 'arraybuffer' }).pipe(map(res => {
    	var file = new Blob([res],{ type: 'application/vnd.ms-excel' })
    	var fileURL = URL.createObjectURL(file);
  		return fileURL;
    }));
  }

getReprtAllBalances(): Observable<any>{
  return this.http.post(`/api/reportes/general/balances`, null, { responseType: 'arraybuffer' }).pipe(map(res => {
    var file = new Blob([res],{ type: 'application/vnd.ms-excel' })
    var fileURL = URL.createObjectURL(file);
    return fileURL;
  }));
}

  getReprtGeneralByCarts(id: number): Observable<any>{
    return this.http.post(`/api/reportes/general/ventas`,id, { responseType: 'arraybuffer' }).pipe(map(res => {
    	var file = new Blob([res],{ type: 'application/vnd.ms-excel' })
    	var fileURL = URL.createObjectURL(file);
  		return fileURL;
    }));
  }

  getPDFByCart(id: number): Observable<any>{
    return this.http.post(`/api/generator/pdf/purchase`,id, { responseType: 'arraybuffer' }).pipe(map(res => {
    	var file = new Blob([res],{ type: 'application/pdf' })
    	var fileURL = URL.createObjectURL(file);
  		return fileURL;
    }));
  }

}
