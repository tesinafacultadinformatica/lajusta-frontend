import { Injectable } from "@angular/core";
import { Node } from "../models/node";
import { Observable, of, Subject } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { BasicService } from "./basic-services/basic.service";

@Injectable({
  providedIn: "root",
})
export class NewsletterService {

  constructor(http: HttpClient) { }
  
  create(formData: any): Observable<T> {
    return this.http.post(`${this.url}`, formData).pipe(
      map((p) => {
        this.updateData.next(true);
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        return <T>p;
      })
    );
  }
}
