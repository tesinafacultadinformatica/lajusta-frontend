import { Injectable } from "@angular/core";
import { User } from "../models/user";
import { HttpClient } from "@angular/common/http";
import { Observable, Subject } from "rxjs";
import { BasicService } from './basic-services/basic.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: "root",
})
export class UserService extends BasicService<User> {
  constructor(http: HttpClient) {
    super(http, "/api/user");
  }

  register(formData: any): Observable<any> {
    return this.http.post(`/api/user/signup`, formData);
  }

  recoveryPassword(formData: any): Observable<any> {
    return this.http.post(`/api/email/recovery`,formData);
  }

  changePassword(formData: any): Observable<any> {
    return this.http.post(`/api/email/recovery/confirm`, formData);
  }

  getByEmail(filter: string){
    return this.http.get(`api/user?filter=email,${filter.toString()}`)
  }

}
