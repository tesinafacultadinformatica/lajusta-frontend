import { Injectable } from "@angular/core";
import { Observable, BehaviorSubject, of } from "rxjs";
import { LoginUser, User } from "../models/user";
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators'
import { Role } from '../enums/role.enum';
import { Image } from '../models/image';
import jwt_decode from "jwt-decode";
@Injectable()
export class AuthService {
  private currentUserSubject: BehaviorSubject<LoginUser>;
  private userImage: BehaviorSubject<Image> = new BehaviorSubject<Image>(null);
  public currentUser: Observable<LoginUser>;

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<LoginUser>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();

    const user = this.currentUserSubject.value;
    if (user !== null) {
        this.userImage.next(user.user.image);
    }

  }

  public get currentUserValue(): LoginUser {
      return this.currentUserSubject.value;
  }

  getUserImage(): Observable<Image> {
    return this.userImage.asObservable();
  }

  setCurrentUser(userL: LoginUser){
    localStorage.removeItem('currentUser');
    localStorage.setItem('currentUser', JSON.stringify(userL));
    this.currentUserSubject.next(userL);
    this.userImage.next(userL.user.image);
  }

  refreshLocalCurrentUser(userData: User){
    const user = this.currentUserSubject.value;
    user.user = userData;
    this.setCurrentUser(user);
  }

  isLoggedInBack() {
    var decoded = jwt_decode(this.currentUserValue.value);
    if((decoded as any).exp === undefined){
      return false;
    }
    const currentDate = new Date(0);
    const tokenExpDate = currentDate.setUTCSeconds((decoded as any).exp);
    return tokenExpDate.valueOf() > new Date().valueOf();
  }

  login(username: string, password: string): Observable<any> {
        return this.http.post<any>(`${environment.url_api_base}/api/token/generate-token`, { userName: username, userPassword: password })
          .pipe(map(user => {
              // store user details and jwt token in local storage to keep user logged in between page refreshes
              this.setCurrentUser(user);
              return user;
          }));
  }

  logout() {
      localStorage.removeItem('currentUser');
      sessionStorage.removeItem('instance');
      this.currentUserSubject.next(null);
      this.userImage.next(null);
  }

  isAdmin(): boolean {
    const user = this.currentUserSubject.value;
    if (user !== null) {
      return (user.user.role === parseInt(Role.Administrador));
    }
    return false;
  }

  isLoggedIn(): boolean{
    return this.currentUserSubject.value != null;
  }

}

