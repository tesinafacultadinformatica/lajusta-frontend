import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Expense } from "../models/expense";
import { BasicService } from './basic-services/basic.service';

@Injectable({
  providedIn: "root",
})
export class ExpenseService extends BasicService<Expense> {

  constructor(http: HttpClient) {
    super(http, '/api/expense');
  }

  
 
}
