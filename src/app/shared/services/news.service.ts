import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { BasicService } from './basic-services/basic.service';
import { News } from '../models/news';


@Injectable({
  providedIn: "root",
})
export class NewsService extends BasicService<News> {

  constructor(http: HttpClient) {
    super(http, "/api/news");
  }

}
