import { Injectable } from "@angular/core";


import { HttpClient } from "@angular/common/http";
import { User } from "../models/user";
import { BasicService } from './basic-services/basic.service';
import { Observable, Subject } from 'rxjs';
import { Pageable } from "../models/pageable";

@Injectable({
  providedIn: "root",
})
export class PersonalService extends BasicService<User> {
  constructor(http: HttpClient) {
    super(http, "/api/user/staff");
  }

  getStaff(): Observable<User>{
    return this.http.get<User>(`${this.url}`);
  }

 

}
