import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { Balance } from "../models/balance";
import { BasicService } from './basic-services/basic.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: "root",
})
export class BalanceService extends BasicService<Balance>  {

  constructor(http: HttpClient) {
    super(http, '/api/balance');
  }

  close(idBalance:number): Observable<any> {
    return this.http.post('/api/balance/close', {id: idBalance});
  }

}
