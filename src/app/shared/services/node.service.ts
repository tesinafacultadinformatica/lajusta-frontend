import { Injectable } from "@angular/core";
import { Node } from "../models/node";
import { Observable, of, Subject } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { BasicService } from "./basic-services/basic.service";

@Injectable({
  providedIn: "root",
})
export class NodeService extends BasicService<Node> {

  constructor(http: HttpClient) {
    super(http, "/api/node");
  }

}
