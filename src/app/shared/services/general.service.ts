import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable, Subject } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { General, DateTimeNode } from "../models/general";
import { BasicService } from "./basic-services/basic.service";
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: "root",
})
export class GeneralService extends BasicService<General> {
  private downPage:Date = null;
  //private isActive: boolean = false;

  isActive: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(http: HttpClient) {
    super(http, "/api/general");
  }

  getActive(): Observable<General>{
    return this.http.get<General>(`${this.url}/active`);
  }

  getNextActive(): Observable<General>{
    return this.http.get<General>(`${this.url}/nextActive`);
  }

  setActivePage(){
    this.getActive().subscribe((res)=> {
      if(res){
       // this.downPage = res.dateDownPage;
       this.isActive.next(true);
      } else {
        this.isActive.next(false);
      }
    });
  };

  pageIsActive(): Observable<boolean> {
    return this.isActive.asObservable();
  }

/*
    this.isActive = this.isActive && this.downPage && (new Date().valueOf() < new Date(this.downPage).valueOf());
    if(!this.isActive){ this.downPage = null;};
    return this.isActive;
*/

  getGeneralNodes(idGeneral: number, hasFridge:boolean = false): Observable<DateTimeNode[]> {
    const subject = new Subject<DateTimeNode[]>();
    super.getById(idGeneral).subscribe((res) => {
      subject.next(hasFridge ? res.activeNodes.filter((n) => n.node.hasFridge) : res.activeNodes);
    });

    return subject.asObservable();
  }
}
