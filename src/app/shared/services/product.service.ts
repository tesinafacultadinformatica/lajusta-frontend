import { Injectable } from "@angular/core";
import { Product } from "../models/product";
import { Observable, of, Subject } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { createUrlQuery } from "../helper/util/create-url-query";
import { stringify } from "querystring";
import { BasicService } from './basic-services/basic.service';

@Injectable({
  providedIn: "root",
})
export class ProductService extends BasicService<Product> {
  constructor(http: HttpClient) {
    super(http, "/api/product");
  }

/*
  getData(
    filter: string = "",
    sort: string = "",
    order: string = "",
    page: number = 1,
    perPage: number = 5,
    initTotal: Function = () => {},
    categoryId: number = null,
    inPromotion: boolean = false
  ): Observable<Product[]> {
    //console.log(createUrlQuery({filter: filter, sort: {field: sort, order: order}, pagination: { page, perPage }}));
    let filterQuery = createUrlQuery({
      filter: filter,
      sort: { field: sort, order: order },
      pagination: { page, perPage },
    });
    if (categoryId) {
      let query: any = {};
      query.categoryId = JSON.stringify(categoryId);
      filterQuery += "&" + stringify(query);
      //filterQuery.append('categoryId', JSON.stringify(categoryId))
    }
    if (inPromotion) {
      let query: any = {};
      query.isPromotion = JSON.stringify(inPromotion);
      filterQuery += "&" + stringify(query);
      //filterQuery.append('isPromotion', JSON.stringify(isPromotion))
    }
    return this.getAll(filterQuery);
  }
  */

  /*
   ----------  Favourite Product Function  ----------
  */

  // Get Favourite Product based on user: { id:
  /*	getUsersFavouriteProduct() {
			const user = this.authService.getLoggedInUser();
			this.favouriteProducts = this.db.list('favouriteProducts', (ref) =>
				ref.orderByChild('user: { id:').equalTo(user.$key)
			);
			return this.favouriteProducts;
		}
		// Adding New product to favourite if logged else to localStorage
		addFavouriteProduct(data: Product): void {
			let a: Product[];
			a = JSON.parse(localStorage.getItem('avf_item')) || [];
			a.push(data);
			this.toastrService.info('Adding Product', 'Adding Product as Favourite');
			setTimeout(() => {
				localStorage.setItem('avf_item', JSON.stringify(a));
				this.calculateLocalFavProdCounts();
			}, 1500);
		}

		// Fetching unsigned users favourite proucts
		getLocalFavouriteProducts(): Product[] {
			const products: Product[] = JSON.parse(localStorage.getItem('avf_item')) || [];

			return products;
		}

		// Removing Favourite Product from Database
		removeFavourite(key: string) {
			this.favouriteProducts.remove(key);
		}

		// Removing Favourite Product from localStorage
		removeLocalFavourite(product: Product) {
			const products: Product[] = JSON.parse(localStorage.getItem('avf_item'));

			for (let i = 0; i < products.length; i++) {
				if (products[i].productId === product.productId) {
					products.splice(i, 1);
					break;
				}
			}
			// ReAdding the products after remove
			localStorage.setItem('avf_item', JSON.stringify(products));

			this.calculateLocalFavProdCounts();
		}

		// Returning Local Products Count
		calculateLocalFavProdCounts() {
			this.navbarFavProdCount = this.getLocalFavouriteProducts().length;
		}

	*/
  /*
   ----------  Cart Product Function  ----------
  */

  // Adding new Product to cart db if logged in else localStorage
}
