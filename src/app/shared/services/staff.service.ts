import { Injectable } from "@angular/core";


import { HttpClient } from "@angular/common/http";
import { Staff } from "../models/staff";
import { BasicService } from './basic-services/basic.service';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: "root",
})
export class StaffService extends BasicService<Staff> {
  constructor(http: HttpClient) {
    super(http, "/api/staff");
  }

}
