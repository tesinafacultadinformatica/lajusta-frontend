import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Unit } from "../models/unit";
import { BasicService } from './basic-services/basic.service';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: "root",
})
export class UnitService extends BasicService<Unit> {
  constructor(http: HttpClient) {
    super(http, "/api/unit");
  }

}
