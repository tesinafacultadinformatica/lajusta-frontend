import { Injectable } from "@angular/core";

import { HttpClient } from "@angular/common/http";
import { Purchase } from "../models/purchase";
import { BasicService } from './basic-services/basic.service';

@Injectable({
  providedIn: "root",
})
export class PurchaseService  extends BasicService<Purchase> {
  constructor(http: HttpClient) {
    super(http, "/api/purchase");
  }
}
