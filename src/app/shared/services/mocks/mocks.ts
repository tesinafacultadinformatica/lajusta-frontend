import { Product } from "../../models/product";
import { User } from '../../models/user';
import { Category } from '../../models/category';
import { Node } from '../../models/node';
import { Cart } from '../../models/cart';
import { Producer } from '../../models/producer';
import { General, DateTimeNode } from '../../models/general';
import { Banner } from '../../models/banner';
import { Unit } from '../../models/unit';
import { Staff } from '../../models/staff';
import { Balance } from '../../models/balance';
import { Expense } from '../../models/expense';
import { Purchase } from '../../models/purchase';
import { Address } from '../../models/address';
/*
const addressMock: Address[] = [
  { id: 1, street: "8", number: 1420, description: "entre 61 y 62", longitude: -57.94142851184983, latitude: -34.92297879916583 },
  { id: 2, street: "6", number: 592, description: "entre 43 y 44", longitude: -57.9543936, latitude: -34.909473 },
  { id: 3, street: "44", number: 733, description: "entre 9 y 10", longitude: -57.960330448416826, latitude: -34.91312398134701 },
  { id: 4, street: "1 y 47", longitude: -57.94712264841676, latitude: -34.907447681044275 },
  { id: 5, street: "60", description: "esquina 119", longitude: -57.93119664841664, latitude: -34.910698481217594 },
  { id: 6, street: "522", description: "entre 6 y 7", longitude: -57.98009331183375, latitude: -34.8915721987457 },
  { id: 7, street: "26",  number: 1629, description: "entre 65 y 66", longitude: -57.955426911833655, latitude: -34.943494599440506 },
  { id: 8, street: "421", description: "esquina 153 bis", longitude: -58.12657351183375, latitude: -34.896770898815284 },
];

export const nodesMock: Node[] = [
    { id: 1, name: "Centro Cultural La Hormiguera", address: addressMock[0], description: null, phone: "0303456",
    image: {id:1, name:"LaH", type:"jpg", value: "../../../../assets/nodes/hormiga.jpg"}
    , hasFridge: false },
    { id: 2, name: "ADULP", address: addressMock[1], description: null, phone: "0303456",
    image: {id:2, name:"ADULP", type:"jpg", value: "../../../../assets/nodes/adulp.jpg"}, hasFridge: true },
    { id: 3, name: "ATULP", address: addressMock[2], description: null, phone: "0303456",
    image: {id:3, name:"ATULP", type:"jpg", value: "../../../../assets/nodes/atulp.jpg"}, hasFridge: true },
    { id: 4, name: "Facultad de Ingeniería", address: addressMock[3], description: null, phone: "0303456",
    image: {id:4, name:"Ingenieria", type:"jpg", value:"../../../../assets/nodes/ingenieria.jpg"}, hasFridge: false },
    { id: 5, name: "Facultad de Agronomía", address: addressMock[4], description: null, phone: "0303456",
    image: {id:5, name:"Agro", type:"jpg", value: "../../../../assets/nodes/agronomia.jpg"}, hasFridge: false },
    { id: 6, name: "Evita-Descamisados", address: addressMock[5], description: null, phone: "0303456", image: null, hasFridge: true },
    { id: 7, name: "CNCT", address: addressMock[6], description: null, phone: "0303456",
    image: {id:6, name:"CNCT", type:"jpg", value: "../../../../assets/nodes/cnct.jpg"},  hasFridge: true },
    { id: 8, name: "Arturo Seguí: Casa de Belén", address: addressMock[7], description: "B° Pro.Cre.Ar", phone: "0303456", image: null, hasFridge: true }
];

export const categoriesMock: Category[] = [
    { id: null, name: "Todos", parent: { id: null }},
    { id: 1, name: "Ofertas", parent: { id: null }},
    { id: 3, name: "Dulces", parent: { id: null }},
    { id: 4, name: "Alfajores", parent: { id: 3 }},
    { id: 5, name: "Miel", parent: { id: 3 }},
    { id: 6, name: "Huevos", parent: { id: null }},
    { id: 7, name: "Pan", parent: { id: null }},
    { id: 8, name: "Lacteos", parent: { id: null }},
    { id: 9, name: "Queso", parent: { id: 8 }},
    { id: 10, name: "Leche", parent: { id: 8 }},
    { id: 11, name: "Otros", parent: { id: null }},
    { id: 12, name: "Semillas", parent: { id: 11 }},
    { id: 14, name: "Yerba", parent: { id: 11 }},
    { id: 15, name: "Maicena", parent: { id: 4 }}
];

export const usersMock: User[] = [
    { id:1,email:'pepe@pepe.com',firstName:'Pepe', lastName:'Prueba', phone: '123123', role: 1, age:18 },
    { id:2,email:'pepe2@pepe.com',firstName:'Pepe2', lastName:'Prueba2', phone: '222222', role: 2, age:22 }
];


export const unitsMock:Unit[] = [
	{id:1, description:'Unidad', code: "unid." },
  {id:2, description:'Unidades', code: "unides." },
	{id:3, description:'Kilo', code: "Kg." },
	{id:4, description:'Kilos', code: "Kgs." },
	{id:5, description:'Gramos', code: "Grs." },
	{id:6, description:'Docena', code: "docena" },
	{id:7, description:'Media Docena', code: "6 unides." },
	{id:8, description:'Litro', code: "Ltr." },
	{id:9, description:'Litros', code: "Ltrs." },
	{id:10, description:'Docenas', code: "docenas" },
];


export const dateTimeNodeMock:DateTimeNode[] = [
	{id:1, node:nodesMock[1], dateTimeFrom:new Date(new Date().getTime()+(4*24*60*60*1000 - (8*60*60*1000))), dateTimeTo: new Date(new Date().getTime()+(4*24*60*60*1000))},
	{id:2, node:nodesMock[1], dateTimeFrom:new Date(new Date().getTime()+(5*24*60*60*1000 - (8*60*60*1000))), dateTimeTo: new Date(new Date().getTime()+(5*24*60*60*1000))},
	{id:3, node:nodesMock[3], dateTimeFrom:new Date(new Date().getTime()+(5*24*60*60*1000 - (8*60*60*1000))), dateTimeTo: new Date(new Date().getTime()+(5*24*60*60*1000))},
	{id:4, node:nodesMock[4], dateTimeFrom:new Date(new Date().getTime()+(5*24*60*60*1000 - (8*60*60*1000))), dateTimeTo: new Date(new Date().getTime()+(5*24*60*60*1000))},
	{id:5, node:nodesMock[7], dateTimeFrom:new Date(new Date().getTime()+(5*24*60*60*1000 - (8*60*60*1000))), dateTimeTo: new Date(new Date().getTime()+(5*24*60*60*1000))},
	{id:6, node:nodesMock[8], dateTimeFrom:new Date(new Date().getTime()+(5*24*60*60*1000 - (8*60*60*1000))), dateTimeTo: new Date(new Date().getTime()+(5*24*60*60*1000))},
];

export const generalsMock:General[] = [
  {id:1, user: { id:1}, dateActivePage: new Date(1231232131), dateDownPage: new Date(),
	  activeNodes:[dateTimeNodeMock[0] ,dateTimeNodeMock[1] ,dateTimeNodeMock[3] ,dateTimeNodeMock[4] ] },
  {id:2, user: { id:2}, dateActivePage: new Date, dateDownPage: new Date,
	  activeNodes:[dateTimeNodeMock[1] ,dateTimeNodeMock[2] ,dateTimeNodeMock[3] ,dateTimeNodeMock[4] ] }
];

export const balancesMock:Balance[] = [
	{id:1, totalExpenses:123.52, totalSales:2000, openDate: new Date(), closeDate: new Date(), balance: 1876.48, generalId: generalsMock[0].id},
];

export const staffsMock:Staff[] = [
	{id:1, description:'Soledad' },
  {id:2, description:'Emanuel' },
	{id:3, description:'Equipo' }
];

export const producersMock: Producer[] = [
    { id:1, email:'pepe@pepe.com',name:'Pepe Prueba', origin:'Pepe Prueba', phone: '123123', isCompany: false, address: addressMock[7]},
    { id:2, email:'pepe2@pepe.com',name:'La Carmelita',origin:'La Carmelita', phone: '222222', isCompany: true, address: addressMock[5] },
    { id:3, email:'capi@espacio.com',name:'Capitan del espacio',origin:'Capitan del espacio', phone: '444', isCompany: true, address: addressMock[6]  }
];

export const productsMock: Product[] = [{
	id: 1,
	user: { id: 1},
	producers: [],
	title: "Alfajores",
	description: "Alfajores de primera calidad cometa, ideal para regalo",
	brand: null,
	unitQuantity: 12,
  unit: {id: 1, code:"Unidad", description:"Unidad"},
	unitDescription: "Docena",
	buyPrice: 10,
	price: 12,
	stock: 4,
	isPromotion: true,
	percent: null,
	needCold:false,
	categories: [{
		id: 1,
		name: "Dulces",
		parent: { id: null}
	}, {
		id: 2,
		name: "Alfajores",
		parent: { id: 1}
	}],
	images: [{
		id: 1,
    name: "Alfajores 1",
    type: "jpg",
		value: "../../../../assets/products/Alfajores1.jpg",
		isMain: false
	}, {
		id: 2,
		name: "Alfajores 2",
    type: "jpg",
		value: "../../../../assets/products/Alfajores2.jpg",
		isMain: false
	}, {
		id: 3,
		name: "Alfajores 3",
    type: "jpg",
		value: "../../../../assets/products/Alfajores3.jpg",
		isMain: true
	}]
},
{
	id: 2,
	user: { id: 2},
	producers: [],
	title: "Bolson",
	description: "Bolson de verduras varias",
	brand: null,
	unitQuantity: 1,
  unit: {id: 1, code:"Unidad", description:"Unidad"},
	unitDescription: null,
	buyPrice: 80,
	price: 100,
	stock: 4,
	isPromotion: false,
	percent: null,
	needCold:false,
	categories: [{
		id: 1,
		name: "Verduras",
		parent: { id: null}
	}, {
		id: 2,
		name: "Bolson",
		parent: { id: 1}
	}],
	images: [{
		id: 1,
		name: "Bolson 1",
    type: "jpg",
		value: "../../../../assets/products/Bolson1.jpg",
		isMain: false
	}, {
		id: 2,
		name: "Bolson 2",
    type: "jpg",
		value: "../../../../assets/products/Bolson2.jpg",
		isMain: true
	}, {
		id: 3,
		name: "Bolson 3",
    type: "jpg",
		value: "../../../../assets/products/Bolson3.jpg",
		isMain: false
	}]
},
{
	id: 3,
	user: { id: 2},
	producers: null,
	title: "Huevos",
	description: "Huevos caseros de la zona",
	brand: null,
	unitQuantity: 12,
  unit: {id: 1, code:"Unidad", description:"Unidad"},
	unitDescription: "docena",
	buyPrice: 40,
	price: 120,
	stock: 4,
	isPromotion: true,
	percent: null,
	needCold:false,
	categories: [{
		id: 1,
		name: "Huevos",
		parent: { id: null}
	}],
	images: [{
		id: 1,
		name: "Huevos 1",
    type: "jpg",
		value: "../../../../assets/products/Huevos1.jpg",
		isMain: false
	}, {
		id: 2,
		name: "Huevos 2",
    type: "jpg",
		value: "../../../../assets/products/Huevos2.jpg",
		isMain: true
	}, {
		id: 3,
		name: "Huevos 3",
    type: "jpg",
		value: "../../../../assets/products/Huevos3.jpg",
		isMain: false
	}]
},
{
	id: 4,
	user: { id: 2},
	producers: null,
	title: "Miel",
	description: "Miel Pura Cordobesa",
	brand: null,
	unitQuantity: 1,
  unit: {id: 1, code:"Unidad", description:"Unidad"},
	unitDescription: null,
	buyPrice: 40,
	price: 120,
	stock: 4,
	isPromotion: false,
	percent: null,
	needCold:false,
	categories: [{
		id: 1,
		name: "Miel",
		parent: { id: null},
	}],
	images: [{
		id: 1,
		name: "Miel 1",
    type: "jpg",
		value: "../../../../assets/products/Miel1.jpg",
		isMain: false
	}, {
		id: 2,
		name: "Miel 2",
    type: "jpg",
		value: "../../../../assets/products/Miel2.jpg",
		isMain: true
	}, {
		id: 3,
		name: "Miel 3",
    type: "jpg",
		value: "../../../../assets/products/Miel3.jpg",
		isMain: false
	}, {
		id: 4,
		name: "Miel 4",
    type: "jpg",
		value: "../../../../assets/products/Miel4.jpg",
		isMain: false
	}]
},
{
	id: 5,
	user: { id: 2},
	producers: null,
	title: "Pan",
	description: "Pan Caserito",
	brand: null,
	unitQuantity: 1,
  unit: {id: 1, code:"Unidad", description:"Unidad"},
	unitDescription: null,
	buyPrice: 10,
	price: 20,
	stock: 12,
	isPromotion: true,
	percent: null,
	needCold:false,
	categories: [{
		id: 1,
		name: "Pan",
		parent: { id: null}
	}],
	images: [{
		id: 1,
		name: "Pan 1",
    type: "jpg",
		value: "../../../../assets/products/Pan1.jpg",
		isMain: true
	}]
},
{
	id: 6,
	user: { id: 2},
	producers: null,
	title: "Queso",
	description: "Queso Saborizado",
	brand: null,
	unitQuantity: 1,
  unit: {id: 1, code:"Unidad", description:"Unidad"},
	unitDescription: null,
	buyPrice: 10,
	price: 20,
	stock: 12,
	isPromotion: true,
	percent: null,
	needCold:false,
	categories: [{
		id: 1,
		name: "Queso",
		parent: { id: null}
	}],
	images: [{
		id: 1,
		name: "Queso 1",
    type: "jpg",
		value: "../../../../assets/products/Queso1.jpg",
		isMain: true
	}]
},
{
	id: 7,
	user: { id: 2},
	producers: null,
	title: "Semillas",
	description: "Semillas Caserito",
	brand: null,
	unitQuantity: 1,
  unit: {id: 1, code:"Unidad", description:"Unidad"},
	unitDescription: null,
	buyPrice: 10,
	price: 20,
	stock: 12,
	isPromotion: false,
	percent: null,
	needCold:false,
	categories: [{
		id: 1,
		name: "Semillas",
		parent: { id: null }
	}],
	images: [{
		id: 1,
		name: "Semillas 1",
    type: "jpg",
		value: "../../../../assets/products/Semillas1.jpg",
		isMain: true
	}]
},
{
	id: 8,
	user: { id: 2},
	producers: null,
	title: "Yerba",
	description: "Yerba Caserito",
	brand: null,
	unitQuantity: 1,
  unit: {id: 1, code:"Unidad", description:"Unidad"},
	unitDescription: null,
	buyPrice: 10,
	price: 20,
	stock: 12,
	isPromotion: true,
	percent: null,
	needCold:false,
	categories: [{
		id: 1,
		name: "Yerba",
		parent: { id: null}
	}],
	images: [{
		id: 1,
		name: "Yerba 1",
    type: "jpg",
		value: "../../../../assets/products/Yerba1.jpg",
		isMain: true
	}, {
		id: 2,
		name: "Yerba 2",
    type: "jpg",
		value: "../../../../assets/products/Yerba2.jpg",
		isMain: true
	}]
}
];

export const expensesMock:Expense[] = [
  {id:1, staff:staffsMock[0], service:'nafta', quantity:1, price: 1876.48, total: 1876.48, date: new Date(), balanceDate: new Date(), generalId: generalsMock[0].id},
];

export const purchasesMock:Purchase[] = [
 // {id:1, producer:producersMock[0], product:productsMock[0], quantity:1, price: 1876.48, total: 1876.48, generalId: generalsMock[0].id},
];
/*



export const cartMock:Cart[] = [
    {id:1, nodeDate:dateTimeNodeMock[0],user: { id:1}, saleDate: new Date(1231232131), possibleDeliveryDate: null, approvedAt: new Date, total: 12,
        cartProducts:[{product:productsMock[1], quantity:1, price: 12, isCanceled:false}, {product:productsMock[2], quantity:0, price: 5, isCanceled:true} ] },
		{id:2, nodeDate:dateTimeNodeMock[3],user: { id:2}, saleDate: new Date, possibleDeliveryDate: new Date, approvedAt: null,  total: 12,
			cartProducts:[{product:productsMock[3], quantity:1, price: 12, isCanceled:false}, {product:productsMock[4], quantity:0, price: 5, isCanceled:true} ] },
			{id:3, nodeDate:dateTimeNodeMock[3],user: { id:2}, saleDate: new Date, deletedAt: new Date, possibleDeliveryDate: new Date, approvedAt: null,  total: 0,
				cartProducts:[{product:productsMock[3], quantity:0, price: 12, isCanceled:true}, {product:productsMock[4], quantity:0, price: 5, isCanceled:true} ] },
		];
export const bannersMock:Banner[] =
[{
	id: 1,
	title: 'Yerba natural, directo del proveedor',
	subtitle: 'La mejor calidad',
	text: 'Traida desde Entre Rios, directo a tus manos',
	image: `../../../../assets/banner/img_1.jpg`,
	url: null

  }, {
	id: 2,
	title: 'Huevos de campo',
	subtitle: 'De gallinas criadas al aire libre',
	text: 'Compre ya!',
	image: `../../../../assets/banner/img_2.jpg`,
	url: null

  }, {
	id: 3,
	title: 'Verduras y frutas variadas',
	subtitle: 'De los chacareros de la zona',
	text: 'Fresca y natural',
	image: `../../../../assets/banner/img_3.jpg`,
	url: null

  }, {
	id: 4,
	title: 'Entrega inmediata',
	subtitle: null,
	text: 'Acordamos lugar de entrega en alguno de nuestros puntos.',
	image: `../../../../assets/banner/img_4.jpg`,
	url: null

  }];
*/
