import { Injectable } from "@angular/core";
import { CartItem, Cart } from "../models/cart";
import { BehaviorSubject, Observable, of, Subject } from "rxjs";
import { Product } from "../models/product";
import { ToastrService } from "ngx-toastr";
import { HttpClient } from "@angular/common/http";
import { BasicService } from './basic-services/basic.service';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Role } from "../enums/role.enum";
import { AuthService } from "./auth.service";

@Injectable({
  providedIn: "root",
})
export class CartService extends BasicService<Cart> {
  cartItems: CartItem[] = [];
  totalPrice: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  totalQuantity: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  cartItemsSub: BehaviorSubject<CartItem[]> = new BehaviorSubject<CartItem[]>(
    null
  );

  constructor(http: HttpClient, private toastrService: ToastrService,  private authService: AuthService) {
    super(http, '/api/cart');
    this.calculateTotalPrice();
    this.getUpdateData().subscribe( () => {this.removeListItems()})
  }

  getTotalPrice(): Observable<number> {
    return this.totalPrice.asObservable();
  }

  getTotalQuantity(): Observable<number> {
    return this.totalQuantity.asObservable();
  }

  getCartItems(): Observable<CartItem[]> {
    return this.cartItemsSub.asObservable();
  }

  refresh() {
    const items: CartItem[] = JSON.parse(localStorage.getItem("list_item"));
    this.cartItemsSub.next(items);
    this.calculateTotalPrice();
  }

  addToCart(product: Product): void {
    let listCartItems: CartItem[];

    listCartItems = JSON.parse(localStorage.getItem("list_item")) || [];
    if (!listCartItems.some((item) => item.product.id == product.id)) {
      listCartItems.push(new CartItem(product));
      this.toastrService.info("Agregado a la compra", "¡Producto agregado!");
      localStorage.setItem("list_item", JSON.stringify(listCartItems));

      this.cartItemsSub.next(listCartItems);
      this.calculateTotalPrice();
    } else {
      this.toastrService.error(
        "Ya pertenece a la compra",
        "¡Producto no agregado!"
      );
    }
  }

  // Removing cart from local
  removeLocalCartProduct(idProduct: number) {
    const items: CartItem[] = JSON.parse(localStorage.getItem("list_item"));

    for (let i = 0; i < items.length; i++) {
      if (items[i].product.id === idProduct) {
        this.toastrService.warning(
          items[i].product.title,
          "Se elimino en producto"
        );
        items.splice(i, 1);
        break;
      }
    }
    // ReAdding the products after remove
    localStorage.setItem("list_item", JSON.stringify(items));

    this.cartItemsSub.next(items);
    this.calculateTotalPrice();
  }

  // Updating cart from local
  updateLocalCartProduct(idProduct: number, quantity: number) {
    const items: CartItem[] = JSON.parse(localStorage.getItem("list_item"));

    for (let i = 0; i < items.length; i++) {
      if (items[i].product.id === idProduct) {
        items[i].quantity = quantity;
        break;
      }
    }
    // ReAdding the products after update
    localStorage.setItem("list_item", JSON.stringify(items));

    this.cartItemsSub.next(items);
    this.calculateTotalPrice();
  }

  // Fetching Locat CartsProducts
  getLocalCartProducts(): CartItem[] {
    return JSON.parse(localStorage.getItem("list_item")) || [];
  }

  exist(idProduct: number) {
    return this.getLocalCartProducts().find(
      (item) => item.product.id === idProduct
    );
  }

  quantityProduct(idProduct: number) {
    return this.getLocalCartProducts().find(
      (item) => item.product.id === idProduct
    ).quantity;
  }

  calculateTotalPrice() {
    let totalPriceValue: number = 0;
    let totalQuantityValue: number = 0;
    const items: CartItem[] =
      JSON.parse(localStorage.getItem("list_item")) || [];
    if (this.isFromPersonal()){
      for (let currentCartItem of items) {
        totalPriceValue +=
          currentCartItem.quantity * currentCartItem.product.buyPrice;
        totalQuantityValue += currentCartItem.quantity;
      }
    } else {
      for (let currentCartItem of items) {
        totalPriceValue +=
          currentCartItem.quantity * currentCartItem.product.price;
        totalQuantityValue += currentCartItem.quantity;
      }
    }

    this.totalPrice.next(totalPriceValue);
    this.totalQuantity.next(totalQuantityValue);
  }

  private isFromPersonal(){
    return this.authService.currentUserValue != null ? 
    this.authService.currentUserValue.user.role == +Role.Administrador ||
    this.authService.currentUserValue.user.role == +Role.Equipo :
    false;
  }

  private removeListItems(){
    localStorage.removeItem("list_item");
    this.calculateTotalPrice();
  }

  canceled(id: number): Observable<Cart> {
    return this.http.put(`/api/cart/${id}`, {}).pipe(
      map((res) => {
        this.updateData.next(true);
        return <Cart>res;
      })
    );
  }
}
