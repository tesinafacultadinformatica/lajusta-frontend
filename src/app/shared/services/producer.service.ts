import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Producer, ProductProducer } from "../models/producer";
import { BasicService } from './basic-services/basic.service';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: "root",
})
export class ProducerService extends BasicService<Producer> {
  constructor(http: HttpClient) {
    super(http, "/api/producer");
  }

  getProductsByProducer(idProducer: number): Observable<ProductProducer[]> {
    return this.http.get<ProductProducer[]>(`/api/producer/cataloge/${idProducer}`);
  }
  setProductsByProducer(idProducer: number, products: ProductProducer[] ): Observable<any> {
    return this.http.put(`/api/producer/cataloge`,{
      producer: idProducer,
      products: products
    });
  }


  /*
   ----------  Favourite Producer Function  ----------
  */

  // Get Favourite Producer based on userId
  /*	getUsersFavouriteProducer() {
			const user = this.authService.getLoggedInUser();
			this.favouriteProducers = this.db.list('favouriteProducers', (ref) =>
				ref.orderByChild('userId').equalTo(user.$key)
			);
			return this.favouriteProducers;
		}
		// Adding New product to favourite if logged else to localStorage
		addFavouriteProducer(data: Producer): void {
			let a: Producer[];
			a = JSON.parse(localStorage.getItem('avf_item')) || [];
			a.push(data);
			this.toastrService.info('Adding Producer', 'Adding Producer as Favourite');
			setTimeout(() => {
				localStorage.setItem('avf_item', JSON.stringify(a));
				this.calculateLocalFavProdCounts();
			}, 1500);
		}

		// Fetching unsigned users favourite proucts
		getLocalFavouriteProducers(): Producer[] {
			const producers: Producer[] = JSON.parse(localStorage.getItem('avf_item')) || [];

			return producers;
		}

		// Removing Favourite Producer from Database
		removeFavourite(key: string) {
			this.favouriteProducers.remove(key);
		}

		// Removing Favourite Producer from localStorage
		removeLocalFavourite(product: Producer) {
			const producers: Producer[] = JSON.parse(localStorage.getItem('avf_item'));

			for (let i = 0; i < producers.length; i++) {
				if (producers[i].productId === product.productId) {
					producers.splice(i, 1);
					break;
				}
			}
			// ReAdding the producers after remove
			localStorage.setItem('avf_item', JSON.stringify(producers));

			this.calculateLocalFavProdCounts();
		}

		// Returning Local Producers Count
		calculateLocalFavProdCounts() {
			this.navbarFavProdCount = this.getLocalFavouriteProducers().length;
		}

	*/
  /*
   ----------  Cart Producer Function  ----------
  */

  // Adding new Producer to cart db if logged in else localStorage
}
