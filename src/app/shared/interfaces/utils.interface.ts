export interface Order {
    id: number; 
    name: string;
    active: string;
    direction: string;
    cssIcon: string;
}

export interface View {
    title: string;
    cssIcon: string;
}

export interface Bread{
    id: number;
    name: string;
    route: string;
}

export interface MenuConfiguration {
    name: string;
    route: string;
}

export interface SelectedConfig {
    id: number;
    action: string;
}
