import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ShopComponent } from './shop.component';
import { ShopRoutingModule } from './shop-routing.module';
import { SharedModule } from '../shared/shared.module';
import { HomeComponent } from './home/home.component';
import { PublicationComponent } from './publication/publication.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { CardCarouselComponent } from './components/card-carousel/card-carousel.component';
import { CardProductComponent } from './components/card-product/card-product.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { NewsletterComponent } from './components/newsletter/newsletter.component';
import { BreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';
import { HowBuyComponent } from './components/how-buy/how-buy.component';
import { ListCategoryComponent } from './components/list-category/list-category.component';
import { MaterialModule } from '../material.module';
import { CardProducerComponent } from './components/card-producer/card-producer.component';
import { ProducerComponent } from './producer/producer.component';
import { YouTubePlayerModule } from '@angular/youtube-player';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CardNewsComponent } from './components/card-news/card-news.component';
import { NgxNumberSpinnerModule } from 'ngx-number-spinner';

@NgModule({
  imports: [
    ShopRoutingModule,
    CommonModule,
    FormsModule,
    SharedModule,
		MDBBootstrapModule.forRoot(),
    MaterialModule,
    YouTubePlayerModule,
		FlexLayoutModule,
		NgxNumberSpinnerModule
  ],
  declarations: [
    HomeComponent,
    PublicationComponent,
    ProducerComponent,
    ShopComponent,
    CardCarouselComponent,
    CardProductComponent,
    CardProducerComponent,
    CardNewsComponent,
    AboutUsComponent,
    NewsletterComponent,
    BreadcrumbComponent,
    ListCategoryComponent,
    HowBuyComponent
  ],
  exports: [
		FormsModule,
		MDBBootstrapModule
	],
})
export class ShopModule { }
