import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Product } from '../../../shared/models/product';
import { AuthService } from '../../../shared/services/auth.service';
import { ProductService } from '../../../shared/services/product.service';
import { CartService } from '../../../shared/services/cart.service';
import { Image } from 'src/app/shared/models/image';

@Component({
  selector: 'app-card-product',
  templateUrl: './card-product.component.html',
  styleUrls: ['./card-product.component.scss']
})
export class CardProductComponent {
  @Input() product: Product;
  @Input() isActivePage: boolean;
  @Output() openProductDetail = new EventEmitter();

  constructor(public authService: AuthService, private productService: ProductService, private cartService: CartService) { }


  addFavourite(product: Product) { }
  
  addToCart(product: Product) {
    this.cartService.addToCart(product);
  }

  removeToCart(productId: number) {
    this.cartService.removeLocalCartProduct(productId);
  }

  changeToCart(value: number, productId: number) {
    this.cartService.updateLocalCartProduct(productId,value);
  }

  inTheCart(productId:number){
    return this.cartService.exist(productId);
  }

  quantityInTheCart(productId:number){
    return this.cartService.quantityProduct(productId);
  }

  removeProduct(id: number) {
    this.productService.delete(id);
  }

  editProduct(product: Product) { }

  getSrcImage() {
    return (this.product.images && this.product.images.length > 0)? this.product.images.find(image => image.isMain)?.value : '../../../../assets/no_image.jpg';
  }

  getCategories() {
    return this.product.categories;
  }

  openProductDetailModal(){
    this.openProductDetail.emit();
  }

}
