import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { Bread } from 'src/app/shared/interfaces/utils.interface';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss'],
  changeDetection:ChangeDetectionStrategy.OnPush
})
export class BreadcrumbComponent {
  @Input() breads: Bread[];

  //images2 = [1, 2, 3, 4].map((n) => `../../../../assets/banner/img_${n}.jpg`);
}