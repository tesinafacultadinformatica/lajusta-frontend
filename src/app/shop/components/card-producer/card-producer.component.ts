import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Producer } from '../../../shared/models/producer';

@Component({
  selector: 'app-card-producer',
  templateUrl: './card-producer.component.html',
  styleUrls: ['./card-producer.component.scss']
})
export class CardProducerComponent {
  @Input() producer: Producer;

  constructor() { }


}
