import { Component, Input, Output, EventEmitter } from '@angular/core';
import { News } from '../../../shared/models/news';

@Component({
  selector: 'app-card-news',
  templateUrl: './card-news.component.html',
  styleUrls: ['./card-news.component.scss']
})
export class CardNewsComponent {
  @Input() news: News;

  constructor() { }

  goToLink(url: string){
    window.open(url, "_blank");
  }
}
