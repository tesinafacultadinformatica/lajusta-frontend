import { Component, Input, OnInit, HostListener, TemplateRef, ContentChild } from '@angular/core';
import { Banner } from '../../../shared/models/banner';

@Component({
  selector: 'app-card-carousel',
  templateUrl: './card-carousel.component.html',
  styleUrls: ['./card-carousel.component.scss']
})
export class CardCarouselComponent implements OnInit {
  @Input() cards: any[];

  @ContentChild('elementContent') elementTmpl: TemplateRef<any>; 

  CAROUSEL_BREAKPOINTS = [{cut:1320, cards:3, grid:4}, {cut:960, cards:2, grid:6}, {cut:540, cards:1, grid:12} ];

  gridCards:number = 3;

  slides: any = [[]];
  chunk(arr, chunkSize) {
    let R = [];
    for (let i = 0, len = arr.length; i < len; i += chunkSize) {
      R.push(arr.slice(i, i + chunkSize));
    }
    return R;
  }
  ngOnInit() {
    this.onWindowResize();
  }

  @HostListener('window:resize')
  onWindowResize() {
    if (window.innerWidth <= this.CAROUSEL_BREAKPOINTS[2].cut) {      
      this.slides = this.chunk(this.cards, this.CAROUSEL_BREAKPOINTS[2].cards);    
      this.gridCards = this.CAROUSEL_BREAKPOINTS[2].grid;
    }else {
      if (window.innerWidth <= this.CAROUSEL_BREAKPOINTS[1].cut) {      
        this.slides = this.chunk(this.cards, this.CAROUSEL_BREAKPOINTS[1].cards);    
        this.gridCards = this.CAROUSEL_BREAKPOINTS[1].grid;
      }else {
        if (window.innerWidth <= this.CAROUSEL_BREAKPOINTS[0].cut) {      
          this.slides = this.chunk(this.cards, this.CAROUSEL_BREAKPOINTS[0].cards);    
          this.gridCards = this.CAROUSEL_BREAKPOINTS[0].grid;
        }else {
          this.slides = this.chunk(this.cards, 4);    
          this.gridCards = 3;
        }
      }
    }
  }
}