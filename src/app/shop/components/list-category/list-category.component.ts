import { Component, Input, ChangeDetectionStrategy, OnInit } from '@angular/core';
import {FlatTreeControl} from '@angular/cdk/tree';
import { Category } from 'src/app/shared/models/category';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';

interface ExampleFlatNode {
  expandable: boolean;
  name: string;
  level: number;
}

@Component({
  selector: 'app-list-category',
  templateUrl: './list-category.component.html',
  styleUrls: ['./list-category.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListCategoryComponent implements OnInit {
  @Input() categories: Category[];

  private _transformer = (node: Category, level: number) => {
    return {
      expandable: !!node.childList && node.childList.length > 0,
      id: node.id,
      name: node.name,
      level: level,
    };
  }

  treeControl = new FlatTreeControl<ExampleFlatNode>(
      node => node.level, node => node.expandable);

  treeFlattener = new MatTreeFlattener(
      this._transformer, node => node.level, node => node.expandable, node => node.childList);

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  constructor() {
  }
  ngOnInit(): void {
    this.dataSource.data = this.categories;
  }

  hasChild = (_: number, node: ExampleFlatNode) => node.expandable;


  getRouterLink(id:number){
    let link = '/shop/productos'
    if(id){
      link +=  '/' + id;
    }
    return link ;
  }
}
