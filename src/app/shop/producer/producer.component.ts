import {
  Component,
  OnInit,
  Input,
  ViewChild,
  OnDestroy
} from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { ProducerService } from "src/app/shared/services/producer.service";
import { Producer } from "src/app/shared/models/producer";
import { AuthService } from "src/app/shared/services/auth.service";
import { Order, View, Bread } from "src/app/shared/interfaces/utils.interface";
import { SharedService } from "src/app/shared/services/shared.service";
import { ActivatedRoute } from "@angular/router";
import { Category } from "src/app/shared/models/category";
import { CategoryService } from "src/app/shared/services/category.service";
import { PageEvent, MatPaginator } from "@angular/material/paginator";
import { GeneralService } from 'src/app/shared/services/general.service';
import { Subscription } from 'rxjs';
import { Filter } from '../../shared/models/filter';

@Component({
  selector: "app-producer",
  templateUrl: "./producer.component.html",
  styleUrls: ["./producer.component.scss"],
})
export class ProducerComponent implements OnInit, OnDestroy {
  @Input() categories: Category[];
  producerList: Producer[];
  loading = false;
  pageActive: boolean = false;

  public filter: string;
  private properties: Filter[] = [{ key: 'deletedAt', value: 'null' }]
  producersList: Producer[] = [];
  pagedList: Producer[] = [];
  resultsLength: number = 0;

  pageSize: number = 10; //displaying three cards each row
  pageSizeOptions: number[] = [5, 10, 25, 50];

  @ViewChild(MatPaginator) paginator: MatPaginator;

  subscriptionActive: Subscription;

  orders: Order[] = [
    {
      id: 0,
      name: "Predeterminado",
      active: "id",
      direction: "ASC",
      cssIcon: "fas fa-sort-amount-down-alt",
    },
    {
      id: 1,
      name: "Nombre ascendente",
      active: "title",
      direction: "ASC",
      cssIcon: "fas fa-sort-alpha-down",
    },
    {
      id: 2,
      name: "Nombre descendente",
      active: "title",
      direction: "DESC",
      cssIcon: "fas fa-sort-alpha-down-alt",
    },
  ];

  selectedOrder: Order;

  page = 1;
  categoryId = null;

  constructor(
    public authService: AuthService,
    private producerService: ProducerService,
    private categoryService: CategoryService,
    private toastrService: ToastrService,
    private sharedService: SharedService,
    private generalService: GeneralService,
    private route: ActivatedRoute
  ) {
    this.selectedOrder = this.orders[0];
    this.subscriptionActive = this.generalService.pageIsActive().subscribe(
      data => {this.pageActive = data;}
    )
  }

  ngOnInit() {
      this.filter = null;
      this.getProducers();
    //  this.producersList = <GetOrInitializeYourListHere>;
    this.pagedList = this.producersList.slice(0, 3);
  //  this.resultsLength = this.producersList.length;
    //end Pagination
  }

  onChangeOrder(order) {
    this.selectedOrder = order;
    this.paginator.pageIndex = 0;
    this.getProducers();
  }

  getProducers() {
    // this.spinnerService.show();
    this.producerService
      .getData(
        this.filter,
        this.selectedOrder.active,
        this.selectedOrder.direction,
        (this.paginator?.pageIndex ? this.paginator.pageIndex : 0),// + 1,
        this.paginator?.pageSize ? this.paginator.pageSize : 10,
        this.properties,
        (total) => (this.resultsLength = total)
      )
      .subscribe(
        (producers) => {
          // this.spinnerService.hide();
          this.producerList = producers;
        },
        (err) => {
          this.toastrService.error("Error mientras cargaba los Produceros", err);
        }
      );
  }

  // Pagination
  OnPageChange(event: PageEvent) {
    this.getProducers();
  }
  //end Pagination


  ngOnDestroy() {
    this.subscriptionActive.unsubscribe();
  }

  search(text: string) {
    this.filter = text;
    this.getProducers();
  }
}
