import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ShopComponent } from './shop.component';
import { PublicationComponent } from './publication/publication.component';
import { HomeComponent } from './home/home.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ProducerComponent } from './producer/producer.component';


const routes: Routes = [
  {
    path: '',
    component: ShopComponent,
    children: [
      {
        path: 'home',
        component: HomeComponent,
      },
      {
        path: 'nosotros',
        component: AboutUsComponent,
      },
      {
        path: 'productos',
        component: PublicationComponent,
      },
      {
        path: 'productos/:id-category',
        component: PublicationComponent,
      },
      {
        path: 'productores',
        component: ProducerComponent,
      },
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',
      }
      /*,
      {
        path: 'carritos',
        component: CartComponent,
      },
      {
        path: 'productores',
        component: ProducerComponent,
      },*/
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ShopRoutingModule {
}
