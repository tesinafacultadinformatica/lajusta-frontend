import { Component, ChangeDetectionStrategy, OnInit, ViewChild, ElementRef, OnDestroy } from "@angular/core";
import { BannerService } from "src/app/shared/services/banner.service";
import { Banner } from "src/app/shared/models/banner";
import { ProductService } from "src/app/shared/services/product.service";
import { Product } from "src/app/shared/models/product";
import { SharedService } from "src/app/shared/services/shared.service";
import { GeneralService } from "src/app/shared/services/general.service";
import { ActivatedRoute } from "@angular/router";
import { General } from 'src/app/shared/models/general';
import { Subscription } from 'rxjs';

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeComponent implements OnInit, OnDestroy {
  banners: Banner[] = [];
  products: Product[] = [];
  pageActive: boolean = false;
  codeConfirm: string = null;
  general: General = null;
  minDateNode: Date = null;
  @ViewChild('alert', { static: true }) alert: ElementRef;
  @ViewChild('alertBuy', { static: true }) alertBuy: ElementRef;
  subscriptionActive: Subscription;
  recentBuy: any = null;

  constructor(
    private route: ActivatedRoute,
    private bannerService: BannerService,
    private productService: ProductService,
    private generalService: GeneralService,
    private sharedService: SharedService
  ) {

    this.bannerService
      .getAllBanners()
      .then((res: Banner[]) => {
        console.log(res);
        this.banners = res;
      });
    this.productService
      .getData("", "", "", -1, null, [{ key: "isPromotion", value: true }, { key: "deletedAt", value: "null" }])
      .subscribe((res) => {
        this.products = res;
      });

    this.subscriptionActive = this.generalService.pageIsActive().subscribe(
      data => {this.pageActive = data;}
    )
    this.recentBuy = JSON.parse(localStorage.getItem("recent_buy")) || null;
    localStorage.removeItem("recent_buy");

    if(this.pageActive){
      this.generalService.getActive().subscribe((res: General) => {
        this.general = res;
        if(res && res.activeNodes.length >0){
          this.minDateNode = res.activeNodes.map(an => an.day).reduce(function (valor1, valor2) { return new Date(valor1) <  new Date(valor2) ? valor1 : valor2; });
        }
      });
    }else {
      this.generalService.getNextActive().subscribe((res: General) => {
        this.general = res;
        if(res && res.activeNodes.length >0){
          this.minDateNode = res.activeNodes.map(an => an.day).reduce(function (valor1, valor2) { return new Date(valor1) <  new Date(valor2) ? valor1 : valor2; });
        }
      });
    }

  }

  ngOnInit() {
    this.route.queryParams.subscribe((params) => {
      this.codeConfirm = params.emailConfirmationCode;
      if (this.codeConfirm) {
        this.sharedService.showChangePassword(this.codeConfirm);
      }
    });
  }


  ngOnDestroy() {
    this.subscriptionActive.unsubscribe();
  }

  closeAlert() {
    this.alert.nativeElement.classList.remove('show');
    this.alert.nativeElement.classList.add('display-none');
  }

  closeAlertBuy() {
    this.alertBuy.nativeElement.classList.remove('show');
    this.alertBuy.nativeElement.classList.add('display-none');
  }

  openProduct(idx: number) {
    this.sharedService.showProductDetail(idx);
  }
}
