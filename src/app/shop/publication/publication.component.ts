import {
  Component,
  OnInit,
  Input,
  ViewChild,
  OnDestroy
} from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { ProductService } from "src/app/shared/services/product.service";
import { Product } from "src/app/shared/models/product";
import { AuthService } from "src/app/shared/services/auth.service";
import { Order, View, Bread } from "src/app/shared/interfaces/utils.interface";
import { SharedService } from "src/app/shared/services/shared.service";
import { ActivatedRoute } from "@angular/router";
import { Category } from "src/app/shared/models/category";
import { CategoryService } from "src/app/shared/services/category.service";
import { PageEvent, MatPaginator } from "@angular/material/paginator";
import { GeneralService } from 'src/app/shared/services/general.service';
import { Subscription } from 'rxjs';

@Component({
  selector: "app-publication",
  templateUrl: "./publication.component.html",
  styleUrls: ["./publication.component.scss"],
})
export class PublicationComponent implements OnInit, OnDestroy {
  @Input() categories: Category[];
  productList: Product[];
  loading = false;
  pageActive: boolean = false;

  public filter: string;
  productsList: Product[] = [];
  pagedList: Product[] = [];
  breakpoint: number = 3; //to adjust to screen
  // MatPaginator Inputs
  resultsLength: number = 0;
  pageSize: number = 12; //displaying three cards each row
  pageSizeOptions: number[] = [6, 12, 24, 60];

  @ViewChild(MatPaginator) paginator: MatPaginator;

  subscriptionActive: Subscription;
  orders: Order[] = [
    {
      id: 0,
      name: "Predeterminado",
      active: "id",
      direction: "ASC",
      cssIcon: "fas fa-sort-amount-down-alt",
    },
    {
      id: 1,
      name: "Menor precio",
      active: "price",
      direction: "ASC",
      cssIcon: "fas fa-sort-numeric-down",
    },
    {
      id: 2,
      name: "Mayor precio",
      active: "price",
      direction: "DESC",
      cssIcon: "fas fa-sort-numeric-down-alt",
    },
    {
      id: 3,
      name: "Nombre ascendente",
      active: "title",
      direction: "ASC",
      cssIcon: "fas fa-sort-alpha-down",
    },
    {
      id: 4,
      name: "Nombre descendente",
      active: "title",
      direction: "DESC",
      cssIcon: "fas fa-sort-alpha-down-alt",
    },
  ];

  breads: Bread[];

  selectedOrder: Order;
  selectedView: View;

  page = 1;
  categoryId = null;

  constructor(
    public authService: AuthService,
    private productService: ProductService,
    private categoryService: CategoryService,
    private toastrService: ToastrService,
    private sharedService: SharedService,
    private generalService: GeneralService,
    private route: ActivatedRoute
  ) {
    this.selectedOrder = this.orders[0];
    this.subscriptionActive = this.generalService.pageIsActive().subscribe(
      data => {this.pageActive = data;}
    )
  }

  ngOnInit() {
    this.getCategories();
    this.route.paramMap.subscribe((params) => {
      this.categoryId = params.get("id-category")
        ? !isNaN(Number(params.get("id-category")))
          ? Number(params.get("id-category"))
          : null
        : null;
      this.filter = null;
      this.getProducts();
      this.breads = this.resolveBreads();
    });
    // Pagination
    this.breakpoint = window.innerWidth <= 800 ? 1 : 3;
    //  this.productsList = <GetOrInitializeYourListHere>;
    this.pagedList = this.productsList.slice(0, 3);
  //  this.resultsLength = this.productsList.length;
    //end Pagination
  }

  onChangeOrder(order) {
    this.selectedOrder = order;
    this.paginator.pageIndex = 0;
    this.getProducts();
  }

  getProducts() {
    // this.spinnerService.show();
    this.productService
      .getData(
        this.filter,
        this.selectedOrder.active,
        this.selectedOrder.direction,
        (this.paginator?.pageIndex ? this.paginator.pageIndex : 0),// + 1,
        this.paginator?.pageSize ? this.paginator.pageSize : 12,
        this.categoryId ? [{ key: "categories.id", value: this.categoryId }, { key: "deletedAt", value: "null" }]:[{ key: "deletedAt", value: "null" }],
        (total) => (this.resultsLength = total)
      )
      .subscribe(
        (products) => {
          // this.spinnerService.hide();
          this.productList = products;
        },
        (err) => {
          this.toastrService.error("Error mientras cargaba los Productos", err);
        }
      );
  }

  // Pagination
  OnPageChange(event: PageEvent) {
    this.getProducts();
  }

  onResize(event) {
    //to adjust to screen size
    this.breakpoint = event.target.innerWidth <= 800 ? 1 : 3;
  }
  //end Pagination

  resolveBreads(): Bread[] {
    let result: Bread[] = [];
    if (this.categories && this.categoryId) {
      let cate = this.searchCategory(this.categories, this.categoryId);
      let route = "/shop/productos";
      if (cate.id) {
        route = route + "/" + cate.id;
      }
      result.push({ id: cate.id, name: cate.name, route: route });

      while (cate.parent && cate.parent.id) {
        cate = this.searchCategory(this.categories, cate.parent.id);
        result = [
          { id: cate.id, name: cate.name, route: "/shop/productos/" + cate.id },
          ...result,
        ];
      }
    }
    result = [{ id: null, name: "Todos", route: "/shop/productos" }, ...result];
    return result;
  }

  searchCategory(valorAnterior: Category[], id: number): Category {
    let cateResult = null;
    for (let cate of valorAnterior) {
      if (cate.id === id) {
        cateResult = cate;
        break;
      } else {
        if (cate.childList) {
          cateResult = this.searchCategory(cate.childList, id);
          if (cateResult) {
            break;
          }
        }
      }
    }
    return cateResult;
  }

  getCategories() {
    // this.spinnerService.show();
    this.loading = true;
    this.categoryService.getAllActives().subscribe(
      (categories) => {
        this.loading = false;
        // this.spinnerService.hide();
        this.categories = categories;
        this.breads = this.resolveBreads();
      },
      (err) => {
        this.toastrService.error("Error mientras cargaba las Categorias", err);
      }
    );
  }

  openProduct(idx: number) {
    this.sharedService.showProductDetail(idx);
  }

  search(text: string) {
    this.filter = text;
    this.getProducts();
  }

  ngOnDestroy() {
    this.subscriptionActive.unsubscribe();
  }
}
