import { Component, ViewChild} from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { ToastrService } from 'ngx-toastr';
import { News } from 'src/app/shared/models/news';
import { NewsService } from 'src/app/shared/services/news.service';
@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent {

  newsList: News[];
  loading = false;
  pageSize: number = 10; //displaying three cards each row
  pageSizeOptions: number[] = [5, 10, 25, 50];
  resultsLength: number = 0;

  @ViewChild(MatPaginator) paginator: MatPaginator;


  constructor(
    private newsService: NewsService,
    private toastrService: ToastrService){}

    ngOnInit() {
      this.getNews();
  }

  getNews() {
    // this.spinnerService.show();
    this.newsService
      .getData(
        null,
        'id',
        'ASC',
        (this.paginator?.pageIndex ? this.paginator.pageIndex : 0),// + 1,
        this.paginator?.pageSize ? this.paginator.pageSize : 10,
        [],
        (total) => (this.resultsLength = total)
      )
      .subscribe(
        (news) => {
          // this.spinnerService.hide();
          this.newsList = news;
        },
        (err) => {
          this.toastrService.error("Error mientras cargaba la información de prensa", err);
        }
      );
  }

  // Pagination
  OnPageChange(event: PageEvent) {
    this.getNews();
  }
}
