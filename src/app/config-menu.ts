

export const MENU_ITEMS: any[] = [
  {
    title: 'CONFIG',
    group: true,
  },
  {
    title: 'Usuarios',
    icon: 'activity-outline',
    link: '/config/usuarios',
  },
  {
    title: 'Productos',
    icon: 'cloud-upload-outline',
    link: '/config/productos',
  },
  {
    title: 'Productores',
    icon: 'cloud-upload-outline',
    link: '/config/productores',
  }
];
