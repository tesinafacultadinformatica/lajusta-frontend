import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NoAccessComponent } from './shared/components/no-access/no-access.component';
import { PageNotFoundComponent } from './shared/components/page-not-found/page-not-found.component';
import { AdminGaurd } from './shared/guards/admin-gaurd';
import { ShopModule } from './shop/shop.module';

const routes: Routes = [
  {
    path: 'config',
    canActivate: [AdminGaurd],
    loadChildren: () => import('./configuration/configuration.module')
      .then(m => m.ConfigurationModule),
  },
  {
    path: 'shop', loadChildren:() => {return ShopModule;} 
  /*  loadChildren: () => import('./shop/shop.module')
      .then(m => m.ShopModule)*/
  },
  { path: '', redirectTo: 'shop', pathMatch: 'full' },
	{ path: 'no-access', component: NoAccessComponent },
	{ path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { useHash: false, scrollPositionRestoration: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
