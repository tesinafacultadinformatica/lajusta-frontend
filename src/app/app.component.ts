import { Component, OnInit, OnDestroy, HostListener } from "@angular/core";
import { MENU_ITEMS } from "./config-menu";
import { AuthService } from "./shared/services/auth.service";
import { ModalService } from "./shared/services/modal.service";
import { LoginUser } from "./shared/models/user";
import { Router } from "@angular/router";
import { Node } from "./shared/models/node";
import { Image } from "./shared/models/image";
import { SharedService } from "./shared/services/shared.service";
import { CategoryService } from "./shared/services/category.service";
import { Category } from "./shared/models/category";
import { ToastrService } from "ngx-toastr";
import { SubscriptionLike as ISubscription } from "rxjs";
import { CartService } from "./shared/services/cart.service";
import { SelectedConfig } from "./shared/interfaces/utils.interface";
import { GeneralService } from "./shared/services/general.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent implements OnInit, OnDestroy {
  tabRegisterActive: boolean = false;
  title = "la-justa-fe";
  menu = MENU_ITEMS;
  showModalLogin: boolean = false;
  currentUser: LoginUser;
  loading: boolean = false;
  isAdmin: boolean = false;
  categories: Category[] = [];

  selectedProductId: number;
  selectedCartId: number = null;
  codeRecovery: string = null;
  selectedStaff: SelectedConfig = { id: null, action: "" };
  selectedTag: SelectedConfig = { id: null, action: "" };
  selectedPurchase: SelectedConfig = { id: null, action: "" };
  selectedExpense: SelectedConfig = { id: null, action: "" };
  selectedUnit: SelectedConfig = { id: null, action: "" };
  selectedUser: SelectedConfig = { id: null, action: "" };
  selectedProduct: SelectedConfig = { id: null, action: "" };
  selectedProducer: SelectedConfig = { id: null, action: "" };
  selectedNode: SelectedConfig = { id: null, action: "" };
  selectedGeneral: SelectedConfig = { id: null, action: "" };
  selectedBanner: SelectedConfig = { id: null, action: "" };
  selectedNews: SelectedConfig = { id: null, action: "" };
  selectedCart: SelectedConfig = { id: null, action: "" };
  selectedCategory: SelectedConfig = { id: null, action: "" };
  selectedPersonal: SelectedConfig = { id:null, action: "" };
  imagesModal: Image[] = [];
  node: Node;

  subs: ISubscription[] = [];

  constructor(
    public authService: AuthService,
    public modalService: ModalService,
    private router: Router,
    private sharedService: SharedService,
    private categoryService: CategoryService,
    private cartService: CartService,
    private generalService: GeneralService,
    private toastrService: ToastrService
  ) {
    this.generalService.setActivePage();
    if (this.authService.isLoggedIn()) {
        if (this.authService.isLoggedInBack()) {
          this.currentUser = this.authService.currentUserValue;
          this.isAdmin = this.authService.isAdmin();
        } else {
          this.authService.logout();
        }
    }
    this.authService.currentUser.subscribe((loginUser) => {
      this.currentUser = loginUser;
      this.isAdmin = this.authService.isAdmin();
    });
    if (!sessionStorage.getItem("instance")) {
      let numberInstance = Number.parseInt(localStorage.getItem("instance"));
      let instance = Number.isNaN(numberInstance) ? 1 : numberInstance + 1;
      localStorage.setItem("instance", instance.toString());
      sessionStorage.setItem("instance", instance.toString());
    }

    document.addEventListener("visibilitychange", () => {
      this.router.routeReuseStrategy.shouldReuseRoute = function () {
        return false;
      };
      this.router.onSameUrlNavigation = "reload";
      this.router.navigateByUrl(this.router.url, { queryParams: { index: 1 } });
      this.cartService.refresh();
    });
  }

  ngOnInit() {
    this.getCategories();
    //subscribe to the events
    this.subs.push(
      this.sharedService
        .modalProductDetail()
        .subscribe((data) => setTimeout(() => this.openProductDetail(data)))
    );
    this.subs.push(
      this.sharedService
        .modalChangePassword()
        .subscribe((data) => setTimeout(() => this.openChangePassword(data)))
    );
    this.subs.push(
      this.sharedService
        .modalUser()
        .subscribe((data) => setTimeout(() => this.openUser(data)))
    );
    this.subs.push(
      this.sharedService
        .modalUnit()
        .subscribe((data) => setTimeout(() => this.openUnit(data)))
    );
    this.subs.push(
      this.sharedService
        .modalStaff()
        .subscribe((data) => setTimeout(() => this.openStaff(data)))
    );
    this.subs.push(
      this.sharedService
        .modalTag()
        .subscribe((data) => setTimeout(() => this.openTag(data)))
    );
    this.subs.push(
      this.sharedService
        .modalExpense()
        .subscribe((data) => setTimeout(() => this.openExpense(data)))
    );
    this.subs.push(
      this.sharedService
        .modalPurchase()
        .subscribe((data) => setTimeout(() => this.openPurchase(data)))
    );
    this.subs.push(
      this.sharedService
        .modalProduct()
        .subscribe((data) => setTimeout(() => this.openProduct(data)))
    );
    this.subs.push(
      this.sharedService
        .modalProducer()
        .subscribe((data) => setTimeout(() => this.openProducer(data)))
    );
    this.subs.push(
      this.sharedService
        .modalNode()
        .subscribe((data) => setTimeout(() => this.openNode(data)))
    );
    this.subs.push(
      this.sharedService
        .modalGeneral()
        .subscribe((data) => setTimeout(() => this.openGeneral(data)))
    );
    this.subs.push(
      this.sharedService
        .modalCategory()
        .subscribe((data) => setTimeout(() => this.openCategory(data)))
    );
    this.subs.push(
      this.sharedService
        .modalCart()
        .subscribe((data) => setTimeout(() => this.openCart(data)))
    );
    this.subs.push(
      this.sharedService
        .modalBanner()
        .subscribe((data) => setTimeout(() => this.openBanner(data)))
    );
    this.subs.push(
      this.sharedService
        .modalNews()
        .subscribe((data) => setTimeout(() => this.openNews(data)))
    );
    this.subs.push(
      this.sharedService
        .modalPersonal()
        .subscribe((data) => setTimeout(() => this.openPersonal(data))
      )
    );
  }

  ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }

  @HostListener("window:beforeunload", ["$event"])
  beforeUnloadHandler(event) {
    let instance = Number.parseInt(localStorage.getItem("instance")) - 1;
    if (instance <= 0) {
      localStorage.removeItem("list_item");
      localStorage.removeItem("instance");
    } else {
      localStorage.setItem("instance", instance.toString());
    }
    sessionStorage.removeItem("instance");
  }

  getCategories() {
    // this.spinnerService.show();
    this.loading = true;
    this.subs.push(
      this.categoryService.getAllActives().subscribe(
        (categories) => {
          this.loading = false;
          // this.spinnerService.hide();
          this.categories = categories;
        },
        (err) => {
          this.toastrService.error(
            "Error mientras cargaba las Categorias",
            err
          );
        }
      )
    );
  }

  logout() {
    this.authService.logout();
    this.router.navigate(["/"]);
  }

  openProductDetail(productId: number) {
    this.selectedProductId = productId;
    this.modalService.open("product-detail-modal");
  }

  openChangePassword(code: string) {
    this.codeRecovery = code;
    this.modalService.open("change-password-modal");
  }

  openGallery(images: Image[]) {
    this.imagesModal = images;
    this.modalService.open("gallery-modal");
  }

  openNodeLocation(node: Node) {
    this.node = node;
    this.modalService.open("location-modal");
  }

  openCartDetail(cartId: number) {
    this.selectedCartId = cartId;
    this.modalService.open("cart-modal");
  }

  openUser(data: any) {
    this.selectedUser.id = data.userId;
    this.selectedUser.action = data.action;
    this.modalService.open("config-user-modal");
  }

  openUnit(data: any) {
    this.selectedUnit.id = data.unitId;
    this.selectedUnit.action = data.action;
    this.modalService.open("config-unit-modal");
  }

  openExpense(data: any) {
    this.selectedExpense.id = data.expenseId;
    this.selectedExpense.action = data.action;
    this.modalService.open("config-expense-modal");
  }

  openPurchase(data: any) {
    this.selectedPurchase.id = data.purchaseId;
    this.selectedPurchase.action = data.action;
    this.modalService.open("config-purchase-modal");
  }

  openStaff(data: any) {
    this.selectedStaff.id = data.staffId;
    this.selectedStaff.action = data.action;
    this.modalService.open("config-staff-modal");
  }

  openTag(data: any) {
    this.selectedTag.id = data.tagId;
    this.selectedTag.action = data.action;
    this.modalService.open("config-tag-modal");
  }

  openProduct(data: any) {
    this.selectedProduct.id = data.productId;
    this.selectedProduct.action = data.action;
    this.modalService.open("config-product-modal");
  }

  openProducer(data: any) {
    this.selectedProducer.id = data.producerId;
    this.selectedProducer.action = data.action;
    this.modalService.open("config-producer-modal");
  }

  openNode(data: any) {
    this.selectedNode.id = data.nodeId;
    this.selectedNode.action = data.action;
    this.modalService.open("config-node-modal");
  }

  openGeneral(data: any) {
    this.selectedGeneral.id = data.generalId;
    this.selectedGeneral.action = data.action;
    this.modalService.open("config-general-modal");
  }

  openCategory(data: any) {
    this.selectedCategory.id = data.categoryId;
    this.selectedCategory.action = data.action;
    this.modalService.open("config-category-modal");
  }

  openCart(data: any) {
    this.selectedCart.id = data.cartId;
    this.selectedCart.action = data.action;
    this.modalService.open("config-cart-modal");
  }

  openBanner(data: any) {
    this.selectedBanner.id = data.bannerId;
    this.selectedBanner.action = data.action;
    this.modalService.open("config-banner-modal");
  }

  openNews(data: any) {
    this.selectedNews.id = data.newsId;
    this.selectedNews.action = data.action;
    this.modalService.open("config-news-modal");
  }

  openPersonal(data: any){
    this.selectedPersonal.id = data.personalId;
    this.selectedPersonal.action = data.action;
    this.modalService.open("config-personal-modal");
  }

}
