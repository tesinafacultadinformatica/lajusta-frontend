import { registerLocaleData, CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';

import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';
import { ToastrModule } from 'ngx-toastr';
import { MaterialModule } from './material.module';
import { LOCALE_ID } from '@angular/core';
import localeAr from '@angular/common/locales/es-AR';

registerLocaleData(localeAr);
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
		ToastrModule.forRoot({
      positionClass: "toast-bottom-right",
      closeButton:true
    }),
    MDBBootstrapModule.forRoot(),
    FormsModule,
    SharedModule,
    CommonModule,
    MaterialModule
  ],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: "es_AR"
    }],
  bootstrap: [AppComponent],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class AppModule { }
