import { Component, OnInit, OnDestroy } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ToastrService } from "ngx-toastr";
import { SubscriptionLike as ISubscription } from "rxjs";
import { DialogModalComponent } from "src/app/shared/modals/dialog-modal/dialog-modal.component";
import { Filter } from "src/app/shared/models/filter";
import { AuthService } from "src/app/shared/services/auth.service";
import { BalanceService } from "src/app/shared/services/balance.service";
import { ExpenseService } from "src/app/shared/services/expense.service";
import { GeneralService } from "src/app/shared/services/general.service";
import { PurchaseService } from "src/app/shared/services/purchase.service";
import { SharedService } from "src/app/shared/services/shared.service";

@Component({
  selector: "app-balance",
  templateUrl: "./balance.component.html",
})
export class BalanceComponent implements OnInit, OnDestroy {
  subs: ISubscription[] = [];
  idGeneral: number = null;
  selectedBalanceIsCurrentGeneralClose = false;
  selectedBalanceIsCurrentGeneral = false;
  aditionalFilter: Filter = null;
  currentGeneral: number = null;

  constructor(
    public authService: AuthService,
    private generalService: GeneralService,
    public balanceService: BalanceService,
    public purchaseService: PurchaseService,
    public expenseService: ExpenseService,
    public sharedService: SharedService,
    private toastrService: ToastrService,
    public dialog: MatDialog
  ) {
    this.generalService.getActive().subscribe((result) => {
      this.idGeneral = result ? result.id : null;
      this.updateGeneral(this.idGeneral);
    });
  }

  ngOnInit() {}

  onDetailBalance(event) {
    this.updateGeneral(event.general.id);
  }

  updateGeneral(generalId: number) {
    this.currentGeneral = generalId;
    if (generalId){
      this.generalService
        .getById(generalId)
        .subscribe(
          (general) =>
            (this.selectedBalanceIsCurrentGeneralClose =
              new Date(general.dateDownPage).getTime() < Date.now() &&
              generalId === this.idGeneral)
        );
    }
    this.selectedBalanceIsCurrentGeneral = generalId && generalId === this.idGeneral;
    this.aditionalFilter = generalId
      ? { key: "general.id", value: generalId }
      : null;
  }

  onNewExpense() {
    this.sharedService.showExpense(null, "new");
  }

  onEditExpense(event) {
    this.sharedService.showExpense(event, "edit");
  }

  onDeleteExpense(event) {
    this.openDialog(
      "¿Esta seguro?",
      "Va a eliminar el gasto " + event.service,
      event.id,
      true,
      this.expenseService
    );
  }

  onRestoreExpense(event) {
    this.openDialog(
      "¿Esta seguro?",
      "Va a restaurar el gasto " + event.service,
      event.id,
      false,
      this.expenseService
    );
  }

  onNewPurchase() {
    this.sharedService.showPurchase(null, "new");
  }

  onEditPurchase(event) {
    this.sharedService.showPurchase(event, "edit");
  }

  onDeletePurchase(event) {
    this.openDialog(
      "¿Esta seguro?",
      "Va a eliminar la compra que debe realizar de " + event.producto.name,
      event.id,
      true,
      this.purchaseService
    );
  }
  onRestorePurchase(event) {
    this.openDialog(
      "¿Esta seguro?",
      "Va a restaurar la compra " + event.producto.name,
      event.id,
      false,
      this.purchaseService
    );
  }

  openDialog(
    title: string,
    description: string,
    id: number,
    toDelete: boolean,
    service: any
  ): void {
    const dialogRef = this.dialog.open(DialogModalComponent, {
      disableClose: true,
      width: "300px",
      data: { title: title, description: description },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.subs.push(
          service[toDelete ? "delete" : "restore"](id).subscribe(
            (data) => {
              this.toastrService.success(
                `${toDelete ? "Eliminado" : "Habilitado"} correctamente`,
                `Se ${toDelete ? "eliminó" : "habilit"} el gasto`
              );
              window.location.reload();
            },
            (error) => {
              this.toastrService.error(
                "Algo salio mal",
                `No se pudo ${toDelete ? "eliminar" : "habilitar"} el gasto`
              );
            }
          )
        );
      }
    });
  }

  ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
