import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth.service';
import { DialogModalComponent } from 'src/app/shared/modals/dialog-modal/dialog-modal.component';
import { SharedService } from 'src/app/shared/services/shared.service';
import { MatDialog } from '@angular/material/dialog';
import { CartService } from 'src/app/shared/services/cart.service';
import { SubscriptionLike as ISubscription } from "rxjs";
import { ToastrService } from 'ngx-toastr';
import { ReportService } from 'src/app/shared/services/report.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
})
export class CartComponent implements OnDestroy {
  subs: ISubscription[] = [];
  constructor(
    public authService: AuthService,
    public sharedService: SharedService,
    public reportService: ReportService,
   public cartService: CartService,
    private toastrService: ToastrService,
    public dialog: MatDialog
  ) {
  }


  onDetail(event) {
    this.sharedService.showCart(event,'detail');
   }

  onEdit(event) {
    this.sharedService.showCart(event,'edit');
  }

  onRestore(event) {
    this.openDialog('¿Esta seguro?', 'Va a restaurar la compra ' + event.id,
    event.id,
    'restore'
  );
  }

  onDelete(event) {
    this.openDialog('¿Esta seguro?', 'Va a eliminar la compra ' + event.id,
    event.id,
    'delete'
  );
  }

  onCheck(event) {
    this.openDialog('¿No se entregara la compra?', 'Va a confirmar la cancelacion de la entrega de la compra ' + event.id,
    event.id,
    'canceled'
  );
}

openDialog(
  title: string,
  description: string,
  id: number,
  action: string
): void {
  const dialogRef = this.dialog.open(DialogModalComponent, {
    disableClose: true,
    width: "300px",
    data: { title: title, description: description },
  });
  dialogRef.afterClosed().subscribe((result) => {
    if (result) {
      this.subs.push(
        this.cartService[action](id).subscribe(
          (data) => {
            this.toastrService.success(
              `${action === 'delete' ? "Eliminado" : (action === 'restore' ?  "Habilitado" : "Cancelado la entrega")} correctamente`,
              `Se ${action === 'delete' ? "elimino" : (action === 'restore' ?  "habilito"  : "canceló la entrega")} la compra`
            );
            window.location.reload();
          },
          (error) => {
            this.toastrService.error(
              "Algo salio mal",
              `No se pudo ${action === 'delete' ? "eliminar" :  (action === 'restore' ? "habilitar" : "actualizar")} la compra `
            );
          }
        )
      );
    }
  });
}

onDownload1(event) {
  this.reportService.getPDFByCart(event.id).subscribe(res => {
    window.open(res, "_blank");
  })
}

ngOnDestroy() {
  this.subs.forEach((sub) => sub.unsubscribe());
}
}
