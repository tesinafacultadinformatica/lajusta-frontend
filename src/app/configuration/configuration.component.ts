import { Component, OnInit } from '@angular/core';
import { MenuConfiguration } from '../shared/interfaces/utils.interface';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.scss']
})
export class ConfigurationComponent implements OnInit {

  menuConfig: MenuConfiguration[] = [
    { name: 'Rondas', route:'rondas'  },
    { name: 'Balance', route:'balances'  },
    { name: 'Banner', route:'banners'  },
    { name: 'Productos', route:'productos' },
    { name: 'Categorias', route:'categorias'  },
    { name: 'Productores', route:'productores'  },
    { name: 'Ventas', route:'ventas'  },
    { name: 'Nodos', route:'nodos'  },
    { name: 'Usuarios', route:'usuarios'  },
    { name: 'Unidades', route:'unidades'  },
    { name: 'Equipo', route:'personal' },
    { name: 'Staff', route:'staff'  },
    { name: 'Tag', route:'tag'  },
    { name: 'Prensa', route:'prensa'  }
  ];

  constructor() {

  }

  ngOnInit(): void {
  }

}
