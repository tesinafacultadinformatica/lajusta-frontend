import { Component, OnInit, OnDestroy } from "@angular/core";
import { AuthService } from "src/app/shared/services/auth.service";
import { DialogModalComponent } from "src/app/shared/modals/dialog-modal/dialog-modal.component";
import { SharedService } from "src/app/shared/services/shared.service";
import { MatDialog } from "@angular/material/dialog";
import { SubscriptionLike as ISubscription } from "rxjs";
import { ToastrService } from "ngx-toastr";
import { UnitService } from "src/app/shared/services/unit.service";

@Component({
  selector: "app-unit",
  templateUrl: "./unit.component.html",
})
export class UnitComponent implements OnDestroy {
  subs: ISubscription[] = [];

  constructor(
    public authService: AuthService,
    public sharedService: SharedService,
    private toastrService: ToastrService,
    public unitService: UnitService,
    public dialog: MatDialog
  ) {}


  onNew() {
    this.sharedService.showUnit(null, "new");
  }

  onDetail(event) {
    this.sharedService.showUnit(event, "detail");
  }

  onEdit(event) {
    this.sharedService.showUnit(event, "edit");
  }

  onRestore(event) {
    this.openDialog(
      "¿Esta seguro?",
      "Va a restaurar la unidad " + event.description,
      event.id,
      false
    );
  }

  onDelete(event) {
    this.openDialog(
      "¿Esta seguro?",
      "Va a eliminar la unidad " + event.description,
      event.id,
      true
    );
  }

  openDialog(
    title: string,
    description: string,
    id: number,
    toDelete: boolean
  ): void {
    const dialogRef = this.dialog.open(DialogModalComponent, {
      disableClose: true,
      width: "300px",
      data: { title: title, description: description },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.subs.push(
          this.unitService[toDelete ? "delete" : "restore"](id).subscribe(
            (data) => {
              this.toastrService.success(
                `${toDelete ? "Eliminado" : "Habilitado"} correctamente`,
                `Se ${toDelete ? "elimino" : "habilito"} la unidad`
              );
              window.location.reload();
            },
            (error) => {
              this.toastrService.error(
                "Algo salio mal",
                `No se pudo ${toDelete ? "eliminar" : "habilitar"} la unidad`
              );
            }
          )
        );
      }
    });
  }

  ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
