import { Component, OnInit, OnDestroy } from "@angular/core";
import { AuthService } from "src/app/shared/services/auth.service";
import { SharedService } from "src/app/shared/services/shared.service";
import { MatDialog } from "@angular/material/dialog";
import { DialogModalComponent } from "src/app/shared/modals/dialog-modal/dialog-modal.component";
import { ProductService } from "src/app/shared/services/product.service";
import { SubscriptionLike as ISubscription } from "rxjs";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-product",
  templateUrl: "./product.component.html",
})
export class ProductComponent implements OnDestroy {
  subs: ISubscription[] = [];

  constructor(
    public authService: AuthService,
    public sharedService: SharedService,
    private toastrService: ToastrService,
    public productService: ProductService,
    public dialog: MatDialog
  ) {}

  onNew() {
    this.sharedService.showProduct(null, "new");
  }

  onDetail(event) {
    this.sharedService.showProduct(event, "detail");
  }

  onEdit(event) {
    this.sharedService.showProduct(event, "edit");
  }

  onRestore(event) {
    this.openDialog(
      "¿Esta seguro?",
      "Va a restaurar al producto " + event.title,
      event.id,
      false
    );
  }

  onDelete(event) {
    this.openDialog(
      "¿Esta seguro?",
      "Va a eliminar al producto " + event.title,
      event.id,
      true
    );
  }

  openDialog(title: string, description: string, id: number, toDelete: boolean): void {
    const dialogRef = this.dialog.open(DialogModalComponent, {
      disableClose: true,
      width: "300px",
      data: { title: title, description: description },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.subs.push(
          this.productService[toDelete ? "delete" : "restore"](id).subscribe(
            (data) => {
              this.toastrService.success(`${toDelete ? "Eliminado" : "Habilitado"} correctamente` , `Se ${toDelete ? "elimino" : "habilito"} el producto`);
              window.location.reload();
            },
            (error) => {
              this.toastrService.error("Algo salio mal", `No se pudo ${toDelete ? "eliminar" : "habilitar"} el producto `);
            }
          )
        );
      }
    });
  }

  ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
