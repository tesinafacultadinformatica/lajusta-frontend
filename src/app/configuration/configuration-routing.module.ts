import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ConfigurationComponent } from './configuration.component';
import { UserComponent } from './user/user.component';
import { ProductComponent } from './product/product.component';
import { ProducerComponent } from './producer/producer.component';
import { CartComponent } from './cart/cart.component';
import { GeneralComponent } from './general/general.component';
import { BannerComponent } from './banner/banner.component';
import { CategoryComponent } from './category/category.component';
import { NodeComponent } from './node/node.component';
import { UnitComponent } from './unit/unit.component';
import { StaffComponent } from './staff/staff.component';
import { TagComponent } from './tag/tag.component';
import { BalanceComponent } from './balance/balance.component';
import { NewsComponent } from './news/news.component';
import { PersonalComponent } from './personal/personal.component';


const routes: Routes = [
  {
    path: '',
    component: ConfigurationComponent,
    children: [
      {
        path: 'rondas',
        component: GeneralComponent,
      },
      {
        path: 'banners',
        component: BannerComponent,
      },
      {
        path: 'ventas',
        component: CartComponent,
      },
      {
        path: 'categorias',
        component: CategoryComponent,
      },
      {
        path: 'nodos',
        component: NodeComponent,
      },
      {
        path: 'usuarios',
        component: UserComponent,
      },
      {
        path: 'productos',
        component: ProductComponent,
      },
      {
        path: 'productores',
        component: ProducerComponent,
      },
      {
        path: 'unidades',
        component: UnitComponent,
      },
      {
        path: 'staff',
        component: StaffComponent,
      },
      {
        path: 'tag',
        component: TagComponent,
      },
      {
        path: 'balances',
        component: BalanceComponent,
      },
      {
        path: 'prensa',
        component: NewsComponent,
      },
      {
        path: 'personal',
        component: PersonalComponent,
      },
      {
        path: '',
        redirectTo: 'rondas',
        pathMatch: 'full',
      }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfigurationRoutingModule {
}
