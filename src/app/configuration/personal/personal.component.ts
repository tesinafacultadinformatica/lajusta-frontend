import { Component, OnDestroy } from "@angular/core";
import { AuthService } from "src/app/shared/services/auth.service";
import { MatDialog } from "@angular/material/dialog";
import { DialogModalComponent } from "src/app/shared/modals/dialog-modal/dialog-modal.component";
import { SharedService } from "src/app/shared/services/shared.service";
import { SubscriptionLike as ISubscription } from "rxjs";
import { ToastrService } from "ngx-toastr";
import { User } from "src/app/shared/models/user";
import { Role } from "src/app/shared/enums/role.enum"
import { UserService } from "src/app/shared/services/user.service";

@Component({
  selector: "app-personal",
  templateUrl: "./personal.component.html",
})
export class PersonalComponent implements OnDestroy {
  subs: ISubscription[] = [];
  personalList: User[];

  filter: string;

  constructor(
    public authService: AuthService,
    public sharedService: SharedService,
    private toastrService: ToastrService,
    private userService: UserService,
    public dialog: MatDialog
  ) {}

  onNew() {
    this.sharedService.showPersonal(null, "new");
  }

  onDetail(event) {
    this.sharedService.showUser(event, "detail");
  }

  onEdit(event) {
    this.sharedService.showUser(event, "edit");
  }

  onDelete(event) {
    (event.role == Role.Administrador) 
    ? this.toastrService.error(
      "Algo salio mal",
      `No se puede eliminar a los administradores del equipo`) 
    : this.openDialog(
      "¿Esta seguro?",
      "Va a eliminar al usuario  " + event.firstName + " " + event.lastName + " del equipo",
      event,
      true
    );
  }

  openDialog(
    title: string,
    description: string,
    user: User,
    toDelete: boolean
  ): void {
    const dialogRef = this.dialog.open(DialogModalComponent, {
      disableClose: true,
      width: "300px",
      data: { title: title, description: description },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        toDelete ? user.role= +Role.Usuario : user.role= +Role.Equipo;
        this.subs.push(
          this.userService.update(user).subscribe(
            (data) => {
              this.toastrService.success(
                `${toDelete ? "Eliminado" : "Habilitado"} correctamente`,
                `Se ${toDelete ? "elimino" : "habilito"} al usuario ${toDelete ? "del" : "en el"} equipo`
              );
              window.location.reload();
            },
            (error) => {
              this.toastrService.error(
                "Algo salio mal",
                `No se pudo ${toDelete ? "eliminar" : "habilitar"} al usuario ${toDelete ? "del" : "en el"} equipo`
              );
            }
          )
        );
      }
    });
  }

  ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}