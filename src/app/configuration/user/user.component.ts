import { Component, OnInit, OnDestroy } from "@angular/core";
import { AuthService } from "src/app/shared/services/auth.service";
import { MatDialog } from "@angular/material/dialog";
import { Router } from "@angular/router";
import { DialogModalComponent } from "src/app/shared/modals/dialog-modal/dialog-modal.component";
import { SharedService } from "src/app/shared/services/shared.service";
import { UserService } from "src/app/shared/services/user.service";
import { SubscriptionLike as ISubscription } from "rxjs";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-user",
  templateUrl: "./user.component.html",
})
export class UserComponent implements OnDestroy {
  subs: ISubscription[] = [];
  idUserModal: number;

  constructor(
    public authService: AuthService,
    //  public router: Router,
    public sharedService: SharedService,
    private toastrService: ToastrService,
    public userService: UserService,
    public dialog: MatDialog
  ) {}

  onNew() {
    this.sharedService.showUser(null, "new");
  }

  onDetail(event) {
    this.sharedService.showUser(event, "detail");
  }

  onEdit(event) {
    this.sharedService.showUser(event, "edit");
  }

  onRestore(event) {
    this.openDialog(
      "¿Esta seguro?",
      "Va a restaurar al usuario " + event.firstName + " " + event.lastName,
      event.id,
      false
    );
  }

  onDelete(event) {
    this.openDialog(
      "¿Esta seguro?",
      "Va a eliminar al usuario " + event.firstName + " " + event.lastName,
      event.id,
      true
    );
  }

  openDialog(
    title: string,
    description: string,
    id: number,
    toDelete: boolean
  ): void {
    const dialogRef = this.dialog.open(DialogModalComponent, {
      disableClose: true,
      width: "300px",
      data: { title: title, description: description },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.subs.push(
          this.userService[toDelete ? "delete" : "restore"](id).subscribe(
            (data) => {
              this.toastrService.success(
                `${toDelete ? "Eliminado" : "Habilitado"} correctamente`,
                `Se ${toDelete ? "elimino" : "habilito"} al usuario`
              );
              window.location.reload();
            },
            (error) => {
              this.toastrService.error(
                "Algo salio mal",
                `No se pudo ${toDelete ? "eliminar" : "habilitar"} al usuario`
              );
            }
          )
        );
      }
    });
  }

  ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
