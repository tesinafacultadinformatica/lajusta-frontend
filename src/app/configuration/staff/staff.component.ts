import { Component, OnInit, OnDestroy } from "@angular/core";
import { AuthService } from "src/app/shared/services/auth.service";
import { DialogModalComponent } from "src/app/shared/modals/dialog-modal/dialog-modal.component";
import { SharedService } from "src/app/shared/services/shared.service";
import { MatDialog } from "@angular/material/dialog";
import { SubscriptionLike as ISubscription } from "rxjs";
import { ToastrService } from "ngx-toastr";
import { StaffService } from "src/app/shared/services/staff.service";

@Component({
  selector: "app-staff",
  templateUrl: "./staff.component.html",
})
export class StaffComponent implements OnDestroy {
  subs: ISubscription[] = [];

  constructor(
    public authService: AuthService,
    public sharedService: SharedService,
    private toastrService: ToastrService,
    public staffService: StaffService,
    public dialog: MatDialog
  ) {}

  onNew() {
    this.sharedService.showStaff(null, "new");
  }

  onDetail(event) {
    this.sharedService.showStaff(event, "detail");
  }

  onEdit(event) {
    this.sharedService.showStaff(event, "edit");
  }

  onRestore(event) {
    this.openDialog(
      "¿Esta seguro?",
      "Va a restaurar del staff a " + event.description,
      event.id,
      false
    );
  }

  onDelete(event) {
    this.openDialog(
      "¿Esta seguro?",
      "Va a eliminar del staff a " + event.description,
      event.id,
      true
    );
  }

  openDialog(
    title: string,
    description: string,
    id: number,
    toDelete: boolean
  ): void {
    const dialogRef = this.dialog.open(DialogModalComponent, {
      disableClose: true,
      width: "300px",
      data: { title: title, description: description },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.subs.push(
          this.staffService[toDelete ? "delete" : "restore"](id).subscribe(
            (data) => {
              this.toastrService.success(
                `${toDelete ? "Eliminado" : "Habilitado"} correctamente`,
                `Se ${toDelete ? "elimino" : "habilito"} del staff`
              );
              window.location.reload();
            },
            (error) => {
              this.toastrService.error(
                "Algo salio mal",
                `No se pudo ${toDelete ? "eliminar" : "habilitar"} del staff `
              );
            }
          )
        );
      }
    });
  }

  ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
