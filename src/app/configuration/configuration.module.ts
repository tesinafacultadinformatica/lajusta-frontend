import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ConfigurationComponent } from "./configuration.component";
import { ConfigurationRoutingModule } from "./configuration-routing.module";
import { SharedModule } from "../shared/shared.module";
import { MaterialModule } from "../material.module";
import { UserComponent } from "./user/user.component";
import { ProductComponent } from "./product/product.component";
import { ProducerComponent } from "./producer/producer.component";
import { CartComponent } from "./cart/cart.component";
import { BannerComponent } from "./banner/banner.component";
import { NewsComponent } from "./news/news.component";
import { CategoryComponent } from "./category/category.component";
import { GeneralComponent } from "./general/general.component";
import { NodeComponent } from "./node/node.component";
import { UnitComponent } from "./unit/unit.component";
import { StaffComponent } from './staff/staff.component';
import { TagComponent } from './tag/tag.component';
import { BalanceComponent } from './balance/balance.component';
import { PersonalComponent } from './personal/personal.component';

@NgModule({
  imports: [
    ConfigurationRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    MaterialModule,
  ],
  declarations: [
    ConfigurationComponent,
    UserComponent,
    ProductComponent,
    ProducerComponent,
    CartComponent,
    BannerComponent,
    NewsComponent,
    CategoryComponent,
    GeneralComponent,
    NodeComponent,
    UnitComponent,
    StaffComponent,
    TagComponent,
    BalanceComponent,
    PersonalComponent,
  ],
})
export class ConfigurationModule {}
