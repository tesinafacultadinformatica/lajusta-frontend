import { Component, OnInit, OnDestroy } from "@angular/core";
import { AuthService } from "src/app/shared/services/auth.service";
import { SharedService } from "src/app/shared/services/shared.service";
import { MatDialog } from "@angular/material/dialog";
import { DialogModalComponent } from "src/app/shared/modals/dialog-modal/dialog-modal.component";
import { ProducerService } from "src/app/shared/services/producer.service";
import { SubscriptionLike as ISubscription } from "rxjs";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-producer",
  templateUrl: "./producer.component.html",
})
export class ProducerComponent implements OnDestroy {
  subs: ISubscription[] = [];

  constructor(
    public authService: AuthService,
    public sharedService: SharedService,
    private toastrService: ToastrService,
    public producerService: ProducerService,
    public dialog: MatDialog
  ) {}

  onNew() {
    this.sharedService.showProducer(null, "new");
  }

  onDetail(event) {
    this.sharedService.showProducer(event, "detail");
  }

  onEdit(event) {
    this.sharedService.showProducer(event, "edit");
  }

  onRestore(event) {
    this.openDialog(
      "¿Esta seguro?",
      "Va a restaurar al productor " + event.name,
      event.id,
      false
    );
  }

  onDelete(event) {
    this.openDialog(
      "¿Esta seguro?",
      "Va a eliminar al productor " + event.name,
      event.id,
      true
    );
  }
  openDialog(
    title: string,
    description: string,
    id: number,
    toDelete: boolean
  ): void {
    const dialogRef = this.dialog.open(DialogModalComponent, {
      disableClose: true,
      width: "300px",
      data: { title: title, description: description },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.subs.push(
          this.producerService[toDelete ? "delete" : "restore"](id).subscribe(
            (data) => {
              this.toastrService.success(
                `${toDelete ? "Eliminado" : "Habilitado"} correctamente`,
                `Se ${toDelete ? "elimino" : "habilito"} el productor`
              );
              window.location.reload();
            },
            (error) => {
              this.toastrService.error(
                "Algo salio mal",
                `No se pudo ${toDelete ? "eliminar" : "habilitar"} el productor`
              );
            }
          )
        );
      }
    });
  }

  ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
