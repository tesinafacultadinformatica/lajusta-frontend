import { Component, OnInit, OnDestroy } from "@angular/core";
import { AuthService } from "src/app/shared/services/auth.service";
import { SharedService } from "src/app/shared/services/shared.service";
import { MatDialog } from "@angular/material/dialog";
import { DialogModalComponent } from "src/app/shared/modals/dialog-modal/dialog-modal.component";
import { GeneralService } from "src/app/shared/services/general.service";
import { SubscriptionLike as ISubscription } from "rxjs";
import { ToastrService } from "ngx-toastr";
import { ReportService } from 'src/app/shared/services/report.service';
import { BalanceService } from 'src/app/shared/services/balance.service';

@Component({
  selector: "app-general",
  templateUrl: "./general.component.html",
})
export class GeneralComponent implements OnInit, OnDestroy {
  subs: ISubscription[] = [];

  constructor(
    public authService: AuthService,
    public sharedService: SharedService,
    public reportService: ReportService,
    private toastrService: ToastrService,
    private balanceService: BalanceService,
    public generalService: GeneralService,
    public dialog: MatDialog
  ) {}

  ngOnInit() {

  }

  onNew() {
    this.sharedService.showGeneral(null, "new");
  }

  onDetail(event) {
    this.sharedService.showGeneral(event, "detail");
  }

  onEdit(event) {
    this.sharedService.showGeneral(event, "edit");
  }

  onCopy(event) {
    this.sharedService.showGeneral(event, "copy");
  }

  onDownload1(event) {
    this.reportService.getReprtGeneralByCarts(event.id).subscribe(res => {
      window.open(res, "_blank");
    })
  }

  onDownload2(event) {
    this.reportService.getReprtGeneralByNode(event.id).subscribe(res => {
      window.open(res, "_blank");
    })

  }

  downloadBalaces() {
    this.reportService.getReprtAllBalances().subscribe(res => {
      window.open(res, "_blank");
    })
  }


  onRestore(event) {
    this.openDialog(
      "¿Esta seguro?",
      "Va a restaurar la configuración general " + event.id,
      event.id,
      false
    );
  }

  onDelete(event) {
    this.openDialog(
      "¿Esta seguro?",
      "Va a eliminar la configuración general " + event.id,
      event.id,
      true
    );
  }

  openDialog(
    title: string,
    description: string,
    id: number,
    toDelete: boolean
  ): void {
    const dialogRef = this.dialog.open(DialogModalComponent, {
      disableClose: true,
      width: "300px",
      data: { title: title, description: description },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.subs.push(
          this.generalService[toDelete ? "delete" : "restore"](id).subscribe(
            (data) => {
              this.toastrService.success(
                `${toDelete ? "Eliminado" : "Habilitado"} correctamente`,
                `Se ${toDelete ? "elimino" : "habilito"} el general`
              );
              window.location.reload();
            },
            (error) => {
              this.toastrService.error(
                error,
                `No se pudo ${toDelete ? "eliminar" : "habilitar"} el general`
              );
            }
          )
        );
      }
    });
  }

  closeBalance(event){
    this.subs.push(
        this.balanceService.close(event.id).subscribe(res => {
          this.toastrService.success(
            `Cerrado correctamente`,
            `Se cerro el balance`
          );
          window.location.reload();
        }, error => {
          this.toastrService.error(
            "Algo salio mal",
            `No se pudo cerrar el balance`
          );
        })
    );
  }

  ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
