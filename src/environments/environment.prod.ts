export const environment = {
  production: true,
  url_api_base: "https://lajustaunlp.com.ar",
  mapboxKey: 'pk.eyJ1IjoicGlwbzEyMjEiLCJhIjoiY2thd2xuMXlnMDc3MjJxbjRnN2ZnNGx1YSJ9.Onj59IEYRcgvuT_JO2CmhQ'
};
